-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Окт 29 2014 г., 12:36
-- Версия сервера: 5.5.39
-- Версия PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `museum`
--

-- --------------------------------------------------------

--
-- Структура таблицы `mu_eventdates`
--

CREATE TABLE IF NOT EXISTS `mu_eventdates` (
`id` int(11) unsigned NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `mu_eventdates`
--

INSERT INTO `mu_eventdates` (`id`, `event_id`, `date`) VALUES
(1, 1, '2014-10-09 12:30:00'),
(2, 2, '2014-10-31 14:30:00');

-- --------------------------------------------------------

--
-- Структура таблицы `mu_events`
--

CREATE TABLE IF NOT EXISTS `mu_events` (
`id` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `kids` enum('0','1') DEFAULT '0',
  `type` tinyint(4) DEFAULT NULL,
  `apply` tinyint(4) DEFAULT NULL,
  `apply_phone` varchar(32) DEFAULT NULL,
  `apply_max` int(11) DEFAULT NULL,
  `lecture_id` int(11) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `mu_events`
--

INSERT INTO `mu_events` (`id`, `title`, `date`, `url`, `status`, `kids`, `type`, `apply`, `apply_phone`, `apply_max`, `lecture_id`, `meta_title`, `keywords`, `description`, `date_added`) VALUES
(2, 'Выставка художника Владимира Глухова &laquo;Качим-Кермек&raquo;', '2014-10-24 14:00:00', '', 1, '1', 1, 3, '', 200, 1, '', '', '', '2014-09-18 18:13:44');

-- --------------------------------------------------------

--
-- Структура таблицы `mu_lectures`
--

CREATE TABLE IF NOT EXISTS `mu_lectures` (
`id` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `date_added` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `mu_lectures`
--

INSERT INTO `mu_lectures` (`id`, `title`, `status`, `date_added`) VALUES
(1, 'Михаил Шемякин. Рисунки в стиле Дзен', 1, NULL),
(2, 'Булгаков в русской литературе', 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `mu_news`
--

CREATE TABLE IF NOT EXISTS `mu_news` (
`id` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `anons` text,
  `status` tinyint(4) DEFAULT '0',
  `date` date DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `mu_news`
--

INSERT INTO `mu_news` (`id`, `title`, `anons`, `status`, `date`, `date_added`, `url`, `meta_title`, `keywords`, `description`) VALUES
(1, 'В музее стартовал фестиваль &laquo;Египетская культурная инициатива&raquo;', '12 сентября состоялось торжественное открытие фестиваля &laquo;Египетская культурная инициатива&raquo;. Все желающие смогут ознакомиться с современной египетской литературой на русском языке, побывать на презентации книги Дмитрия Карягина &laquo;ЖЖизнь в Египте&raquo; и многое другое.', 1, '2014-09-23', '2014-09-22 11:36:56', '', '', '', ''),
(2, 'Музей представит в Индии миниатюру Великих Монголов', 'Впервые одна из ценнейших коллекций ГМВ - иллюстрации к историческим мемуарам Захира ад-дина Мухаммеда Бабура (основателя Империи Великих Моголов) будет показана в Национальном музее Нью-Дели.', 1, '2014-09-26', '2014-09-22 12:12:03', '', '', '', ''),
(3, 'Открытие выставки &laquo;Жизнь Будды в искусстве Мьянмы. Из фотоархива Алексея Кириченко&raquo;', 'На прошлой неделе состоялось открытие фотовыставки, посвященной храмовой росписи одной из самых таинственных стран Юго-Восточной Азии.', 0, '2014-09-22', '2014-09-22 12:13:43', '', '', '', ''),
(4, 'Открытие выставки &laquo;Лаковая живопись Вьетнама&raquo;', 'Вчера, 25 июня, состоялось торжественное открытие выставки вьетнамской лаковой живописи. В экспозиции представлены произведения, принадлежащие творческому коллективу &laquo;Sơn Ta&raquo; (&laquo;Лаки Вьетнама&raquo;).', 1, '2014-09-26', '2014-09-22 12:14:57', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `mu_oneexhibits`
--

CREATE TABLE IF NOT EXISTS `mu_oneexhibits` (
`id` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `short_desc` text,
  `status` tinyint(4) DEFAULT '0',
  `date_added` datetime DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `mu_oneexhibits`
--

INSERT INTO `mu_oneexhibits` (`id`, `title`, `url`, `short_desc`, `status`, `date_added`, `meta_title`, `keywords`, `description`) VALUES
(1, 'Н. Пиросманашвили &laquo;Кутеж&raquo;', '', 'Кутежи были излюбленным сюжетом великого грузинского художника-примитивиста Пиросманашвили, ибо они отвечали основной задаче его искусства - служить украшению тифлисских духанов и отражали одну из самых характерных и неповторимых примет грузинского быта того времени.', 1, '2014-09-19 14:54:45', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `mu_pages`
--

CREATE TABLE IF NOT EXISTS `mu_pages` (
`id` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `parent` varchar(16) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `lang` varchar(16) DEFAULT 'ru',
  `col1_text` text,
  `col2_text` text,
  `anchor` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Дамп данных таблицы `mu_pages`
--

INSERT INTO `mu_pages` (`id`, `title`, `url`, `parent`, `meta_title`, `keywords`, `description`, `status`, `lang`, `col1_text`, `col2_text`, `anchor`) VALUES
(1, 'Как добраться', 'contacts', 'visitors', NULL, NULL, NULL, 1, 'ru', '&lt;p&gt;119019, Москва, Никитский бульвар, дом 12А&lt;/p&gt;\n&lt;p&gt;\n        Телефон для справок: &lt;b&gt;+7 495 691-02-12&lt;/b&gt;\n        &lt;br/&gt;\n        Экскурсбюро: &lt;b&gt;+7 495 691-82-19&lt;/b&gt;\n        &lt;br/&gt;\n        Факс: &lt;b&gt;+7 495 695-48-46&lt;/b&gt;\n        &lt;br/&gt;\n        Пресс-служба: &lt;b&gt;+7 495 690-05-23&lt;/b&gt;\n&lt;/p&gt;\n&lt;p&gt;\n        Почта: &lt;a href=&quot;mailto:info@orientmuseum.ru&quot; class=&quot;text-red&quot;&gt;info@orientmuseum.ru&lt;/a&gt;&lt;br&gt;\n        Сайт: &lt;a href=&quot;http://orientmuseum.ru&quot; target=&quot;_blank&quot; class=&quot;text-red&quot;&gt;orientmuseum.ru&lt;/a&gt; или &lt;a href=&quot;http://музейвостока.рф&quot; target=&quot;_blank&quot; class=&quot;text-red&quot;&gt;музейвостока.рф&lt;/a&gt;\n&lt;/p&gt;\n&lt;p&gt;\n        Пн &amp;mdash; выходной\n        &lt;br/&gt;\n        Вт&amp;ndash;Ср &amp;mdash; 11:00&amp;ndash;20:00\n        &lt;br/&gt;\n        Чт &amp;mdash; 12:00&amp;ndash;21:00\n        &lt;br/&gt;\n        Кассы закрываются за 30 минут до закрытия музея.\n&lt;/p&gt;', '&lt;p&gt;\n        &lt;b&gt;Пешком&lt;/b&gt;\n        &lt;br/&gt;\n        До станции метро Арбатская (Арбатско-Покровской линии), далее пешком вверх по Никитскому бульвару.\n&lt;/p&gt;\n&lt;p&gt;\n        &lt;b&gt;Наземным транспортом&lt;/b&gt;\n        &lt;br/&gt;\n        До станции метро Пушкинская, далее троллейбусами № 1, 5, 15, 31 до остановки &laquo;Арбатские ворота, Музей Востока&raquo;.\n&lt;/p&gt;', 1),
(2, 'How to reach', 'contacts_en', 'visitors', '', '', '', 1, 'en', '&lt;p&gt;119019, Moscow, Nikitsky boulevard, build. 12А&lt;/p&gt;\n&lt;p&gt;\n        For more information call: &lt;b&gt;+7 495 691-02-12&lt;/b&gt;\n        &lt;br/&gt;\n        Tour desk: &lt;b&gt;+7 495 691-82-19&lt;/b&gt;\n        &lt;br/&gt;\n        Fax: &lt;b&gt;+7 495 695-48-46&lt;/b&gt;\n        &lt;br/&gt;\n        Press office: &lt;b&gt;+7 495 690-05-23&lt;/b&gt;\n&lt;/p&gt;\n&lt;p&gt;\n        Mail: &lt;a href=&quot;mailto:info@orientmuseum.ru&quot; class=&quot;text-red&quot;&gt;info@orientmuseum.ru&lt;/a&gt;&lt;br&gt;\n        Website: &lt;a href=&quot;http://orientmuseum.ru&quot; target=&quot;_blank&quot; class=&quot;text-red&quot;&gt;orientmuseum.ru&lt;/a&gt; or &lt;a href=&quot;http://музейвостока.рф&quot; target=&quot;_blank&quot; class=&quot;text-red&quot;&gt;музейвостока.рф&lt;/a&gt;\n&lt;/p&gt;\n&lt;p&gt;\n        Mon &amp;mdash; closed\n        &lt;br/&gt;\n        Tue&amp;ndash;Wed &amp;mdash; 11:00&amp;ndash;20:00\n        &lt;br/&gt;\n        Thu &amp;mdash; 12:00&amp;ndash;21:00\n        &lt;br/&gt;\n        The ticket office closes 30 minutes before closing time. \n&lt;/p&gt;', '&lt;p&gt;\n        &lt;b&gt;Reached on foot&lt;/b&gt;\n        &lt;br/&gt;\n        Up to Arbatskaya metro station (Arbatsko-Pokrovskaya line), and walk up the Nikitsky boulevard.\n&lt;/p&gt;\n&lt;p&gt;\n        &lt;b&gt;By road&lt;/b&gt;\n        &lt;br/&gt;\n        Up to Pushkinskaya metro station, then by trolleybus number 1, 5, 15 or 31 to the stop &laquo;Arbat Gate, Museum of the East&raquo;.\n&lt;/p&gt;', 1),
(3, 'О музее', 'about', 'about', '', '', '', 1, 'ru', NULL, NULL, 0),
(4, 'История', 'history', 'about', '', '', '', 1, 'ru', NULL, NULL, 0),
(5, 'Администрация', 'administration', 'about', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(6, 'Партнеры', 'partners', 'about', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(7, 'Вакансии', 'vacancies', 'about', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(8, 'Госзакупки', 'government_purchases', 'about', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(9, 'Видео', 'video', 'about', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(10, 'Фонд развития', 'development_fund', 'about', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(11, 'Филиал', 'affiliate', 'about', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(12, 'Билеты', 'tickets', 'visitors', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(13, 'Цены', 'prices', 'visitors', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(14, 'Экскурсии', 'tours', 'visitors', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(15, 'Научная библиотека ГМВ', 'library', 'visitors', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(16, 'Наш магазин', 'shop', 'shop', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(17, 'Археология Средней Азии', 'ex_central_asia_archeology', 'exhibition', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(18, 'Индия', 'ex_india', 'exhibition', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(19, 'Иран', 'ex_iran', 'exhibition', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(20, 'Сибирь и Крайний Север', 'ex_siberia_and_far_north', 'exhibition', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(21, 'Кавказ, Средняя Азия и Казахстан', 'ex_caucasus_central_asia_and_kazakhstan', 'exhibition', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(22, 'Китай', 'ex_china', 'exhibition', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(23, 'Корея', 'ex_korea', 'exhibition', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(24, 'Особая кладовая. Археология', 'ex_special pantry_archeology', 'exhibition', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(25, 'Н. К. и С. Н. Рерихи', 'ex_roerichs', 'exhibition', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(26, 'Центральная Азия', 'ex_central_asia', 'exhibition', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(27, 'Юго-Восточная Азия', 'ex_southeast_asia', 'exhibition', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(28, 'Япония', 'ex_japan', 'exhibition', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(29, 'Выставка одного экспоната', 'one_exhibit', 'events', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(30, 'Лекции', 'lectures', 'events', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(31, 'Кружкии', 'sections', 'events', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1),
(32, 'Музей детям', 'for_kids', 'events', NULL, NULL, NULL, 1, 'ru', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `mu_pictures`
--

CREATE TABLE IF NOT EXISTS `mu_pictures` (
`id` int(11) unsigned NOT NULL,
  `file` varchar(128) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `size` varchar(16) DEFAULT NULL,
  `ext` varchar(8) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `cover` enum('0','1') DEFAULT NULL,
  `tempexhibition_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `oneexhibit_id` int(11) DEFAULT NULL,
  `new_id` int(11) DEFAULT NULL,
  `publication_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Дамп данных таблицы `mu_pictures`
--

INSERT INTO `mu_pictures` (`id`, `file`, `name`, `size`, `ext`, `status`, `date`, `cover`, `tempexhibition_id`, `event_id`, `oneexhibit_id`, `new_id`, `publication_id`, `page_id`) VALUES
(5, 'c653af378827aade5aabc26cce6fce67.jpg', 'c653af378827aade5aabc26cce6fce67', '2941043', 'jpg', NULL, '2014-09-17 14:00:46', '1', 1, NULL, NULL, NULL, NULL, NULL),
(6, 'd4696211b0dec56677333c895b1b73f8.jpg', 'd4696211b0dec56677333c895b1b73f8', '6643581', 'jpg', NULL, '2014-09-17 20:43:37', '1', 8, NULL, NULL, NULL, NULL, NULL),
(7, 'a9e8a76f7f1bca84ed89ba06b25b4314.png', 'a9e8a76f7f1bca84ed89ba06b25b4314', '16838', 'png', NULL, '2014-09-19 14:19:09', '0', 1, NULL, NULL, NULL, NULL, NULL),
(8, '64bf8f536fd41b0a2eab017bf711377a.jpg', '64bf8f536fd41b0a2eab017bf711377a', '689313', 'jpg', NULL, '2014-09-19 15:19:07', '1', NULL, NULL, 1, NULL, NULL, NULL),
(10, '3c0fa9c681da59ba758b7e7a229b58ee.jpg', '3c0fa9c681da59ba758b7e7a229b58ee', '1605991', 'jpg', NULL, '2014-09-22 12:08:11', '1', NULL, NULL, NULL, 1, NULL, NULL),
(11, '009947494398b1d112bc8b99b06f4ea1.jpg', '009947494398b1d112bc8b99b06f4ea1', '4842620', 'jpg', NULL, '2014-09-22 12:12:05', '1', NULL, NULL, NULL, 2, NULL, NULL),
(12, 'bbe59a292f26757b6be1188c724f22a5.jpg', 'bbe59a292f26757b6be1188c724f22a5', '980671', 'jpg', NULL, '2014-09-22 12:13:45', '1', NULL, NULL, NULL, 3, NULL, NULL),
(13, '5e60a25213032e9819caba1e8ad26d1d.jpg', '5e60a25213032e9819caba1e8ad26d1d', '1782690', 'jpg', NULL, '2014-09-22 12:14:58', '1', NULL, NULL, NULL, 4, NULL, NULL),
(14, 'a55bd9ecdeb39e52c0ce7a1031f166c6.jpg', 'a55bd9ecdeb39e52c0ce7a1031f166c6', '54257', 'jpg', NULL, '2014-09-22 12:47:00', '1', NULL, NULL, NULL, NULL, 1, NULL),
(15, 'ef1bf28c198fc45d5952c43fc5bece68.jpg', 'ef1bf28c198fc45d5952c43fc5bece68', '76442', 'jpg', NULL, '2014-09-22 12:51:53', '1', NULL, NULL, NULL, NULL, 2, NULL),
(16, '13a4e9eb48dc8e53281e99528bbb855c.jpg', '13a4e9eb48dc8e53281e99528bbb855c', '463378', 'jpg', NULL, '2014-10-07 18:52:50', '1', 4, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `mu_pollanswers`
--

CREATE TABLE IF NOT EXISTS `mu_pollanswers` (
`id` int(11) unsigned NOT NULL,
  `poll_id` int(11) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `user_answer` enum('0','1') DEFAULT '0',
  `result` int(11) DEFAULT '0',
  `result_percent` int(11) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `mu_pollanswers`
--

INSERT INTO `mu_pollanswers` (`id`, `poll_id`, `text`, `user_answer`, `result`, `result_percent`) VALUES
(1, 1, 'Раз в год', '0', 25, 50),
(2, 1, 'Каждый месяц', '0', 10, 19),
(4, 1, 'Ни разу не ходил', '1', 1, 1),
(5, 1, 'Только по праздникам', '1', 1, 1),
(9, 2, 'Да', '0', 0, 0),
(10, 2, 'Нет', '0', 0, 0),
(11, 2, 'Что такое Персия?', '0', 0, 0),
(12, 3, '100 баксов', '0', 0, 0),
(13, 3, 'Бесценно', '0', 0, 0),
(14, 1, 'Каждый день, как на работу', '0', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `mu_polls`
--

CREATE TABLE IF NOT EXISTS `mu_polls` (
`id` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `user_answer` enum('0','1') DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `mu_polls`
--

INSERT INTO `mu_polls` (`id`, `title`, `status`, `date`, `date_end`, `user_answer`) VALUES
(1, 'Как часто вы ходите в музеи?', 1, '2014-09-19 16:18:34', '2014-10-01', '1'),
(2, 'Нравится ли вам искусство Древней Персии?', 0, '2014-09-22 11:21:38', '2014-09-30', '1');

-- --------------------------------------------------------

--
-- Структура таблицы `mu_publications`
--

CREATE TABLE IF NOT EXISTS `mu_publications` (
`id` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `short_desc` varchar(255) DEFAULT NULL,
  `text` text,
  `date` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `url` varchar(128) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT ''
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `mu_publications`
--

INSERT INTO `mu_publications` (`id`, `title`, `short_desc`, `text`, `date`, `status`, `url`, `meta_title`, `keywords`, `description`) VALUES
(1, 'М.В.Кулланда. Османское художественное ремесло второй половины XVI-XIX вв.: Эволюция производства как отражение перемен в жизни империи. – Москва: ГМВ, 2014. – 216 с.: ил.', 'Работа посвящена османскому художественному ремеслу и его эволюции на протяжении XVI-XIX вв. и представляет собой попытку понять, как процессы, происходившие в это время в ремесленном производстве, отразили те или иные черты жизни османского общества и пр', 'Работа посвящена османскому художественному ремеслу и его эволюции на протяжении XVI-XIX вв. и представляет собой попытку понять, как процессы, происходившие в это время в ремесленном производстве, отразили те или иные черты жизни османского общества и происходившие в ней перемены. В качестве основных автором используются материальные источники – предметы турецкого декоративно-прикладного искусства османского времени, хранящиеся в музейных коллекциях, прежде всего в собрании Государственного музея Востока (Москва). Специальное внимание уделено проблеме атрибуции художественных изделий османского времени, традициям, нашедшим отражение в их оформлении, а также вопросам, связанным со степенью зависимости ремесленного производства от политики центральных властей и вкусов султанского двора. Для искусствоведов, историков, специалистов по культуре Востока.', '2014-09-22 12:42:09', 0, NULL, NULL, NULL, ''),
(2, 'М.М.Бронштейн, Ю.А.Широков Резная кость Якутии и Таймыра из собрания Государственного музея Востока. Каталог коллекции // М.: ГМВ, 2014 &ndash; 92 с.: с ил.', 'В каталоге помещены фотографии и описания не публиковавшихся ранее 70 изделий, вырезанных преимущественно из бивня мамонта народными художниками двух обширных ареалов Северной Азии, Якутии и Таймыра, в период с конца XIX века до начала XX столетия.', 'В каталоге помещены фотографии и описания не публиковавшихся ранее 70 изделий, вырезанных преимущественно из бивня мамонта народными художниками двух обширных ареалов Северной Азии, Якутии и Таймыра, в период с конца XIX века до начала XX столетия. Несмотря на небольшой объем и узкие хронологические рамки коллекции, она обладает несомненной ценностью. Коллекция включает в себя ряд высокохудожественных произведений и дает представление об основных направлениях в развитии косторезного дела якутов, долган, других северных этносов в эпоху, когда в их культуре происходили кардинальные, нередко драматичные перемены. Для искусствоведов, этнологов, работников музеев, коллекционеров и всех, кому небезразлична судьба народов, живущих в экстремально суровых условиях Севера.', '2014-09-22 12:51:53', 1, '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `mu_roles`
--

CREATE TABLE IF NOT EXISTS `mu_roles` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `mu_roles`
--

INSERT INTO `mu_roles` (`id`, `name`, `description`) VALUES
(1, 'login', 'Login privileges, granted after account confirmation'),
(2, 'admin', 'Administrative user, has access to everything.');

-- --------------------------------------------------------

--
-- Структура таблицы `mu_roles_users`
--

CREATE TABLE IF NOT EXISTS `mu_roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mu_roles_users`
--

INSERT INTO `mu_roles_users` (`user_id`, `role_id`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `mu_settings`
--

CREATE TABLE IF NOT EXISTS `mu_settings` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `mu_settings`
--

INSERT INTO `mu_settings` (`id`, `name`, `value`, `status`, `date`) VALUES
(1, 'header_message', '25 ноября музей будет работать с 13:00 до 18:00. Касса работать не будет.', 1, '2014-11-25 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `mu_tempexhibitions`
--

CREATE TABLE IF NOT EXISTS `mu_tempexhibitions` (
`id` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `short_desc` text,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `date_added` datetime DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `mu_tempexhibitions`
--

INSERT INTO `mu_tempexhibitions` (`id`, `title`, `url`, `short_desc`, `date_start`, `date_end`, `status`, `date_added`, `meta_title`, `keywords`, `description`) VALUES
(1, 'Симфония человека и Небес. Живопись Чжоу Шаохуа', NULL, 'Выставка познакомит зрителей с традицией изображения жизни Будды в храмовой росписи Мьянмы. Около ста большеформатных фотографий позволят проследить эволюцию стилей культовой живописи XII-XIX вв. на примере ключевых эпизодов из жизнеописания основателя буддизма &ndash; от рождения до нирваны.', '2014-09-10', '2014-10-20', 1, NULL, NULL, NULL, NULL),
(2, 'Жизнь Будды в искусстве Мьянмы. Из фотоархива Алексея Кириченко', NULL, NULL, '2014-08-10', '2014-09-12', 1, NULL, NULL, NULL, NULL),
(3, 'Рашид Доминов. Приглашение к путешествию', NULL, NULL, '2014-08-25', '2014-09-30', 0, NULL, NULL, NULL, NULL),
(4, 'Фестиваль &laquo;Египетская культурная инициатива&raquo;', '', '', '2014-09-19', '2014-11-07', 1, NULL, '', '', ''),
(5, 'Наоми Маки: Превозмогая эфемерность бытия', '', 'С 25 апреля по 18 мая 2014 года в Государственном музее Востока будет проходить выставка &laquo;Наоми Маки: превозмогая эфемерность бытия&raquo;', '2014-09-15', '2014-10-15', 0, NULL, 'Наоми Маки: Превозмогая эфемерность бытия', '', ''),
(6, 'Наоми Маки: Превозмогая эфемерность бытия', '', '', '2014-09-18', '2014-09-28', 1, NULL, '', NULL, NULL),
(8, 'В поисках истины. Расим Бабаев', '', 'С 23 октября по 10 ноября 2013 года в Государственном музее Востока будет проходить выставка &laquo;В поисках истины. Расим Бабаев&raquo;. Живопись, графика.', '2014-09-01', '2014-09-28', 1, NULL, '', '', ''),
(9, 'Выставка', '', 'ыыыыыыыыыыыыыыыыыы ыва ыва 23 23 ываыв', '2014-10-22', '2014-11-05', 0, '2014-10-22 15:10:48', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `mu_templates`
--

CREATE TABLE IF NOT EXISTS `mu_templates` (
`id` int(11) unsigned NOT NULL,
  `type` varchar(64) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `title2` varchar(255) DEFAULT NULL,
  `title3` varchar(255) DEFAULT NULL,
  `text` text,
  `text2` text,
  `date` datetime DEFAULT NULL,
  `item_model_id` int(11) DEFAULT NULL,
  `sort_order` smallint(6) DEFAULT NULL,
  `parent` varchar(64) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Дамп данных таблицы `mu_templates`
--

INSERT INTO `mu_templates` (`id`, `type`, `title`, `title2`, `title3`, `text`, `text2`, `date`, `item_model_id`, `sort_order`, `parent`) VALUES
(2, 'text', NULL, NULL, NULL, '<p><strong>15 октября пройдет круглый стол, посвященный двум научно-художественным экспедициям Николая Рериха по Центральной Азии.</strong></p><p>В дискуссии примут участие ведущие российские ученые, журналисты и общественные деятели, а также гости из зарубежных стран. Музей Николая Рериха в Нью-Йорке будет представлять его директор Даниил Энтин.</p><p>В рамках выставки состоится презентация нового альбома &laquo;Щедрый дар. Коллекция Рерихов из собрания Кэтрин Кэмпбелл в Государственном музее Востока&raquo;.</p><p><em><strong>Куратор:</strong> Дмитрий Попов</em></p>', NULL, '2014-09-26 15:00:46', 4, 1, 'tempexhibition'),
(4, 'back_text', 'Test', NULL, NULL, '<p>&nbsp;aaaaaaa</p>', NULL, '2014-10-22 19:05:05', 4, 2, 'tempexhibition'),
(10, 'quote', 'Bdfy', NULL, NULL, '', NULL, '2014-10-22 19:25:10', 4, 3, 'tempexhibition'),
(11, 'quote', '', NULL, NULL, '', NULL, '2014-10-22 19:48:59', 4, 4, 'tempexhibition'),
(12, 'text', '', NULL, NULL, '<p>aaaa</p>', NULL, '2014-10-22 19:49:07', 4, 5, 'tempexhibition'),
(13, 'back_text', '', NULL, NULL, '<p>aaaa</p>', NULL, '2014-10-22 19:49:16', 4, 6, 'tempexhibition'),
(14, 'quote', '', NULL, NULL, '<p>aaaa</p>', NULL, '2014-10-22 19:49:23', 4, 7, 'tempexhibition'),
(23, 'text', '', NULL, NULL, '<p>&lt;h1&gt;sdf&lt;/h1&gt;</p>', NULL, '2014-10-24 15:32:09', 4, 8, 'tempexhibition'),
(24, 'back_text', 'sdf', NULL, NULL, '<p>&lt;h1&gt;sdf&lt;/h1&gt;</p>', NULL, '2014-10-24 15:52:58', 4, 9, 'tempexhibition'),
(26, 'text', '', '', '', '<h5>1111222444</h5>\n<p>ыва</p>\n<p>ыва</p>\n<p>ыва</p>\n<p>ыва</p>\n<h4>ываыва</h4>\n<p>ва</p>\n<p>ва</p>', '', '2014-10-24 16:55:56', 4, 10, 'tempexhibition'),
(27, 'text', '', '', '', '<p>фыва</p>', '', '2014-10-24 18:40:19', 4, 11, 'tempexhibition'),
(28, 'text', '', '', '', '<p>sd</p>', '', '2014-10-25 16:55:19', 4, 12, 'tempexhibition'),
(29, 'text', '', '', '', '<p>фва</p>', '', '2014-10-25 17:07:30', 4, 13, 'tempexhibition'),
(36, 'text', '', '', '', '<p>1</p>', '', '2014-10-29 12:47:35', 4, 1, 'page'),
(37, 'text', '', '', '', '<p>2</p>', '', '2014-10-29 13:04:32', 4, 2, 'page'),
(38, 'text', '', '', '', '<p>234</p>', '', '2014-10-29 13:19:09', 4, 3, 'page'),
(39, 'text', '', '', '', '<p>234asdfsdf</p>', '', '2014-10-29 15:27:07', 4, 4, 'page'),
(40, 'text', '', '', '', '<p>Новый цфаыва</p>', '', '2014-10-29 15:32:23', 4, 5, 'page'),
(41, 'text', '', '', '', '<p>Новый цфаыва234234</p>', '', '2014-10-29 15:32:36', 4, 6, 'page');

-- --------------------------------------------------------

--
-- Структура таблицы `mu_users`
--

CREATE TABLE IF NOT EXISTS `mu_users` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `lastname` varchar(64) DEFAULT NULL,
  `fathername` varchar(64) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `username` varchar(32) NOT NULL DEFAULT '',
  `password` varchar(64) NOT NULL,
  `logins` int(10) unsigned NOT NULL DEFAULT '0',
  `last_login` int(10) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `mu_users`
--

INSERT INTO `mu_users` (`id`, `name`, `lastname`, `fathername`, `title`, `email`, `username`, `password`, `logins`, `last_login`, `date`) VALUES
(1, 'Администратор', 'Рустам', NULL, 'Администратор', 'rustam@kuzmani.ru', 'admin', 'c2b45490f2ad894fc7e6f15cccc45182ba2ded04b1ecf1b468a06bb4e721ff15', 14, 1413292206, '2014-06-24 01:17:03');

-- --------------------------------------------------------

--
-- Структура таблицы `mu_user_tokens`
--

CREATE TABLE IF NOT EXISTS `mu_user_tokens` (
`id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(40) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `mu_user_tokens`
--

INSERT INTO `mu_user_tokens` (`id`, `user_id`, `user_agent`, `token`, `created`, `expires`) VALUES
(1, 1, '8227ff9f0da205ac15c85cfce5e1039a1a104460', '876b964008fdea0f7f343ba91ecd4c55b8384775', 1410345830, 1411555430),
(2, 1, 'a67eab9181b748695b2cf9da483f09eb91e89a53', 'ce3cdb4045128089d056f484c2896ba7bb974a05', 1410530599, 1411740199),
(3, 1, 'b4d83ea37f26439f00198cae9ffea6b954ed9d08', 'b9b395b15b9866ca19207d98877733e9cb12616a', 1411028968, 1412238568),
(4, 1, '14200a9be4e608e318e08ae58f235a42abf56e90', '872f82a87e9b81eb46dae99962114bd9779358c0', 1411553109, 1412762709),
(5, 1, 'c50a1e25525c1d5d8e65803b4b97a4100e514e6f', '12f15e52ea85e0c328fe02a216291bcc9f270b7a', 1413292206, 1414501806);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mu_eventdates`
--
ALTER TABLE `mu_eventdates`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mu_events`
--
ALTER TABLE `mu_events`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mu_lectures`
--
ALTER TABLE `mu_lectures`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mu_news`
--
ALTER TABLE `mu_news`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mu_oneexhibits`
--
ALTER TABLE `mu_oneexhibits`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mu_pages`
--
ALTER TABLE `mu_pages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mu_pictures`
--
ALTER TABLE `mu_pictures`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mu_pollanswers`
--
ALTER TABLE `mu_pollanswers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mu_polls`
--
ALTER TABLE `mu_polls`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mu_publications`
--
ALTER TABLE `mu_publications`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mu_roles`
--
ALTER TABLE `mu_roles`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `uniq_name` (`name`);

--
-- Indexes for table `mu_roles_users`
--
ALTER TABLE `mu_roles_users`
 ADD PRIMARY KEY (`user_id`,`role_id`), ADD KEY `fk_role_id` (`role_id`);

--
-- Indexes for table `mu_settings`
--
ALTER TABLE `mu_settings`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mu_tempexhibitions`
--
ALTER TABLE `mu_tempexhibitions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mu_templates`
--
ALTER TABLE `mu_templates`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mu_users`
--
ALTER TABLE `mu_users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `uniq_username` (`username`), ADD UNIQUE KEY `uniq_email` (`email`);

--
-- Indexes for table `mu_user_tokens`
--
ALTER TABLE `mu_user_tokens`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `uniq_token` (`token`), ADD KEY `fk_user_id` (`user_id`), ADD KEY `expires` (`expires`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mu_eventdates`
--
ALTER TABLE `mu_eventdates`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mu_events`
--
ALTER TABLE `mu_events`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mu_lectures`
--
ALTER TABLE `mu_lectures`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mu_news`
--
ALTER TABLE `mu_news`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mu_oneexhibits`
--
ALTER TABLE `mu_oneexhibits`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mu_pages`
--
ALTER TABLE `mu_pages`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `mu_pictures`
--
ALTER TABLE `mu_pictures`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `mu_pollanswers`
--
ALTER TABLE `mu_pollanswers`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `mu_polls`
--
ALTER TABLE `mu_polls`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mu_publications`
--
ALTER TABLE `mu_publications`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mu_roles`
--
ALTER TABLE `mu_roles`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mu_settings`
--
ALTER TABLE `mu_settings`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mu_tempexhibitions`
--
ALTER TABLE `mu_tempexhibitions`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `mu_templates`
--
ALTER TABLE `mu_templates`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `mu_users`
--
ALTER TABLE `mu_users`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mu_user_tokens`
--
ALTER TABLE `mu_user_tokens`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `mu_roles_users`
--
ALTER TABLE `mu_roles_users`
ADD CONSTRAINT `mu_roles_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `mu_users` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `mu_roles_users_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `mu_roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mu_user_tokens`
--
ALTER TABLE `mu_user_tokens`
ADD CONSTRAINT `mu_user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `mu_users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
