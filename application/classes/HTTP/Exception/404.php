<?php defined('SYSPATH') or die('No direct script access.');

class HTTP_Exception_404 extends Kohana_HTTP_Exception_404 {

	/**
	 * Generate a Response for the 404 Exception.
	 *
	 * The user should be shown a nice 404 page.
	 *
	 * @return Response
	 */
	public function get_response()
	{
		$styles['assets/css/bootstrap.min.css']			= 'screen';
		$styles['assets/css/icons.css']				= 'screen';
		$styles['assets/css/non-responsive.css']		= 'screen';
		$styles['assets/css/style.css']				= 'screen';
		$styles['http://fonts.googleapis.com/css?family=Roboto+Slab:400,300,100,700&subset=latin,cyrillic,cyrillic-ext'] = 'screen';

		$scripts[] = 'assets/js/jquery-2.1.1.js';
		$scripts[] = 'assets/js/respond.min.js';
		$scripts[] = 'assets/js/bootstrap.min.js';
		$scripts[] = 'assets/js/common.js';

		$view = View::factory('errors/404');

		// Remembering that `$this` is an instance of HTTP_Exception_404
		$view->message = $this->getMessage();
		$response = Response::factory()->status(404)->body($view->render());

		return $response;
	}
}
?>