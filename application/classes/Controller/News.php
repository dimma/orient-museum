<?php defined('SYSPATH') or die('No direct script access.');

class Controller_News extends Controller_Layout {

	public function action_index()
	{
		$news = ORM::factory('New')->where('status', '=', 1)->order_by('date', 'DESC')->reset(FALSE);
		$data = array(
			'title'	=> 'Новости',
			'news'	=> $news->find_all(),
			'total'	=> $news->count_all(),
		);
		$this->template->title		= $data['title'];
		$this->template->content	= View::factory('front/news/index', $data);
	}

	public function action_info()
	{
		$data = array(
			'title'	=> 'Новости',
			'item'	=> ORM::factory('New', $this->request->param('id'))->where('status', '=', 1),
		);

		if (!empty($data['item']->id))
		{
			$this->template->title		= $data['item']->title;
			$this->template->content	= View::factory('front/news/info', $data);
		}
		else
		{
			Controller::redirect('/news');
		}
	}

}
