<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Layout extends Controller_Template
{

	static public $template_types = array(
		'text'			=> 'Текст',
		'back_text'		=> 'Текст с фоном',
		'quote'			=> 'Цитата',
		'two_cols'		=> 'Две колонки текста',
		'video'			=> 'Видео',
		'photo_comment'		=> 'Фото с комментарием',
		'two_photo_comment'	=> 'Два фото с комментарием',
		'small_photo_text'	=> 'Маленькое фото + текст',
		'gallery'		=> 'Галерея',
		'gallery_modal'		=> 'Галерея в отдельном окне',
		'partners'		=> 'Партнёры',
	);

	# Layout controller
	# ---------------------------------------------------------------------------------------------- #
	public $template = 'admin/layout';

	public function before()
	{
		parent::before();

		# Смотрим, залогинен ли юзер
		$auth = Auth::instance();

		if($this->request->controller() !== 'Recovery')
		{
			if($auth->logged_in())
			{
				$this->user = Auth::instance()->get_user();
			}
			# Если нет, то просим его авторизоваться
			else
			{
				if($this->request->controller() !== 'Auth' && $this->request->action() !== 'login')
				{
					Controller::redirect('/admin/login');
				}
			}
		}
		else
		{
			$this->user = ($auth->logged_in()) ? Auth::instance()->get_user() : FALSE;
		}

		# Параметры шаблона
		if($this->auto_render)
		{
			//$detect = new MobileDetect();

			$this->template->title				= '';
			$this->template->window_title		= '';
			$this->template->meta_keywords		= '';
			$this->template->meta_description	= '';
			$this->template->meta_copyright		= '';
			$this->template->meta_author		= '';
			$this->template->styles				= array();
			$this->template->scripts			= array();
			$this->template->controller			= UTF8::strtolower($this->request->controller());
			$this->template->action				= $this->request->action();
			$this->template->main_page			= ($this->request->controller() == 'main' && $this->request->action() == 'index') ? TRUE : FALSE;
			$this->template->content			= '';
			$this->template->menu				= array();
			$this->template->user_title			= ($auth->logged_in() && isset($this->user)) ? $this->user->name : FALSE;
			$this->template->user				= ($auth->logged_in() && isset($this->user)) ? $this->user : FALSE;
			$this->template->icon               = '';
			$this->template->rights				= array();
			$this->template->title_content		= '';
			$this->template->breadcrumbs		= array();

			# Rights
			if($this->template->user)
			{
				foreach(ORM::factory('Role')->find_all() as $role)
				{
					$this->template->rights[$role->name] = FALSE;
				}

				foreach($this->user->roles->find_all() as $role)
				{
					$this->template->rights[$role->name] = TRUE;
				}
			}

			# Menu
			foreach(Kohana::$config->load('admin_menu') as $k => $v)
			{
				$this->template->menu[$k] = $v;
			}

			# Current page
			$menu = Kohana::$config->load('admin_menu');

			if(isset($menu[$this->template->controller]))
			{
				$this->template->title = $menu[$this->template->controller]['name'];
				$this->template->window_title = $menu[$this->template->controller]['name'].' | Site Expo admin';
				//$this->template->icon = $menu[$this->template->controller]['icon'];
			}
		}
	}

	public function after()
	{
		if($this->auto_render)
		{
			# Подключаем стандартные стили и скрипты
			$styles['assets/admin/css/bootstrap.min.css'] = 'screen';
			$styles['assets/admin/font-awesome/css/font-awesome.css'] = 'screen';
			$styles['assets/admin/css/animate.css'] = 'screen';
			$styles['assets/admin/css/plugins/switchery/switchery.css'] = 'screen';
			$styles['assets/admin/css/plugins/datapicker/datepicker3.css'] = 'screen';
			$styles['assets/admin/css/plugins/iCheck/custom.css'] = 'screen';
			$styles['http://fonts.googleapis.com/css?family=Roboto+Slab:400,300,100,700&subset=latin,cyrillic,cyrillic-ext'] = 'screen';
			$styles['assets/admin/css/style.css'] = 'screen';
			$styles['assets/admin/css/layout.css'] = 'screen';

			$scripts[] = 'assets/admin/js/jquery-1.11.1.min.js';
			$scripts[] = 'assets/admin/js/bootstrap.min.js';
			$scripts[] = 'assets/admin/js/plugins/metisMenu/jquery.metisMenu.js';
			$scripts[] = 'assets/admin/js/inspinia.js';
			$scripts[] = 'assets/admin/js/plugins/pace/pace.min.js';
			$scripts[] = 'assets/admin/js/plugins/autosize/jquery.autosize.min.js';
			$scripts[] = 'assets/admin/js/plugins/switchery/switchery.js';
			$scripts[] = 'assets/admin/js/plugins/datapicker/bootstrap-datepicker.js';
			$scripts[] = 'assets/admin/js/plugins/datapicker/bootstrap-datepicker.ru.js';
			$scripts[] = 'assets/admin/js/plugins/iCheck/icheck.min.js';
			$scripts[] = 'assets/admin/js/plugins/bootbox/bootbox.min.js';
			$scripts[] = 'assets/admin/js/plugins/tinymce/tinymce.min.js';
			$scripts[] = 'assets/admin/js/plugins/tinymce/jquery.tinymce.min.js';
			//$scripts[] = 'assets/admin/js/plugins/ckeditor/ckeditor.js';	// CKEditor
			//$scripts[] = 'assets/admin/js/plugins/ckeditor/jquery.js';
			$scripts[] = 'assets/admin/js/common.js';

			# jQuery file upload
			$scripts[] = 'assets/admin/js/plugins/fileupload/jquery.ui.widget.js';
			$scripts[] = 'assets/admin/js/plugins/fileupload/jquery.iframe-transport.js';
			$scripts[] = 'assets/admin/js/plugins/fileupload/jquery.fileupload.js';

			$this->template->styles		= array_merge($this->template->styles, $styles);
			$this->template->scripts	= array_merge($this->template->scripts, $scripts);
		}

		parent::after();
	}
}
?>