<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Ajax extends Controller_Admin_Adminajax {

	public function action_index()
	{
		exit;
	}

	# Add item
	public function action_add_item()
	{
		if($this->request->post())
		{
			$json = array();
			$model = ucfirst($this->request->post('item_model'));

			try
			{
				$post = $this->request->post();

				# Temporary exhibitions
				if($model == 'Tempexhibition')
				{
					$post['date_start'] = date('Y-m-d', strtotime($post['date_start']));
					$post['date_end'] = date('Y-m-d', strtotime($post['date_end']));
					$post['date_added'] = Date::formatted_time();
				}
				# Event
				if($model == 'Event')
				{
					$post['date_added'] = Date::formatted_time();
					$post['kids']  = ($this->request->post('kids')) ? '1' : '0';
					$post['date']  = date('Y-m-d', strtotime($post['date']));
					$post['date'] .= ' '.$post['hour'].':'.$post['minute'];

					if($post['apply_max'] == '')
					{
						unset($post['apply_max']);
					}
				}
				# One exhibit
				if($model == 'Oneexhibit')
				{
					$post['date_added'] = Date::formatted_time();
				}
				# Polls
				if($model == 'Poll')
				{
					$post['user_answer'] = ($this->request->post('user_answer')) ? '1' : '0';
					$post['date'] = Date::formatted_time();
					$post['date_end'] = date('Y-m-d-', strtotime($post['date_end']));
				}
				# News
				if($post['item_model'] == 'new')
				{
					$post['date'] = date('Y-m-d', strtotime($post['date']));
					$post['date_added'] = Date::formatted_time();
				}
				# Publications
				if($post['item_model'] == 'publication')
				{
					$post['date'] = Date::formatted_time();
				}

				$item = ORM::factory($model)->values($post)->save();

				# New event dates
				if($model == 'Event')
				{
					if(isset($post['new_eventdates']))
					{
						foreach($post['new_eventdates']['date'] as $k => $v)
						{
							ORM::factory('Eventdate')->values(array(
								'event_id' => $item->id,
								'date' => date('Y-m-d', strtotime($v)).' '.$post['new_eventdates']['hour'][$k].':'.$post['new_eventdates']['minute'][$k]
							))->save();
						}
					}
				}

				# Polls
				if($model == 'Poll')
				{
					foreach($post['newanswers'] as $value)
					{
						try
						{
							ORM::factory('Pollanswer')->values(array(
								'poll_id' => $item->id,
								'text' => $value
							))->save();
						}
						catch(ORM_Validation_Exception $e)
						{

						}
					}
				}

				# Item pictures
				if($this->request->post('pictures'))
				{
					$uploadFolder	= 'assets/upload/';
					$folder		= 'items_pictures';
					foreach ($post['pictures'] as $name => $ext)
					{
						$img = Image::factory(DOCROOT.$uploadFolder.'temp/'.$name.'.'.$ext);
						$img->save(DOCROOT.$uploadFolder.$folder.'/'.$name.'.'.$ext);
						$img->save(DOCROOT.$uploadFolder.$folder.'/'.$name.'_200_200.'.$ext);
						$img->save(DOCROOT.$uploadFolder.$folder.'/'.$name.'_1000.'.$ext);

						$imgPreview = Image::factory(DOCROOT.$uploadFolder.$folder.'/'.$name.'_200_200.'.$ext);
						if ($imgPreview->width > $imgPreview->height)
						{
							$imgPreview->resize(NULL, 200);
						}
						elseif ($imgPreview->width < $imgPreview->height)
						{
							$imgPreview->resize(200, NULL);
						}
						else
						{
							$imgPreview->resize(200, 200);
						}
						$imgPreview->save();

						$imgBig = Image::factory(DOCROOT.$uploadFolder.$folder.'/'.$name.'_1000.'.$ext);
						$imgBig->resize(1000, NULL);
						$imgBig->save();

						$fk = $this->request->post('item_model').'_id';
						$cover = (ORM::factory('Picture')->where($fk,'=',$item->id)->count_all() == 0) ? '1' : '0';
						$json['cover'] = ($cover) ? 'active' : '';

						$file_data = array(
							'name'	=> $name,
							$fk	=> $item->id,
							'file'	=> $name.'.'.$ext,
							'size'	=> filesize(DOCROOT.$uploadFolder.$folder.'/'.$name.'.'.$ext),
							'ext'	=> $ext,
							'date'	=> Date::formatted_time(),
							'cover'	=> $cover,
						);

						ORM::factory('Picture')->values($file_data)->save();

						unlink(DOCROOT.$uploadFolder.'temp/'.$name.'.'.$ext);
					}
				}

				$json['item_id'] = $item->id;
				$json['status'] = 1;
			}
			catch(ORM_Validation_Exception $e)
			{
				$json['status'] = 0;
				$json['errors'] = $e->errors();
			}

			echo json_encode($json);
		}

		exit;
	}

	# Update item
	public function action_update_item()
	{
		$post = $this->request->post();
		if (!empty($post['item_id']))
		{
			$item = ORM::factory(ucfirst($post['item_model']), $post['item_id']);
			if ($item->id)
			{
				$json = array();
				
				if (!empty($post['date']))
				{
					$post['date'] = date('Y-m-d', strtotime($post['date']));
				}

				switch ($post['item_model'])
				{
					case 'tempexhibition':
						$post['date_start']	= Date::format($post['date_start'], 'Y-m-d');
						$post['date_end']	= Date::format($post['date_end'], 'Y-m-d');
					break;

					case 'event':
						$post['kids']	= (!empty($post['kids']) ? '1' : '0');
						$post['date']	= date('Y-m-d', strtotime($post['date']));
						$post['date']	.= ' '.$post['hour'].':'.$post['minute'];

						# New event dates
						if(isset($post['new_eventdates']))
						{
							foreach($post['new_eventdates']['date'] as $k => $v)
							{
								ORM::factory('Eventdate')->values(array(
									'event_id' => $post['item_id'],
									'date' => date('Y-m-d', strtotime($v)).' '.$post['new_eventdates']['hour'][$k].':'.$post['new_eventdates']['minute'][$k]
								))->save();
							}
						}

						if(isset($post['eventdates']))
						{
							foreach($post['eventdates'] as $k => $v)
							{
								ORM::factory('Eventdate', $k)->values(array('date' => date('Y-m-d', strtotime($v['date'])).' '.$v['hour'].':'.$v['minute']))->update();
							}
						}

						if($post['apply_max'] == '')
						{
							unset($post['apply_max']);
						}
					break;

					case 'poll':
						$post['user_answer']	= (!empty($post['user_answer']) ? '1' : '0');
						$post['date_end']	= date('Y-m-d', strtotime($post['date_end']));

						# Answers
						foreach ($post['pollanswers'] as $id => $value)
						{
							ORM::factory('Pollanswer', $id)->values(array('text' => $value))->update();
						}

						# New answers
						if (isset($post['newanswers']))
						{
							foreach ($post['newanswers'] as $value)
							{
								ORM::factory('Pollanswer')->values(array('poll_id' => $item->id, 'text' => $value))->save();
							}
						}
					break;

					case 'new':
						$post['date'] = date('Y-m-d', strtotime($post['date']));
					break;

					case 'page':
						$post['anchor']	= (!empty($post['anchor']) ? '1' : '0');
					break;
				}

				try
				{
					$item->values($post)->update();
					$json['status'] = 1;
				}
				catch(ORM_Validation_Exception $e)
				{
					$json['errors'] = $e->errors();
					$json['status'] = 0;
				}

				echo json_encode($json);
			}
		}

		exit;
	}

	# Update item status
	public function action_update_item_status()
	{
		if($this->request->post())
		{
			$modelName	= ucfirst($this->request->post('item_type'));
			$model		= ORM::factory($modelName, $this->request->post('item_id'));

			if ($model->id)
			{
				// Unset all active polls
				if ($this->request->post('item_type') === 'poll')
				{
					DB::update($this->request->post('item_type') . 's')->set(array('status' => 0))->where('status', '=', 1)->execute();
				}
				$model->values(array('status' => $this->request->post('status')))->update();
			}
		}

		exit;
	}

	# Delete item
	public function action_delete_item()
	{
		if($this->request->post('item_model') && $this->request->post('item_id'))
		{
			if($this->request->post('item_model') == 'template')
			{
				$template = ORM::factory('Template', $this->request->post('item_id'));
				$parent_id = $template->item_model_id;
			}

			ORM::factory(ucfirst($this->request->post('item_model')), $this->request->post('item_id'))->delete();

			# Template
			if($this->request->post('item_model') == 'template')
			{
				$templ = ORM::factory('Template')
					->where('item_model_id', '=', $parent_id)
					->and_where('parent', '=', $this->request->post('item_model'))
					->find_all();
				$i = 1;
				foreach ($templ as $tpl)
				{
					$tpl->values(array('sort_order' => $i))->update();
					$i++;
				}
			}
		}

		exit;
	}

	# Delete picture
	public function action_delete_pic()
	{
		$uploadFolder = 'assets/upload/';
		if($this->request->post('id'))
		{
			$picture = ORM::factory('Picture', $this->request->post('id'));
			unlink(DOCROOT.$uploadFolder.'items_pictures/'.$picture->file);
			unlink(DOCROOT.$uploadFolder.'items_pictures/'.$picture->name.'_200_200.'.$picture->ext);
			unlink(DOCROOT.$uploadFolder.'items_pictures/'.$picture->name.'_1000.'.$picture->ext);
			$picture->delete();
		}

		if($this->request->post('name') && $this->request->post('ext'))
		{
			unlink(DOCROOT.$uploadFolder.'temp/'.$this->request->post('name').'.'.$this->request->post('ext'));
			unlink(DOCROOT.$uploadFolder.'temp/'.$this->request->post('name').'_200_200.'.$this->request->post('ext'));
			unlink(DOCROOT.$uploadFolder.'temp/'.$this->request->post('name').'_1000.'.$this->request->post('ext'));
		}

		exit;
	}

	# Upload item picture
	public function action_upload_item_pic()
	{
		if (isset($_FILES['picture']['tmp_name']) && $this->request->post('item_model'))
		{
			$type = key($_FILES);

			// For multi files inputs
			if (is_array($_FILES[$type]['name']))
			{
				foreach ($_FILES[$type] as $k => $v)
				{
					$_FILES[$type][$k] = $v[0];
				}
			}

			$ext		= strtolower(pathinfo($_FILES[$type]['name'], PATHINFO_EXTENSION));
			$name		= md5(rand(11111111, 99999999) . time());
			$file_name	= $name.'.'.$ext;
			$folder		= ($this->request->post('item_id') && !$this->request->post('adding_template')) ? 'items_pictures' : 'temp';
			$uploadFolder	= 'assets/upload/';

			Upload::save($_FILES[$type], $file_name, DOCROOT.$uploadFolder.$folder);

			if ($this->request->post('item_id') && !$this->request->post('adding_template'))
			{
				$img = Image::factory(DOCROOT.$uploadFolder.$folder.'/'.$file_name);
				$img->save(DOCROOT.$uploadFolder.$folder.'/'.$name.'_200_200.'.$ext);
				$img->save(DOCROOT.$uploadFolder.$folder.'/'.$name.'_1000.'.$ext);

				$imgPreview = Image::factory(DOCROOT.$uploadFolder.$folder.'/'.$name.'_200_200.'.$ext);
				if ($imgPreview->width > $imgPreview->height)
				{
					$imgPreview->resize(NULL, 200);
				}
				elseif ($imgPreview->width < $imgPreview->height)
				{
					$imgPreview->resize(200, NULL);
				}
				else
				{
					$imgPreview->resize(200, 200);
				}
				$imgPreview->save();

				$imgBig = Image::factory(DOCROOT.$uploadFolder.$folder.'/'.$name.'_1000.'.$ext);
				$imgBig->resize(1000, NULL);
				$imgBig->save();

				$fk	= $this->request->post('item_model') . '_id';
				$itemId	= $this->request->post('item_id');
				if ($this->request->post('item_model') === 'page' && in_array($this->request->post('type'), array_keys(Controller_Admin_Layout::$template_types)))
				{
					$fk	= 'template_id';
					$itemId	= $this->request->post('template_id');
				}
				$cover = (ORM::factory('Picture')->where($fk, '=', $itemId)->count_all() == 0) ? '1' : '0';
				$json['cover'] = ($cover) ? 'active' : '';

				$file_data = array(
					'name'	=> $name,
					$fk	=> $itemId,
					'file'	=> $file_name,
					'size'	=> $_FILES[$type]['size'],
					'ext'	=> $ext,
					'date'	=> Date::formatted_time(),
					'cover'	=> $cover,
				);

				$picture = ORM::factory('Picture')->values($file_data)->save();

				$json['id']		= $picture->id;
				$json['file']		= '/'.$uploadFolder.$folder.'/'.$name.'_1000.'.$ext;
				$json['preview']	= '/'.$uploadFolder.$folder.'/'.$name.'_200_200.'.$ext;
			}
			else
			{
				$json['temp'] = 1;
			}

			$json['name']	= $name;
			$json['ext']	= $ext;
			$json['type']	= 'item_cover';

			echo json_encode($json);
		}
	}

	# Delete event date
	public function action_delete_eventdate()
	{
		if($this->request->post('id'))
		{
			ORM::factory('Eventdate', $this->request->post('id'))->delete();
		}

		exit;
	}

	# Get event dates
	public function action_get_event_dates()
	{
		if($this->request->post('id'))
		{
			$data['dates'] = ORM::factory('Eventdate')->where('event_id','=',$this->request->post('id'))->find_all();
			$json['dates'] = View::factory('admin/events/_dates', $data)->render();
			echo json_encode($json);
		}

		exit;
	}

	# Get lectures list
	public function action_get_lectures_list()
	{
		$data['items'] = ORM::factory('Lecture')->find_all();
		echo View::factory('admin/events/_lectures_list', $data);

		exit;
	}

	# Lecture info
	public function action_lecture_info()
	{
		if($this->request->param('id'))
		{
			echo View::factory('admin/events/_lecture_info_body', array('item' => ORM::factory('Lecture', $this->request->param('id'))));
		}

		exit;
	}

	# Get poll answers
	public function action_get_poll_answers()
	{
		if($this->request->param('id'))
		{
			$json['answers'] = View::factory('admin/polls/_answers', array('answers' => ORM::factory('Pollanswer')->where('poll_id','=',$this->request->param('id'))->and_where('user_answer','=','0')->find_all()))->render();
			echo json_encode($json);
		}

		exit;
	}

	# Save template
	public function action_save_template()
	{
		$json = array();

		if ($this->request->post('type') && $this->request->post('item_id') && $this->request->post('item_model'))
		{
			try
			{
				$type	= $this->request->post('type');
				$title	= $this->request->post('title');
				$tmpCnt	= ORM::factory('Template')
					->where('item_model_id', '=', $this->request->post('item_id'))
					->and_where('parent', '=', $this->request->post('item_model'))
					->count_all();

				switch ($type)
				{
					case 'partners':
						$title = implode(';', $title);
					break;

					case 'video':
						$explUrl = explode('/', $title);
						foreach ($explUrl as $i => $v)
						{
							if (in_array($v, array('youtube.com', 'www.youtube.com')))
							{
								$explNewUrl = explode('?v=', $explUrl[$i+1]);
								if (!empty($explNewUrl[1]))
								{
									$newUrl = $explNewUrl[1];
								}
							}
							elseif (in_array($v, array('youtu.be', 'www.youtu.be')))
							{
								$newUrl = $explUrl[$i+1];
							}
						}
						$title = 'http://youtube.com/embed/' . $newUrl;
					break;
				}

				$insertData = array(
					'type'		=> $type,
					'title'		=> $title,
					'title2'	=> $this->request->post('title2'),
					'title3'	=> $this->request->post('title3'),
					'text'		=> $this->request->post('text-' . $type),
					'text2'		=> $this->request->post('text2-' . $type),
					'date'		=> Date::formatted_time(),
					'item_model_id'	=> $this->request->post('item_id'),
					'parent'	=> $this->request->post('item_model'),
				);

				$templateId = $this->request->post('template_id');
				if (!empty($templateId))
				{
					ORM::factory('Template', $templateId)->values($insertData)->update();
				}
				else
				{
					$insertData['sort_order'] = $tmpCnt + 1;
					$templateId = ORM::factory('Template')->values($insertData)->save();
				}

				if ($this->request->post('pictures'))
				{
					$uploadFolder	= 'assets/upload/';
					$folder		= 'items_pictures';
					foreach ($this->request->post('pictures') as $name => $ext)
					{
						$img = Image::factory(DOCROOT.$uploadFolder.'temp/'.$name.'.'.$ext);
						$img->save(DOCROOT.$uploadFolder.$folder.'/'.$name.'.'.$ext);
						$img->save(DOCROOT.$uploadFolder.$folder.'/'.$name.'_200_200.'.$ext);
						$img->save(DOCROOT.$uploadFolder.$folder.'/'.$name.'_1000.'.$ext);

						$imgPreview = Image::factory(DOCROOT.$uploadFolder.$folder.'/'.$name.'_200_200.'.$ext);
						if ($imgPreview->width > $imgPreview->height)
						{
							$imgPreview->resize(NULL, 200);
						}
						elseif ($imgPreview->width < $imgPreview->height)
						{
							$imgPreview->resize(200, NULL);
						}
						else
						{
							$imgPreview->resize(200, 200);
						}
						$imgPreview->save();

						$imgBig = Image::factory(DOCROOT.$uploadFolder.$folder.'/'.$name.'_1000.'.$ext);
						$imgBig->resize(1000, NULL);
						$imgBig->save();

						$fk = 'template_id';
						$cover = (ORM::factory('Picture')->where($fk, '=', $templateId)->count_all() == 0) ? '1' : '0';
						$json['cover'] = ($cover) ? 'active' : '';

						$file_data = array(
							'name'	=> $name,
							$fk	=> $templateId,
							'file'	=> $name.'.'.$ext,
							'size'	=> filesize(DOCROOT.$uploadFolder.$folder.'/'.$name.'.'.$ext),
							'ext'	=> $ext,
							'date'	=> Date::formatted_time(),
							'cover'	=> $cover,
						);

						ORM::factory('Picture')->values($file_data)->save();

						unlink(DOCROOT.$uploadFolder.'temp/'.$name.'.'.$ext);
					}
				}

				$json['status'] = 1;
			}
			catch(ORM_Validation_Exception $e)
			{
				$json['status'] = 0;
				$json['errors'] = $e->errors();
			}
		}

		echo json_encode($json);

		exit;
	}

	# Get templates for item
	public function action_templates_list()
	{
		$post = $this->request->post();
		if (!empty($post['item_model']) && !empty($post['item_id']))
		{
			echo View::factory(
				'admin/templates/_item_templates_list',
				array(
					'templates' => Model_Template::getModelTemplates($post['item_model'], $post['item_id']),
				)
			);
		}

		exit;
	}

	# Get template content
	public function action_template_content()
	{
		if ($this->request->post('template_id'))
		{
			$template = ORM::factory('Template', $this->request->post('template_id'));
			echo View::factory(
				'admin/templates/' . $template->type . '_edit',
				array(
					'template'	=> $template,
					'name'		=> Controller_Admin_Layout::$template_types[$template->type],
				)
			);
		}

		exit;
	}

	public function action_hide_notify()
	{
		Session::instance()->set('hide_notify', TRUE);
		exit;
	}
}
