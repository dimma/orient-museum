<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Events extends Controller_Admin_Layout {

	public function action_index()
	{
		$data = array(
			'items'		=> ORM::factory('Event')->order_by('date','DESC')->find_all(),
			'event_types'	=> Model_Event::$event_types,
		);
		$this->template->title_content = View::factory('admin/events/_index_top');
		$this->template->content = View::factory('admin/events/index', $data);
	}

	# Event info
	public function action_info()
	{
		$data = array(
			'item'		=> ORM::factory('Event', $this->request->param('id')),
			'event_types'	=> Model_Event::$event_types,
		);

		if($data['item']->id)
		{
			$this->template->window_title = $data['item']->title;
			$this->template->title = $data['item']->title;
			$this->template->breadcrumbs = array(array('url' => 'events', 'title' => 'События'));
			$this->template->title_content = View::factory('admin/events/_info_top', array('item_id' => $data['item']->id,'status' => $data['item']->status));
			$this->template->content = View::factory('admin/events/info', $data);
		}
		else
		{
			Controller::redirect('/admin/events');
		}
	}

	# Lecture cycles
	public function action_lectures()
	{
		$data['items'] = ORM::factory('Lecture')->find_all();

		$this->template->window_title = 'Циклы лекций';
		$this->template->title = 'Циклы лекций';
		$this->template->title_content = View::factory('admin/events/_lectures_top');
		$this->template->content = View::factory('admin/events/lectures', $data);
	}

	# Add event
	public function action_add()
	{
		$data = array('event_types' => Model_Event::$event_types);
		$this->template->window_title = 'Добавление события';
		$this->template->title = 'Добавление события';
		$this->template->content = View::factory('admin/events/add', $data);
	}
}
