<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Polls extends Controller_Admin_Layout {

	public function action_index()
	{
		$data['items'] = ORM::factory('Poll')->find_all();

		$this->template->title = 'Опросы';
		$this->template->window_title = 'Опросы';
		$this->template->title_content = View::factory('admin/polls/_index_top');
		$this->template->content = View::factory('admin/polls/index', $data);
	}

	# Info
	public function action_info()
	{
		$data['item'] = ORM::factory('Poll', $this->request->param('id'));

		$answers_count = DB::select(DB::expr('SUM(result) as sum'))->where('poll_id','=',$data['item']->id)->from('pollanswers')->execute()->as_array();
		$data['answers_count'] = $answers_count[0]['sum'];

		# Answers
		$user_answers = DB::select()->from('pollanswers')->where('poll_id','=',$data['item']->id)->and_where('user_answer','=','1')->execute()->as_array();
		$best_answer = DB::select('id')->from('pollanswers')->where('poll_id','=',$data['item']->id)->and_where('user_answer','=','0')->order_by('result','DESC')->execute()->as_array();

		if(count($best_answer))
		{
			$data['best_answer'] = $best_answer[0]['id'];
		}

		$data['user_answers_count'] = 0;
		$data['user_answers_total'] = 0;
		foreach($user_answers as $k => $v)
		{
			$data['user_answers_count'] += $v['result_percent'];
			$data['user_answers_total'] += $v['result'];
		}

		$this->template->window_title = $data['item']->title;
		$this->template->title = $data['item']->title;
		$this->template->title_content = View::factory('admin/polls/_info_top', array('item_id' => $data['item']->id,'status' => $data['item']->status));
		$this->template->content = View::factory('admin/polls/info', $data);
		$this->template->breadcrumbs = array(array('url' => 'polls', 'title' => 'Опросы'));
	}

	# Add
	public function action_add()
	{
		$data = array();
		$this->template->title = 'Добавление опроса';
		$this->template->breadcrumbs = array(array('url' => 'polls', 'title' => 'Опросы'));
		$this->template->content = View::factory('admin/polls/add', $data);
	}

}
