<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Adminajax extends Controller_Template
{
	# Layout controller
	# ---------------------------------------------------------------------------------------------- #
	public $template = 'admin/ajax';

	public function before()
	{
		parent::before();

		# Смотрим, залогинен ли юзер
		$auth = Auth::instance();

		if($auth->logged_in())
		{
			$this->user = Auth::instance()->get_user();
		}
		else
		{
			exit;
		}
	}

	public function after()
	{
		parent::after();
	}
}
?>