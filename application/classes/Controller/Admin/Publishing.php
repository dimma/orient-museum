<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Publishing extends Controller_Admin_Layout {

	public function action_index()
	{
		$data['items'] = ORM::factory('Publication')->find_all();

		$this->template->title = 'Издания';
		$this->template->window_title = 'Издания';
		$this->template->title_content = View::factory('admin/publications/_index_top');
		$this->template->content = View::factory('admin/publications/index', $data);
	}

	# Info
	public function action_info()
	{
		$data['item'] = ORM::factory('Publication', $this->request->param('id'));

		$this->template->window_title = $data['item']->title;
		$this->template->title = $data['item']->title;
		$this->template->title_content = View::factory('admin/publications/_info_top', array('item_id' => $data['item']->id,'status' => $data['item']->status));
		$this->template->content = View::factory('admin/publications/info', $data);
		$this->template->breadcrumbs = array(array('url' => 'publishing', 'title' => 'Издания'));
	}

	# Add
	public function action_add()
	{
		$data = array();
		$this->template->title = 'Добавление издания';
		$this->template->breadcrumbs = array(array('url' => 'publishing', 'title' => 'Издания'));
		$this->template->content = View::factory('admin/publications/add', $data);
	}

}
