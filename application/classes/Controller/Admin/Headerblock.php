<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Headerblock extends Controller_Admin_Layout {

	public function action_index()
	{
		$data['item'] = ORM::factory('Setting',1);

		$this->template->title = 'Блок информации в шапке';
		$this->template->window_title = 'Блок информации в шапке';
		$this->template->title_content = View::factory('admin/headerblock/_top');
		$this->template->content = View::factory('admin/headerblock/index', $data);
	}

}
