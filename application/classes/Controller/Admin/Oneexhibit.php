<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Oneexhibit extends Controller_Admin_Layout {

	public function action_index()
	{
		$data['items'] = ORM::factory('Oneexhibit')->find_all();
		$data['archive'] = FALSE;

		$this->template->title = 'Выставка одного экспоната';
		$this->template->window_title = 'Выставка одного экспоната';
		$this->template->title_content = View::factory('admin/oneexhibit/_index_top');
		$this->template->content = View::factory('admin/oneexhibit/index', $data);
	}

	# Info
	public function action_info()
	{
		$data['item'] = ORM::factory('Oneexhibit', $this->request->param('id'));

		$this->template->window_title = $data['item']->title;
		$this->template->title = $data['item']->title;
		$this->template->title_content = View::factory('admin/oneexhibit/_info_top', array('item_id' => $data['item']->id,'status' => $data['item']->status));
		$this->template->content = View::factory('admin/oneexhibit/info', $data);
		$this->template->breadcrumbs = array(array('url' => 'oneexhibit', 'title' => 'Выставка одного экспоната'));
	}

	# Add
	public function action_add()
	{
		$data = array();
		$this->template->title = 'Добавление выставки одного экспоната';
		$this->template->breadcrumbs = array(array('url' => 'oneexhibit', 'title' => 'Выставка одного экспоната'));
		$this->template->content = View::factory('admin/oneexhibit/add', $data);
	}

}
