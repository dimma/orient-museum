<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_News extends Controller_Admin_Layout {

	public function action_index()
	{
		$data['items'] = ORM::factory('New')->find_all();

		$this->template->title = 'Новости';
		$this->template->window_title = 'Новости';
		$this->template->title_content = View::factory('admin/news/_index_top');
		$this->template->content = View::factory('admin/news/index', $data);
	}

	# Info
	public function action_info()
	{
		$data['item'] = ORM::factory('New', $this->request->param('id'));

		$this->template->window_title = $data['item']->title;
		$this->template->title = $data['item']->title;
		$this->template->title_content = View::factory('admin/news/_info_top', array('item_id' => $data['item']->id,'status' => $data['item']->status));
		$this->template->content = View::factory('admin/news/info', $data);
		$this->template->breadcrumbs = array(array('url' => 'news', 'title' => 'Новости'));
	}

	# Add
	public function action_add()
	{
		$data = array();
		$this->template->title = 'Добавление новости';
		$this->template->breadcrumbs = array(array('url' => 'news', 'title' => 'Новости'));
		$this->template->content = View::factory('admin/news/add', $data);
	}

}
