<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Contacts extends Controller_Admin_Layout {

	public function action_index()
	{
		Controller::redirect('/admin/contacts/ru');
	}

	# Russian
	public function action_ru()
	{
		$data['item'] = ORM::factory('Page', 1);
		$data['lang'] = 'ru';
		$this->template->window_title = 'Контакты на русском';
		$this->template->title = $this->template->window_title;
		$this->template->content = View::factory('admin/contacts/index', $data);
	}

	# English
	public function action_en()
	{
		$data['item'] = ORM::factory('Page', 2);
		$data['lang'] = 'en';
		$this->template->window_title = 'Контакты на английском';
		$this->template->title = $this->template->window_title;
		$this->template->content = View::factory('admin/contacts/index', $data);
	}

}
