<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Pages extends Controller_Admin_Layout
{

	public function action_index()
	{
		$data = array(
			'items'	=> DB::select('id', 'title', 'url', 'parent', 'status')
				->from('pages')
				->where('lang', '=', 'ru')
				->and_where('url', '!=', 'contacts')
				->order_by('id')
				->order_by('parent')
				->execute()
				->as_array(),
		);
		$this->template->content = View::factory('admin/pages/index', $data);
	}

	public function action_info()
	{
		$data = array(
			'item' => ORM::factory('Page', $this->request->param('id')),
		);

		if ($data['item']->id)
		{
			$this->template->window_title	= $data['item']->title;
			$this->template->title		= $data['item']->title;
			$this->template->breadcrumbs	= array(array('url' => 'pages', 'title' => 'Остальные страницы'));
			$this->template->content	= View::factory('admin/pages/info', $data);
		}
		else
		{
			Controller::redirect('/admin/pages');
		}
	}

}
