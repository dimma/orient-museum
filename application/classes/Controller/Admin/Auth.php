<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Auth extends Controller_Admin_Layout
{
	# Authorisation
	# ---------------------------------------------------------------------------------------------- #
	public function action_index()
	{

	}

	public function action_login()
	{
		$auth = Auth::instance();

		if($this->request->post('username') && $this->request->post('password'))
		{
			$remember = TRUE;
			$error = array();

			try
			{
				$post = Validation::factory($this->request->post())
					->rule('username', 'trim')
					->rule('username', 'not_empty')
					->rule('password', 'trim')
					->rule('password', 'not_empty');

				if($post->check() && $auth->login($post['username'], $post['password'], $remember))
				{
					Controller::redirect('/admin');
				}
				else
				{
					$error['login_falied'] = TRUE;
				}
			}
			catch (ORM_Validation_Exception $e)
			{
				//Kohana::ar($e->errors());
			}
			catch (Exception $e)
			{
				// Do your error handling
			}
		}

		if(!$auth->logged_in())
		{
			$this->template->set_filename('admin/auth/login')->bind('error', $error);
		}
		else
		{
			Controller::redirect('/admin');
		}
	}

	# Logout
	# ---------------------------------------------------------------------------------------------- #
	public function action_logout()
	{
		if(Auth::instance()->logged_in())
		{
			Auth::instance()->logout();
		}

		Controller::redirect('/admin');
	}

	# Add user
	public function action_adduser()
	{
		$pass = rand(11111111,99999999);	// 40390236

		$reg_data = array(
			'name'				=> 'Администратор',
			'email'				=> 'rustam@otono.ru',
			'username'			=> 'admin',
			'password'			=> $pass,
			'password_confirm'	=> $pass,
			'date'				=> Date::formatted_time()
		);

		try
		{
			$user = ORM::factory('user')->create_user($reg_data, array('name', 'lastname', 'title', 'email', 'username', 'password', 'date'));
			$user->add('roles', ORM::factory('role', array('name' => 'login')));
			$user->add('roles', ORM::factory('role', array('name' => 'admin')));


			echo 'user created - '.$pass;
		}
		catch(ORM_Validation_Exception $e)
		{
			// Set failure message
			$message = 'There were errors, please see form below.';
			// Set errors using custom messages
			$errors = $e->errors('models');
			Kohana::ar($errors);
		}

		die();
	}

}
?>