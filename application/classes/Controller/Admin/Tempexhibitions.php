<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Tempexhibitions extends Controller_Admin_Layout {

	public function action_index()
	{
		$data['items'] = ORM::factory('Tempexhibition')->where('date_end','>=',date('Y-m-d'))->order_by('date_end','DESC')->find_all();
		$data['archive'] = FALSE;
		$this->template->title_content = View::factory('admin/tempexhibitions/_index_top');
		$this->template->content = View::factory('admin/tempexhibitions/index', $data);
	}

	# Archive
	public function action_archive()
	{
		$data['items'] = ORM::factory('Tempexhibition')->where('date_end','<=',date('Y-m-d'))->order_by('date_end','DESC')->find_all();
		$data['archive'] = TRUE;
		$this->template->title = 'Архив временных выставок';
		$this->template->breadcrumbs = array(array('url' => 'tempexhibitions', 'title' => 'Временные выставки'));
		$this->template->content = View::factory('admin/tempexhibitions/index', $data);
	}

	# One item
	public function action_info()
	{
		$data['item'] = ORM::factory('Tempexhibition', $this->request->param('id'));

		$this->template->window_title = $data['item']->title;
		$this->template->title = $data['item']->title;

		if($data['item']->date_end <= date('Y-m-d'))
		{
			$this->template->breadcrumbs = array(array('url' => 'tempexhibitions', 'title' => 'Временные выставки'), array('url' => 'tempexhibitions/archive', 'title' => 'Архив'));
		}
		else
		{
			$this->template->breadcrumbs = array(array('url' => 'tempexhibitions', 'title' => 'Временные выставки'));
		}

		$this->template->title_content = View::factory('admin/tempexhibitions/_info_top', array('item_id' => $data['item']->id,'status' => $data['item']->status));
		$this->template->content = View::factory('admin/tempexhibitions/info', $data);
	}

	# Add item
	public function action_add()
	{
		$data = array();
		$this->template->title = 'Добавление выставки';
		$this->template->breadcrumbs = array(array('url' => 'tempexhibitions', 'title' => 'Временные выставки'));
		$this->template->content = View::factory('admin/tempexhibitions/add', $data);
	}
}
