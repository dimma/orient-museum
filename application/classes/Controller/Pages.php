<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Pages extends Controller_Layout
{

	public function action_index()
	{
		$url = $this->request->param('id');
		if (!empty($url))
		{
			$pages = DB::select('id', 'title', 'url', 'anchor')
				->from('pages')
				->where('lang', '=', 'ru')
				->and_where('status', '=', 1)
				->and_where('url', '=', $url)
				->execute()
				->current();

			if (!empty($pages['title']))
			{
				$templates = ORM::factory('Template')
					->where('parent', '=', 'page')
					->and_where('item_model_id', '=', $pages['id'])
					->order_by('sort_order')
					->find_all();

				if ($templates->count())
				{
					$data = array(
						'title'		=> $pages['title'],
						'anchor'	=> (!empty($pages['anchor']) ? 1 : 0),
						'templates'	=> $templates,
					);

					$this->template->title		= $pages['title'];
					$this->template->content	= View::factory('front/pages/index', $data);
				}
				else
				{
					Controller::redirect('/');
				}
			}
			else
			{
				Controller::redirect('/');
			}
		}
		else
		{
			Controller::redirect('/');
		}
	}

}
