<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Exhibitions extends Controller_Layout {

	public function action_index()
	{
		$data = array(
			'title'		=> 'Текущие выставки',
			'publish'	=> ORM::factory('Exhibition')->where('status', '=', 1)->find_all(),
		);

		$this->template->title		= $data['title'];
		$this->template->content	= View::factory('front/exhibitions/index', $data);
	}

	public function action_info()
	{
		$data = array(
			'title'	=> 'Текущая выставка',
			'item'	=> ORM::factory('Exhibition', $this->request->param('id'))->where('status', '=', 1),
		);

		if (!empty($data['item']->id))
		{
			$this->template->title		= $data['item']->title;
			$this->template->content	= View::factory('front/exhibitions/info', $data);
		}
		else
		{
			Controller::redirect('/exhibitions');
		}
	}

}
