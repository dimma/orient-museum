<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Publishing extends Controller_Layout {

	public function action_index()
	{
		$data = array(
			'title'		=> 'Издания',
			'publish'	=> ORM::factory('Publication')->where('status', '=', 1)->find_all(),
		);

		$this->template->title		= $data['title'];
		$this->template->content	= View::factory('front/publishing/index', $data);
	}

	public function action_info()
	{
		$data = array(
			'title'	=> 'Издания',
			'item'	=> ORM::factory('Publication', $this->request->param('id'))->where('status', '=', 1),
		);

		if (!empty($data['item']->id))
		{
			$this->template->title		= $data['item']->title;
			$this->template->content	= View::factory('front/publishing/info', $data);
		}
		else
		{
			Controller::redirect('/publishing');
		}
	}

}
