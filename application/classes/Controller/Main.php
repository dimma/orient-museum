<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Main extends Controller_Layout {

	public function action_index()
	{
		$polls		= ORM::factory('Poll')->where('status', '=', 1)->order_by('date', 'DESC')->find();
		$news		= ORM::factory('New')->where('status', '=', 1)->order_by('date', 'DESC')->reset(FALSE);
		$exhibitions	= ORM::factory('Tempexhibition')->where('status', '=', 1)->and_where('date_end', '>=', date('Y-m-d'))->order_by('date_end', 'DESC')->reset(FALSE);
		$events		= ORM::factory('Event')->where('status', '=', 1)->and_where('date', '>=', date('Y-m-d'))->order_by('date', 'DESC')->reset(FALSE);
		$constExhib	= ORM::factory('Page')->where('status', '=', 1)->and_where('parent', '=', 'exhibition')->order_by('title')->reset(FALSE);
		$answersCount	= DB::select(DB::expr('SUM(result) AS sum'))->where('poll_id', '=', $polls->id)->from('pollanswers')->execute()->as_array();
		$userAnswers	= DB::select()->from('pollanswers')->where('poll_id', '=', $polls->id)->and_where('user_answer', '=', '1')->execute()->as_array();
		$bestAnswer	= DB::select('id')->from('pollanswers')->where('poll_id', '=', $polls->id)->and_where('user_answer', '=', '0')->order_by('result', 'DESC')->execute()->as_array();

		$data	= array(
			'title'				=> 'Государственный Музей Востока',
			'polls'				=> $polls,
			'answers_count'			=> (empty($answersCount[0]['sum']) ? 0 : $answersCount[0]['sum']),
			'news'				=> $news->find_all(),
			'total_news'			=> $news->count_all(),
			'exhibitions'			=> $exhibitions->find_all(),
			'total_exhibitions'		=> $exhibitions->count_all(),
			'events'			=> $events->find_all(),
			'total_events'			=> $events->count_all(),
			'event_types'			=> Model_Event::$event_types,
			'event_apply_types'		=> Model_Event::$event_appply_types,
			'const_exhibitions'		=> $constExhib->find_all(),
			'total_const_exhibitions'	=> $constExhib->count_all(),
		);

		if (!empty($bestAnswer[0]['id']))
		{
			$data['best_answer'] = $bestAnswer[0]['id'];
		}

		$data['user_answers_count'] = 0;
		$data['user_answers_total'] = 0;
		foreach ($userAnswers as $v)
		{
			$data['user_answers_count'] += $v['result_percent'];
			$data['user_answers_total'] += $v['result'];
		}

		$this->template->title		= $data['title'];
		$this->template->content	= View::factory('front/index', $data);
	}

	public function action_examples()
	{
		$this->template->title		= 'Примеры верстки';
		$this->template->content	= View::factory('front/templates/_examples');
	}

	public function action_contacts()
	{
		$data = array(
			'contacts'	=> ORM::factory('Page')->where('url', '=', 'contacts')->and_where('lang', '=', 'ru')->find(),
			'title'		=> 'Как добраться',
		);

		$this->template->title		= $data['title'];
		$this->template->content	= View::factory('front/contacts', $data);
	}

	public function action_contacts_en()
	{
		$data = array(
			'contacts'	=> ORM::factory('Page')->where('url', '=', 'contacts_en')->and_where('lang', '=', 'en')->find(),
			'title'		=> 'How to reach',
		);

		$this->template->title		= $data['title'];
		$this->template->content	= View::factory('front/contacts_en', $data);
	}

}
