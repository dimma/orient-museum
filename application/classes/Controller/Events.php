<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Events extends Controller_Layout {

	public function action_index()
	{
		$events = ORM::factory('Event')->where('status', '=', 1)->order_by('date', 'DESC')->reset(FALSE);
		$data = array(
			'title'	=> 'События',
			'news'	=> $events->find_all(),
			'total'	=> $events->count_all(),
		);
		$this->template->title		= $data['title'];
		$this->template->content	= View::factory('front/events/index', $data);
	}

	public function action_info()
	{
		$data = array(
			'title'	=> 'Текущая выставка',
			'item'	=> ORM::factory('Event', $this->request->param('id'))->where('status', '=', 1),
		);

		if (!empty($data['item']->id))
		{
			$this->template->title		= $data['item']->title;
			$this->template->content	= View::factory('front/events/info', $data);
		}
		else
		{
			Controller::redirect('/events');
		}
	}

}
