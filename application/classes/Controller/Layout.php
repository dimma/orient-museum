<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Layout extends Controller_Template
{
	public $template = 'layout';

	static public $months_genitive = array(
		1	=> 'января',
		2	=> 'февраля',
		3	=> 'марта',
		4	=> 'апреля',
		5	=> 'мая',
		6	=> 'июня',
		7	=> 'июля',
		8	=> 'августа',
		9	=> 'сентября',
		10	=> 'октября',
		11	=> 'ноября',
		12	=> 'декабря',
	);

	private $_schedule = array(
		1 => array('hours' => 'Выходной', 'tickets' => 'Касса не работает'),
		2 => array('hours' => 'Музей работает с 11:00 до 20:00', 'tickets' => 'Касса до 19:30'),
		3 => array('hours' => 'Музей работает с 11:00 до 20:00', 'tickets' => 'Касса до 19:30'),
		4 => array('hours' => 'Музей работает с 12:00 до 21:00', 'tickets' => 'Касса до 20:30'),
		5 => array('hours' => 'Музей работает с 11:00 до 20:00', 'tickets' => 'Касса до 19:30'),
		6 => array('hours' => 'Музей работает с 11:00 до 20:00', 'tickets' => 'Касса до 19:30'),
		7 => array('hours' => 'Музей работает с 11:00 до 20:00', 'tickets' => 'Касса до 19:30'),
	);

	/**
	 * Get menu items
	 * @access private
	 * @return array('menu_items' => array(), 'sitemap_items' => array())
	 */
	private function _get_menu_items()
	{
		$exhIndex = 2;
		$sitemapItems = array(
			'left' => array(
				0 => array(
					'url'	=> 'about',
					'name'	=> 'О музее',
					'class'	=> 'icon-about',
					'drop'	=> TRUE,
					'items'	=> array(),
				),
				1 => array(
					'url'	=> 'visitors',
					'name'	=> 'Посетителям',
					'class'	=> 'icon-visitors',
					'drop'	=> TRUE,
					'items'	=> array(),
				),
				3 => array(
					'url'	=> 'shop',
					'name'	=> 'Наш магазин',
					'class'	=> 'icon-shop',
				),
				4 => array(
					'url'	=> 'publishing',
					'name'	=> 'Издания',
					'class'	=> 'icon-publishing',
				),
			),
			'right' => array(
				2 => array(
					'url'	=> 'events',
					'name'	=> 'Выставки и события',
					'class'	=> 'icon-events',
					'drop'	=> TRUE,
					'items'	=> array(
						0 => array(
							'url'	=> 'exhibitions',
							'name'	=> 'Текущие выставки',
						),
						1 => array(
							'url'	=> 'events',
							'name'	=> 'События',
						),
						$exhIndex => array(
							'url'	=> 'exhibition',
							'name'	=> 'Постоянная экспозиция',
							'drop'	=> TRUE,
							'items'	=> array(),
						),
					),
				),
			),
		);

		$menuPages = DB::select('title', 'url', 'parent')
			->from('pages')
			->where('lang', '=', 'ru')
			->and_where('status', '=', 1)
			->execute()
			->as_array();

		foreach ($menuPages as $mp)
		{
			foreach ($sitemapItems['left'] as &$left)
			{
				if (!in_array($mp['url'], array('about', 'shop')) && $mp['parent'] == $left['url'])
				{
					$left['items'][] = array(
						'url'	=> $mp['url'],
						'name'	=> $mp['title'],
					);
				}

				// Add menu item after url in case
				switch ($mp['url'])
				{
					case 'affiliate':
						if ($left['url'] === 'about')
						{
							$left['items'][] = array(
								'url'	=> 'news',
								'name'	=> 'Новости',
							);
						}
					break;

					case 'tours':
						if ($left['url'] === 'visitors')
						{
							$left['items'][] = array(
								'url'	=> 'virtual_tour',
								'name'	=> 'Виртуальный тур',
							);
						}
					break;
				}
			}
			unset($left);

			foreach ($sitemapItems['right'] as $k => &$right)
			{
				if ($mp['parent'] == $right['url'])
				{
					$right['items'][] = array(
						'url'	=> $mp['url'],
						'name'	=> $mp['title'],
					);
				}
				if ($k == $exhIndex && $mp['parent'] === 'exhibition')
				{
					$right['items'][$exhIndex]['items'][] = array(
						'url'	=> $mp['url'],
						'name'	=> $mp['title'],
					);
				}
			}
			unset($right);
		}

		$menuItems = $sitemapItems['left'] + $sitemapItems['right'];
		ksort($menuItems);

		return array('menu_items' => $menuItems, 'sitemap_items' => $sitemapItems);
	}

	public function before()
	{
		parent::before();

		if ($this->auto_render)
		{
			$this->template->title			= '';
			$this->template->window_title		= '';
			$this->template->meta_keywords		= '';
			$this->template->meta_description	= '';
			$this->template->meta_copyright		= '';
			$this->template->meta_author		= '';
			$this->template->styles			= array();
			$this->template->scripts		= array();
			$this->template->controller		= UTF8::strtolower($this->request->controller());
			$this->template->action			= $this->request->action();
			$this->template->main_page		= ($this->template->controller == 'main' && $this->template->action == 'index');
			$this->template->content		= '';
			$this->template->menu			= Kohana::$config->load('menu');
			$this->template->body_class		= '';
			$this->template->uri			= Request::$current->uri();
			$this->template->detect			= new MobileDetect();
			$this->template->iphone			= ($this->template->detect->is('iPhone') || $this->template->detect->isMobile());

			$curSchedule = $this->_schedule[date('N')];
			$this->template->today			= strtoupper('Сегодня ' . date('j') . ' ' . self::$months_genitive[date('n')]);
			$this->template->today_museum		= $curSchedule['hours'];
			$this->template->today_ticket		= $curSchedule['tickets'];
			
			$menuItems = $this->_get_menu_items();
			$this->template->sitemap_items		= $menuItems['sitemap_items'];
			$this->template->menu_items		= $menuItems['menu_items'];

			if (!Session::instance()->get('hide_notify'))
			{
				$this->template->header_notify = ORM::factory('Setting')
					->where('name', '=', 'header_message')
					->and_where('status', '=', 1)
					->and_where('date', '>=', DB::expr('NOW() - INTERVAL 1 DAY'))
					->find()
					->as_array();
			}
		}
	}

	public function after()
	{
		if($this->auto_render)
		{
			$styles['assets/css/bootstrap.min.css']			= 'screen';
			$styles['assets/css/icons.css']				= 'screen';
			$styles['assets/css/non-responsive.css']		= 'screen';
			$styles['assets/css/style.css']				= 'screen';
			$styles['http://fonts.googleapis.com/css?family=Roboto+Slab:400,300,100,700&subset=latin,cyrillic,cyrillic-ext'] = 'screen';

			$scripts[] = 'assets/js/jquery-2.1.1.js';
			$scripts[] = 'assets/js/respond.min.js';
			$scripts[] = 'assets/js/bootstrap.min.js';
			$scripts[] = 'assets/js/common.js';

			$this->template->styles		= array_merge($this->template->styles, $styles);
			$this->template->scripts	= array_merge($this->template->scripts, $scripts);
		}

		parent::after();
	}
}
?>