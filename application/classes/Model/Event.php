<?php defined('SYSPATH') or die('No direct script access.');

class Model_Event extends ORM
{

	static public $event_types = array(
		1 => 'Лекция',
		2 => 'Концерт',
		3 => 'Мастер-класс',
		4 => 'Другое',
	);

	static public $event_appply_types = array(
		1 => 'Без записи',
		2 => 'Запись по телефону',
		3 => 'Запись через форму на сайте',
	);

	public function filters()
	{
		return array(
			TRUE => array(
				array('trim'),
				array('stripslashes'),
				array('HTML::entities'),
				//array('Security::xss_clean'),
			),
		);
	}

	protected $_belongs_to = array(
		'lecture' => array('model' => 'Lecture', 'foreign_key' => 'lecture_id')
	);

	protected $_has_many = array(
		'dates' => array('model' => 'Eventdate', 'foreign_key' => 'event_id')
	);
}
?>