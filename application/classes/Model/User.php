<?php defined('SYSPATH') or die('No direct script access.');

class Model_User extends Model_Auth_User
{
	/**
	 * A user has many tokens and roles
	 *
	 * @var array Relationhips
	 */
	protected $_has_many = array(
		'user_tokens' => array('model' => 'User_Token'),
		'roles'       => array('model' => 'Role', 'through' => 'roles_users'),
	);

	protected $_has_one = array(
		'partner' => array('model' => 'partner', 'foreign_key' => 'user_id'),
	);

	/**
	 * Rules for the user model. Because the password is _always_ a hash
	 * when it's set,you need to run an additional not_empty rule in your controller
	 * to make sure you didn't hash an empty string. The password rules
	 * should be enforced outside the model or with a model helper method.
	 *
	 * @return array Rules
	 */
	public function rules()
	{
		return array(
			'name' => array(
				array('not_empty'),
			),
			'username' => array(
				array('not_empty'),
				array('max_length', array(':value', 32)),
				array(array($this, 'unique'), array('username', ':value')),
				array(array($this, 'unique'), array('email', ':value')),
				//array('phone', array(':value', array(11))),
			),
			'password' => array(
				array('not_empty'),
			),
			/*
						'email' => array(
							array('not_empty'),
							array('email'),
							array(array($this, 'unique'), array('email', ':value')),
						),
			*/
		);
	}

}
?>