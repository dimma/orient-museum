<?php defined('SYSPATH') or die('No direct script access.');

class Model_Tempexhibition extends ORM
{
	protected $_sorting = array('date_end' => 'DESC');

	public function filters()
	{
		return array(
			TRUE => array(
				array('trim'),
				array('stripslashes'),
				array('HTML::entities'),
				//array('Security::xss_clean'),
			),
		);
	}

	protected $_has_many = array(
		'pictures'	=> array('model' => 'Picture', 'foreign_key' => 'tempexhibition_id'),
		'templates'	=> array('model' => 'Template', 'foreign_key' => 'item_model_id')
	);

	public function rules()
	{
		return array(
			'title' => array(array('not_empty')),
		);
	}
}
?>