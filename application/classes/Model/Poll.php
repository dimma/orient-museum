<?php defined('SYSPATH') or die('No direct script access.');

class Model_Poll extends ORM
{
	protected $_sorting = array('date' => 'DESC');

	public function filters()
	{
		return array(
			TRUE => array(
				array('trim'),
				array('stripslashes'),
				array('HTML::entities'),
				//array('Security::xss_clean'),
			),
		);
	}

	protected $_has_many = array(
		'answers' => array('model' => 'Pollanswer', 'foreign_key' => 'poll_id')
	);
}
?>