<?php defined('SYSPATH') or die('No direct script access.');

class Model_Page extends ORM
{
	protected $_has_many = array(
		'pictures'	=> array('model' => 'Picture', 'foreign_key' => 'page_id'),
		'templates'	=> array('model' => 'Template', 'foreign_key' => 'item_model_id')
	);

	public function filters()
	{
		return array(
			TRUE => array(
				array('trim'),
				array('stripslashes'),
				array('HTML::entities'),
				//array('Security::xss_clean'),
			),
		);
	}
}
?>