<?php defined('SYSPATH') or die('No direct script access.');

class Model_Eventdate extends ORM
{
	protected $_sorting = array('date' => 'ASC');

	public function filters()
	{
		return array(
			TRUE => array(
				array('trim'),
				array('stripslashes'),
				array('HTML::entities'),
				//array('Security::xss_clean'),
			),
		);
	}

	protected $_belongs_to = array(
		'event' => array('model' => 'Event', 'foreign_key' => 'event_id')
	);
}
?>