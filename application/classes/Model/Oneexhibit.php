<?php defined('SYSPATH') or die('No direct script access.');

class Model_Oneexhibit extends ORM
{
	protected $_sorting = array('date_added' => 'DESC');

	public function filters()
	{
		return array(
			TRUE => array(
				array('trim'),
				array('stripslashes'),
				array('HTML::entities'),
				//array('Security::xss_clean'),
			),
		);
	}

	protected $_has_many = array(
		'pictures' => array('model' => 'Picture', 'foreign_key' => 'oneexhibit_id')
	);

}
?>