<?php defined('SYSPATH') or die('No direct script access.');

class Model_Template extends ORM
{
	protected $_sorting = array('sort_order' => 'ASC');

	public function filters()
	{
		return array(
			TRUE => array(
				array('trim'),
				array('stripslashes'),
				//array('HTML::entities'),
				//array('Security::xss_clean'),
			),
		);
	}

	protected $_belongs_to = array(
		'tempexhibition'	=> array('model' => 'Tempexhibition', 'foreign_key' => 'item_model_id'),
		'event'			=> array('model' => 'Event', 'foreign_key' => 'item_model_id'),
		'oneexhibit'		=> array('model' => 'Oneexhibit', 'foreign_key' => 'item_model_id'),
		'new'			=> array('model' => 'New', 'foreign_key' => 'item_model_id'),
		'publication'		=> array('model' => 'Publication', 'foreign_key' => 'item_model_id'),
		'page'			=> array('model' => 'Page', 'foreign_key' => 'item_model_id'),
	);

	protected $_has_many = array(
		'pictures'	=> array('model' => 'Picture', 'foreign_key' => 'template_id'),
	);

	/**
	 * Get all templates for model
	 * @access public
	 * @static
	 * @param string $modelName tempexhibition|event|oneexhibit|new|publication|page
	 * @param int $itemId
	 * @return object
	 */
	static public function getModelTemplates($modelName, $itemId)
	{
		$result = FALSE;
		if (!empty($itemId) && in_array($modelName, array('tempexhibition', 'event', 'oneexhibit', 'new', 'publication', 'page')))
		{
			$result = DB::select('t.*')
				->from(array('templates', 't'))
				->join(array($modelName . 's', 'm'), 'LEFT')
				->on('m.id', '=', 't.item_model_id')
				->where('t.parent', '=', $modelName)
				->and_where('t.item_model_id', '=', $itemId)
				->order_by('sort_order')
				->execute()
				->as_array();
		}

		return $result;
	}
}
?>