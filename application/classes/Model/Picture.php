<?php defined('SYSPATH') or die('No direct script access.');

class Model_Picture extends ORM
{
	public function filters()
	{
		return array(
			TRUE => array(
				array('trim'),
				array('stripslashes'),
				array('HTML::entities'),
				//array('Security::xss_clean'),
			),
		);
	}

	protected $_belongs_to = array(
		'tempexhibitions'	=> array('model' => 'Tempexhibition', 'foreign_key' => 'tempexhibition_id'),
		'event'			=> array('model' => 'Event', 'foreign_key' => 'event_id'),
		'oneexhibit'		=> array('model' => 'Oneexhibit', 'foreign_key' => 'oneexhibit_id'),
		'new'			=> array('model' => 'New', 'foreign_key' => 'new_id'),
		'publication'		=> array('model' => 'Publication', 'foreign_key' => 'publication_id'),
		'page'			=> array('model' => 'Page', 'foreign_key' => 'page_id'),
		'template'		=> array('model' => 'Template', 'foreign_key' => 'template_id'),
	);
}
?>