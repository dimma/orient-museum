<?php defined('SYSPATH') or die('No direct script access.');

class Model_Pollanswer extends ORM
{
	public function filters()
	{
		return array(
			TRUE => array(
				array('trim'),
				array('stripslashes'),
				array('HTML::entities'),
				//array('Security::xss_clean'),
			),
		);
	}

	public function rules()
	{
		return array(
			'text' => array(array('not_empty')),
		);
	}

	protected $_belongs_to = array(
		'poll' => array('model' => 'Poll', 'foreign_key' => 'answer_id')
	);
}
?>