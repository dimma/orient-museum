<!DOCTYPE html>
<html lang="ru">
	<head>
		<title><?=$title?></title>
		<meta charset="utf-8">
		<meta name="keywords" content="<?=$meta_keywords;?>" />
		<meta name="description" content="<?=$meta_description;?>" />
		<meta name="copyright" content="<?=$meta_copyright;?>" />
		<meta name="author" content="<?=$meta_author;?>" />
		<?php foreach ($styles as $file => $type) { echo HTML::style($file, array('media' => $type)), "\n"; } ?>
	</head>
	<body>
		<?php if (!empty($header_notify['value'])) : ?>
			<div class="notifyMessage">
				<div class="container">
					<span class="close close-white icon-close" id="headerNotify"></span>
					<p class="container container-main"><?=$header_notify['value']?></p>
				</div>
			</div>
		<?php endif; ?>

		<header class="container">
			<section class="container container-main">
				<div class="row">
					<div class="col-xs-4 col-sm-4 col-md-4">
						<p class="lang"><a href="/contacts_en"><span class="icon-aboutEn"></span> <b>ABOUT MUSEUM IN ENGLISH</a></b></p>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 text-center">
						<a title="МУЗЕЙ ВОСТОКА" class="logo" href="/"><img src="/assets/img/logoHeader.svg" title="МУЗЕЙ ВОСТОКА" alt="МУЗЕЙ ВОСТОКА"/></a>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 text-right">
						<p><b><?=$today?></b></p>
						<p><?=$today_museum?></p>
						<p><?=$today_ticket?></p>
					</div>
				</div>
			</section>
		</header>

		<div aria-hidden="true" aria-labelledby="siteMapLabel" role="dialog" tabindex="-1" class="modal fade" id="siteMap">
			<div class="modal-dialog container">
				<button data-dismiss="modal" class="close close-grey icon-close pull-left" type="button">
					<span class="" aria-hidden="true"></span>
					<span class="sr-only">Close</span>
				</button>
				<div class="modal-content container container-main">
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-5 col-sm-5 col-md-5">
								<?php foreach ($sitemap_items['left'] as $ml1) : ?>
									<h3><a href="<?=$ml1['url']?>"><?=$ml1['name']?></a></h3>
									<?php if (!empty($ml1['items'])) : ?>
										<ul class="nav">
											<?php foreach ($ml1['items'] as $ml2) : ?>
												<li><h5><a href="<?=$ml2['url']?>"><?=$ml2['name']?></a></h5></li>
											<?php endforeach; ?>
										</ul>
									<?php endif; ?>
								<?php endforeach; ?>
							</div>
							<div class="col-xs-7 col-sm-7 col-md-7">
								<?php foreach ($sitemap_items['right'] as $mr1) : ?>
									<h3><a href="<?=$mr1['url']?>"><?=$mr1['name']?></a></h3>
									<?php if (!empty($mr1['items'])) : ?>
										<ul class="nav">
											<?php foreach ($mr1['items'] as $mr2) : ?>
												<li><h5><a href="<?=$mr2['url']?>"><?=$mr2['name']?></a></h5></li>
												<?php if (!empty($mr2['items'])) : ?>
													<li>
														<ul class="nav">
															<?php foreach ($mr2['items'] as $mr3) : ?>
																<li><p><a href="<?=$mr3['url']?>"><?=$mr3['name']?></a></p></li>
															<?php endforeach; ?>
														</ul>
													</li>
												<?php endif; ?>
											<?php endforeach; ?>
										</ul>
									<?php endif; ?>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>

		<div class="navbar navbar-default affix-top" role="navigation" data-spy="affix" data-offset-top="195">
			<div class="container">
				<ul class="nav navbar-nav navbar-right pull-right">
					<li>
						<button id="btn-search" type="submit" class="btn btn-link"><span class="icon-search"></span></button>
					</li>
				</ul>
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" style="display: block;" data-target="#siteMap" data-toggle="modal"> <!-- data-toggle="collapse" data-target=".navbar-collapse" -->
						<span class="sr-only">Карта сайта</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div class="container container-main">
					<div class="headerMenu"> <!-- <div class="navbar-collapse collapse"> -->
						<ul class="nav navbar-nav text-center">
							<?php foreach ($menu_items as $m1) : ?>
								<li <?php if (!empty($m1['drop'])) : ?>class="dropdown"<?php endif; ?>>
									<a href="/<?=$m1['url']?>" <?php if (!empty($m1['drop'])) : ?>class="dropdown-toggle" data-toggle="dropdown"<?php endif; ?>><span class="<?php if (!empty($m1['class'])) : ?><?=$m1['class']?><?php endif; ?>"></span> <?=$m1['name']?></a>
									<?php if (!empty($m1['items'])) : ?>
										<ul class="dropdown-menu">
											<?php foreach ($m1['items'] as $m2) : ?>
												<li <?php if (!empty($m2['drop'])) : ?>class="dropdown"<?php endif; ?>>
													<a href="/<?=$m2['url']?>" <?php if (!empty($m2['drop'])) : ?>class="dropdown-toggle" data-toggle="dropdown"<?php endif; ?>><?=$m2['name']?></a>
													<?php if (!empty($m2['items'])) : ?>
														<div class="dropdown-menu">
															<div class="container">
																<div class="container container-main">
																	<div class="row">
																		<?php $i = 1; ?>
																		<?php $totalItems = count($m2['items']) / 3; // Make 3 columns of submenu ?>
																		<?php foreach ($m2['items'] as $m3) : ?>
																			<?php if ($i === 1 || ($i % $totalItems) === 1) : ?><ul class="col-xs-4 col-sm-4 col-md-4"><?php endif; ?>
																				<li>
																					<a href="/<?=$m3['url']?>"><?=$m3['name']?></a>
																				</li>
																			<?php if ($i === 0 || ($i % $totalItems) === 0) : ?></ul><?php endif; ?>
																			<?php $i += 1; ?>
																		<?php endforeach; ?>
																	</div>
																</div>
															</div>
														</div>
													<?php endif; ?>
												</li>
											<?php endforeach; ?>
										</ul>
									<?php endif; ?>
								</li>
							<?php endforeach; ?>
						</ul>
					</div><!--/.nav-collapse -->
					<div id="search"><input class="form-control" type="search" placeholder="Введите свой запрос и нажмите кнопку enter…" /></div>
				</div>
			</div>
		</div>

		<?=$content?>

		<footer>
			<div class="container partners">
				<div class="container container-main">
					<div class="row">
						<div class="col-xs-4 col-sm-4 col-md-4">
							<table>
								<tbody>
									<tr>
										<th>Для справок</th>
										<td>8 495 691-02-12</td>
									</tr>
									<tr>
										<th>Экскурсбюро</th>
										<td>8 495 691-82-19</td>
									</tr>
									<tr>
										<th>Факс</th>
										<td>8 495 695-48-46</td>
									</tr>
									<tr>
										<th>Пресс-служба</th>
										<td>8 495 690-05-23</td>
									</tr>
									<tr>
										<th>Почта</th>
										<td>info@orientmuseum.ru</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4">
							<table>
								<tbody>
								<tr>
									<th>Пн</th>
									<td>Выходной</td>
								</tr>
								<tr>
									<th>Вт–Вс</th>
									<td>11:00–20:00</td>
								</tr>
								<tr>
									<th>Чт</th>
									<td>12:00–21:00</td>
								</tr>
								</tbody>
							</table>
							<p>Кассы закрываются за 30 минут до закрытия музея.</p>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4">
							<p>Подписаться на новости музея</p>
							<form class="form-inline" action="" role="form">
								<div class="form-group">
									<label class="sr-only" for="">Ваша почта</label>
									<input class="form-control" type="text" placeholder="Ваша почта" />
								</div>
								<button class="btn btn-black">ок</button>
							</form>
						</div>
					</div>
					<div class="row mb20 mt10">
						<div class="col-xs-4 col-sm-4 col-md-4">
							<a class="mr20" href="/contacts"><span class="icon-aboutEn"></span> как проехать</a>
							<a href="/prices"><span class="icon-tickets"></span> билеты</a>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4">
							<a class="socLink icon-vk" href="#"></a>
							<a class="socLink icon-tw" href="#"></a>
							<a class="socLink icon-in" href="#"></a>
							<a class="socLink icon-fb" href="#"></a>
							<a class="socLink icon-ok" href="#"></a>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4">
							<a href=""><span class="icon-feedback"></span> обратная связь</a>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4 col-sm-4 col-md-4">
							<p>119019, Москва,<br />Никитский бульвар, дом 12а</p>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4">
							<p>Разработка сайта</p>
							<a class="company icon-siteExpo" href="http://siteexpo.ru/"></a>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4">
							<p>Государственный музей Востока<br />&copy; 2008–<?=date('Y')?></p>
						</div>
					</div>
				</div>
			</div>
		</footer>

		<div aria-hidden="true" aria-labelledby="siteMapLabel" role="dialog" tabindex="-1" class="modal fade in" id="feedBackComplete" style="display: bl1ock">
			<div class="modal-dialog container">
				<button data-dismiss="modal" class="close close-grey close-fill icon-close pull-left" type="button">
					<span class="" aria-hidden="true"></span>
					<span class="sr-only">Close</span>
				</button>
				<div class="modal-content container container-main">
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-7 col-sm-7 col-md-7 col-xs-offset-3 col-sm-offset-3 col-md-offset-3">
								<h3>Обратная связь</h3>
								<h5>Ваше письмо отправлено. При необходимости, мы&nbsp;с&nbsp;вами свяжемся. Спасибо!</h5>
								<p class="text-grey">Через 5&nbsp;секунд это сообщение закроется автоматически. Либо вы&nbsp;можете <a class="text-red" href="">закрыть</a> его самостоятельно.</p>
							</div>
						</div>
						<div class="helper"></div>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>

		<div aria-hidden="true" aria-labelledby="siteMapLabel" role="dialog" tabindex="-1" class="modal fade in" id="feedBack" style="display: blo1ck">
			<div class="modal-dialog container">
				<button data-dismiss="modal" class="close close-grey close-fill icon-close pull-left" type="button">
					<span class="" aria-hidden="true"></span>
					<span class="sr-only">Close</span>
				</button>
				<div class="modal-content container container-main">
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-7 col-sm-7 col-md-7 col-xs-offset-3 col-sm-offset-3 col-md-offset-3">
								<h3>Обратная связь</h3>
								<h5>Если у&nbsp;вас есть вопросы по&nbsp;работе музея, мы&nbsp;с&nbsp;удовольствием на&nbsp;них ответим.</h5>
								<div class="form-group">
									<label class="sr-only" for="">Представьтесь</label>
									<input class="form-control" tabindex="1" type="text" placeholder="Представьтесь" />
								</div>
								<div class="form-group has-error">
									<label class="sr-only" for="">Электронная почта</label>
									<input class="form-control" tabindex="2" type="text" placeholder="Электронная почта" />
								</div>
								<div class="form-group">
									<label class="sr-only" for="">Ваше сообщение...</label>
									<textarea class="form-control"  tabindex="3" name="" id="" cols="30" rows="10" placeholder="Ваше сообщение..."></textarea>
								</div>
								<p class="text-grey"><button class="btn btn-black mr15" tabindex="4" type="submit">отправить</button> Все поля обязательный для заполнения</p>
							</div>
						</div>
						<div class="helper"></div>
					</div>

				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>

		<div aria-hidden="true" aria-labelledby="siteMapLabel" role="dialog" tabindex="-1" class="modal fade in" id="distribution" style="display: blo1ck">
			<div class="modal-dialog container">
				<button data-dismiss="modal" class="close close-grey close-fill icon-close pull-left" type="button">
					<span class="" aria-hidden="true"></span>
					<span class="sr-only">Close</span>
				</button>
				<div class="modal-content container container-main">
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-7 col-sm-7 col-md-7 col-xs-offset-3 col-sm-offset-3 col-md-offset-3">
								<h3>Жаль :(</h3>
								<h5>Вы&nbsp;отписались от&nbsp;рассылки. Но&nbsp;всегда можете <a class="text-red" href="">подписаться снова!</a></h5>
								<p class="text-grey">Через 5&nbsp;секунд это сообщение закроется автоматически. Либо вы&nbsp;можете <a class="text-red" href="">закрыть</a> его самостоятельно.</p>
							</div>
						</div>
						<div class="helper"></div>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>		

		<!-- Javascripts
		================================================== -->
		<?php foreach($scripts as $file) { echo HTML::script($file), "\n"; } ?>
	</body>
</html>
