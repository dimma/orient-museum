<div class="container eventsFilter mb40">
	<div class="container container-main">
		<h1 class="text-center mt70 mb70"><?=$title?></h1>
		<h2 class="mb70">
			<a class="text-red" href="">Ближайшие</a> события
			<div class="btn-group filterYears">
				<a type="button" class="text-red dropdown-toggle" data-toggle="dropdown">2014</a>
				<div class="dropdown-menu" role="menu">
					<ul>
						<li class="active"><a href="#">2014</a></li>
						<li><a href="#">2013</a></li>
						<li><a href="#">2012</a></li>
						<li><a href="#">2011</a></li>
						<li><a href="#">2010</a></li>
						<li><a href="#">2009</a></li>
						<li><a href="#">2008</a></li>
						<li><a href="#">2007</a></li>
						<li><a href="#">2006</a></li>
						<li><a href="#">2005</a></li>
						<li><a href="#">2004</a></li>
						<li><a href="#">2003</a></li>
						<li><a href="#">2002</a></li>
						<li><a href="#">2001</a></li>
						<li><a href="#">2000</a></li>
					</ul>
				</div>
			</div>
			года
		</h2>
		<ul class="searchFilter mb30">
			<li class="active"><a href="" class="btn btn-grey btn-xs">все события</a></li>
			<li><a href="" class="btn btn-grey btn-xs">лекции</a></li>
			<li><a href="" class="btn btn-grey btn-xs">КРУЖКИ</a></li>
			<li><a href="" class="btn btn-grey btn-xs">чайная церемония</a></li>
			<li><a href="" class="btn btn-grey btn-xs">для детей</a></li>
			<li><a href="" class="btn btn-grey btn-xs">другое</a></li>
		</ul>
		<h4>Цикл лекций №5. Древний восток. Наследие веков.</h4>
		<table class="table mb40">
			<thead>
			<tr>
				<th>дата</th>
				<th>время</th>
				<th>сбытие</th>
				<th>описание</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>Сегодня</td>
				<td>12:00</td>
				<td>Лекция</td>
				<td>
					Черная луна в гороскопе рождения. Особенности появления фиктивной точки зрения. <span class="text-grey">Цикл лекций №5. Древний восток. Наследие веков.</span>
					<div><a class="btn btn-grey btn-xs mt5" href="">записаться</a></div>
				</td>
			</tr>
			<tr>
				<td>7 августа</td>
				<td>14:00</td>
				<td>Лекция для детей</td>
				<td>
					Черная луна в гороскопе рождения. Особенности появления фиктивной точки зрения. <span class="text-grey">Цикл лекций №6. Древний восток. Наследие веков.</span>
				</td>
			</tr>
			<tr>
				<td>Сегодня</td>
				<td>12:00</td>
				<td>Лекция</td>
				<td>
					Черная луна в гороскопе рождения. Особенности появления фиктивной точки зрения. <span class="text-grey">Цикл лекций №5. Древний восток. Наследие веков.</span>
					<div><a class="btn btn-grey btn-xs mt5" href="">записаться</a></div>
				</td>
			</tr>
			<tr>
				<td>7 августа</td>
				<td>14:00</td>
				<td>Лекция для детей</td>
				<td>
					Черная луна в гороскопе рождения. Особенности появления фиктивной точки зрения. <span class="text-grey">Цикл лекций №6. Древний восток. Наследие веков.</span>
				</td>
			</tr>
			</tbody>
		</table>

		<h4>Цикл лекций №10. Древний восток. Наследие веков.</h4>

		<table class="table mb40">
			<thead>
			<tr>
				<th>дата</th>
				<th>время</th>
				<th>сбытие</th>
				<th>описание</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>Сегодня</td>
				<td>12:00</td>
				<td>Лекция</td>
				<td>
					Черная луна в гороскопе рождения. Особенности появления фиктивной точки зрения. <span class="text-grey">Цикл лекций №5. Древний восток. Наследие веков.</span>
					<div><a class="btn btn-grey btn-xs mt5" href="">записаться</a></div>
				</td>
			</tr>
			<tr>
				<td>7 августа</td>
				<td>14:00</td>
				<td>Лекция для детей</td>
				<td>
					Черная луна в гороскопе рождения. Особенности появления фиктивной точки зрения. <span class="text-grey">Цикл лекций №6. Древний восток. Наследие веков.</span>
				</td>
			</tr>
			<tr>
				<td>Сегодня</td>
				<td>12:00</td>
				<td>Лекция</td>
				<td>
					Черная луна в гороскопе рождения. Особенности появления фиктивной точки зрения. <span class="text-grey">Цикл лекций №5. Древний восток. Наследие веков.</span>
					<div><a class="btn btn-grey btn-xs mt5" href="">записаться</a></div>
				</td>
			</tr>
			<tr>
				<td>7 августа</td>
				<td>14:00</td>
				<td>Лекция для детей</td>
				<td>
					Черная луна в гороскопе рождения. Особенности появления фиктивной точки зрения. <span class="text-grey">Цикл лекций №6. Древний восток. Наследие веков.</span>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
</div>