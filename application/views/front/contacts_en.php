<div class="container contacts">
	<div class="container container-main">
		<h1 class="text-center mt30 mb20"><?=$title?></h1>
		<h3 class="mt30 mb30">State Museum of Oriental Art</h3>
		<div class="row">
			<?php if (!empty($contacts->col1_text)) : ?>
				<div class="col-xs-6 col-sm-6 col-md-6">
					<?=htmlspecialchars_decode($contacts->col1_text)?>
				</div>
			<?php endif; ?>
			<?php if (!empty($contacts->col2_text)) : ?>
				<div class="col-xs-6 col-sm-6 col-md-6">
					<?=htmlspecialchars_decode($contacts->col2_text)?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>

<div id="google_map" style="width:100%; height:300px;"></div>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&language=en"></script>
<script type="text/javascript">
	var lat = 55.756182;
	var lng = 37.600084;
	var mapOptions = {
		center			: new google.maps.LatLng(lat, lng),
		zoom			: 16,
		mapTypeId		: google.maps.MapTypeId.ROADMAP,
		disableDefaultUI	: true,
		scrollwheel		: false,
		zoomControl		: true
	};
	var mapObject = new google.maps.Map(document.getElementById("google_map"), mapOptions);
	var marker = new google.maps.Marker({
		position	: new google.maps.LatLng(lat, lng),
		map		: mapObject
	});
</script>

<div class="container contacts mb40">
	<div class="container container-main">
		<h3 class="mt30 mb20">Филиал</h3>
		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-6">
				<p>385000, Республика Адыгея, г. Майкоп, улица Первомайская, дом 221</p>
				<p>Телефон для справок: <b>(8772) 52-10-33, 52-10-33</b></p>
				<p>Почта: <a class="text-red" href="mailto:muzvostok@radnet.ru">muzvostok@radnet.ru</a> или <a class="text-red" href="mailto:muzvostok@mail.ru">muzvostok@mail.ru</a></p>
				<p>Вт–Чт — 10:00–18:00<br />
				Пт — 10:00–17:00<br />
				Сб–Пн — выходной</p>
				<a class="text-red" href="">Подробнее</a>
			</div>
		</div>
	</div>
</div>