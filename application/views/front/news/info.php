<div class="container mainItem">
	<div class="container container-main">
		<h1 class="text-center mt70 mb70"><?=$title?></h1>
	</div>
</div>
<div class="container mainItem mb70">
	<div class="container container-main">
		<div class="row">
			<div class="col-xs-2 col-sm-2 col-md-2">
				<?php $date = explode('-', $item->date); ?>
				<span class="dataItem pull-left"><?=((int) $date[2] . ' ' . Controller_Layout::$months_genitive[(int)$date[1]])?></span>
			</div>
			<div class="col-xs-10 col-sm-10 col-md-10">
				<h3 class="mb20"><?=$item->title?></h3>
				<div class="thumbItem">
					<?php if ($item->pictures->count_all()) : ?>
						<?php $img = $item->pictures->where('cover', '=', '1')->order_by('date', 'DESC')->find(); ?>
						<img width="360" src="/assets/upload/items_pictures/<?=$img->name?>.<?=$img->ext?>" alt=""/>
					<?php endif; ?>
				</div>
				<p class="mb20"><?=$item->anons?></p>
				<!--<script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
				<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki" data-yashareTheme="counter"></div>
				-->
			</div>
		</div>
	</div>
</div>