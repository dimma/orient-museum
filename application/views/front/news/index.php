<div class="container news mb70">
	<div class="container container-main">
		<h1 class="text-center mt70 mb70"><?=$title?></h1>

		<?=View::factory('front/news/_news_block', array('news' => $news))?>

		<?php if ($total > 10) : ?>
			<div class="text-center"><a class="btn btn-grey btn-xs" href="javascript:void(0);">Еще новости</a></div>
		<?php endif; ?>
	</div>
</div>