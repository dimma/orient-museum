<div class="row">
	<?php $i = 0; ?>
	<?php foreach ($news as $n) : ?>
		<?php if ($i === 0) : ?><a href="/news/<?=$n->id?>"><?php endif; ?>
		<div class="<?php if ($i === 0) : ?>col-xs-8 col-sm-8 col-md-8 kindLg<?php elseif ($i === 1) : ?>col-xs-4 col-sm-4 col-md-4 kindMd<?php else : ?>col-xs-3 col-sm-3 col-md-3 kindSm<?php endif; ?>">
			<div class="picWrap">
				<?php if ($i !== 0) : ?><a href="/news/<?=$n->id?>"><?php endif; ?>
				<div class="pic"
					<?php if ($n->pictures->count_all()) : ?>
						<?php $img = $n->pictures->where('cover', '=', '1')->order_by('date', 'DESC')->find(); ?>
						style="background-image: url('/assets/upload/items_pictures/<?=$img->name?><?php if ($i < 2) : ?>_1000<?php else : ?>_200_200<?php endif; ?>.<?=$img->ext?>');"
					<?php endif; ?>></div></a>
				<?php if ($i !== 0) : ?></a><?php endif; ?>
			</div>
			<div class="textContainer">
				<?php $date = explode('-', $n->date); ?>
				<h6><?=((int) $date[2] . ' ' . Controller_Layout::$months_genitive[(int)$date[1]] . ' ' . $date[0])?></h6>
				<?php if ($i === 0) : ?><h3><?php else : ?><h5><?php endif; ?><a href="/news/<?=$n->id?>"><?=$n->title?></a><?php if ($i === 0) : ?></h3><?php else : ?></h5><?php endif; ?>
				<?php if ($i < 2) : ?><p><?=$n->anons?></p><?php endif; ?>
			</div>
		</div>
		<?php if ($i === 0) : ?></a><?php endif; ?>
		<?php $i += 1; ?>
	<?php endforeach; ?>
</div>