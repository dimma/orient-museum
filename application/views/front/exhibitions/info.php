<div class="container mainItem">
	<div class="container container-main">

		<h1 class="text-center mt70 mb70">выставки сегодня</h1>

	</div>
</div>

<div class="container mainItem mb70">
	<div class="container container-main">

		<div class="row">

				<div class="col-xs-2 col-sm-2 col-md-2">
						<span class="dataItem pull-left" data-lableSoon="скоро открытие" data-position="bottom">15 октября &mdash; 16 ноября 2014</span>
				</div>
				<div class="col-xs-10 col-sm-10 col-md-10">

						<h3 class="mb20">В музее стартовал фестиваль «Египетская культурная инициатива»</h3>

						<p class="mb30">12 сентября состоялось торжественное открытие фестиваля «Египетская культурная инициатива». Фестиваль представили руководители «Центра гуманитарного сотрудничества» - Мона Халиль и Татьяна Богдан.<br />Вела церемонию первый заместитель генерального директора ГМВ Татьяна Метакса.</p>

						<div class="tableDiv mb70">

							<div class="pr20">
								<div class="picWrap"><img alt="" src="/assets/img/exImg1.jpg"></div>
							</div>
							<div class="v-a-b">
								<p class="text-grey mb0">В музее открыты постоянные экспозиции: «Искусство Кореи», «Искусство Китая», «Искусство Японии».</p>
							</div>

						</div>

						<script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
						<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki" data-yashareTheme="counter"></div>

				</div>

		</div>
	</div>
</div>