<div class="container mainCont mb70">
	<div class="container container-main">

		<h1 class="text-center mt70 mb70">выставки сегодня</h1>

		<div class="row mb70">
			<div class="col-xs-6 col-sm-6 col-md-6">

				<div class="picWrap mwFull mb30"><img alt="" src="/assets/img/exImg1.jpg" class="mwFull"></div>
				<h6 class="dataItem" data-lableSoon="скоро открытие" data-position="left">15 октября &mdash; 16 ноября 2014</h6>
				<h3>СВЕТ ШАМБАЛЫ Центральная Азия в жизни и творчестве Рерихов</h3>

			</div>
			<div class="col-xs-6 col-sm-6 col-md-6">

				<div class="picWrap mwFull mb30"><img alt="" src="/assets/img/exImg2.jpg" class="mwFull"></div>
				<h6 class="dataItem" data-lableSoon="скоро открытие" data-position="left">15 октября &mdash; 16 ноября 2014</h6>
				<h3>Исмаил-бей Гаспринский и российские мусульмане (к столетию со дня смерти И. Гаспринского)</h3>

			</div>
		</div>

		<div class="row mb70">
			<div class="col-xs-6 col-sm-6 col-md-6">

				<div class="picWrap mwFull mb30"><img alt="" src="/assets/img/exImg1.jpg" class="mwFull"></div>
				<h6 class="dataItem" data-lableSoon="скоро открытие" data-position="left">15 октября &mdash; 16 ноября 2014</h6>
				<h3>СВЕТ ШАМБАЛЫ Центральная Азия в жизни и творчестве Рерихов</h3>

			</div>
			<div class="col-xs-6 col-sm-6 col-md-6">

			</div>
		</div>


		<div class="row mb30">
			<div class="col-xs-12 col-sm-12 col-md-12">

				<h2>Анонс</h2>

			</div>
		</div>

		<div class="row mb40">
			<div class="col-xs-6 col-sm-6 col-md-6">

				<h6>6 ноября–16 декабря 2014</h6>
				<h3>Выставка из фондов музея провинции Хубэй (Китай)</h3>

			</div>
			<div class="col-xs-6 col-sm-6 col-md-6">

				<h6>6 ноября–16 декабря 2014</h6>
				<h3>Свет Шамбалы. Рерихи — исследователи Центральной Азии</h3>

			</div>
		</div>
		<div class="row mb40">
			<div class="col-xs-6 col-sm-6 col-md-6">

				<h6>6 ноября–16 декабря 2014</h6>
				<h3>Выставка из фондов музея провинции Хубэй (Китай)</h3>

			</div>
			<div class="col-xs-6 col-sm-6 col-md-6">

				<h6>6 ноября–16 декабря 2014</h6>
				<h3>Свет Шамбалы. Рерихи — исследователи Центральной Азии</h3>

			</div>
		</div>

		<div class="text-center">
			<p class="text-grey mb30">Музей оставляет за собой право изменять название и дату проведения выставки.</p>
			<a class="btn btn-grey btn-xs" href="">архив выставок</a>
		</div>

	</div>
</div>