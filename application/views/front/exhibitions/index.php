<div class="container mainCont mb70">
	<div class="container container-main">

		<h1 class="text-center mt70 mb70">выставка одного экспоната</h1>

		<div class="row mb70">
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="thumbItem">
					<img src="/assets/img/publishCover.jpg" alt=""/>
				</div>
				<h6>22 августа 2014</h6>
				<h5>Бундо. Окимоно «Странствующий монах-флейтист с корзиной»</h5>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="thumbItem">
					<img src="/assets/img/publishCover.jpg" alt=""/>
				</div>
				<h6>5 августа 2014</h6>
				<h5>Лев и человек</h5>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="thumbItem">
					<img src="/assets/img/publishCover.jpg" alt=""/>
				</div>
				<h6>9 июня 2014</h6>
				<h5>Бодхисаттва Фугэн</h5>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="thumbItem">
					<img src="/assets/img/publishCover.jpg" alt=""/>
				</div>
				<h6>23 апреля 2014</h6>
				<h5>Н. Пиросманашвили «Кутеж»</h5>
			</div>
		</div>

		<div class="row mb70">
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="thumbItem">
					<img src="/assets/img/publishCover.jpg" alt=""/>
				</div>
				<h6>23 апреля 2014</h6>
				<h5>Астролябия Планисфера</h5>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="thumbItem">
					<img src="/assets/img/publishCover.jpg" alt=""/>
				</div>
				<h6>23 апреля 2014</h6>
				<h5>Хання — маска театра Но</h5>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="thumbItem">
					<img src="/assets/img/publishCover.jpg" alt=""/>
				</div>
				<h6>23 апреля 2014</h6>
				<h5>Храм Махедранатха (Патан, 1408 г.) Модель</h5>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="thumbItem">
					<img src="/assets/img/publishCover.jpg" alt=""/>
				</div>
				<h6>23 апреля 2014</h6>
				<h5>Упасака Дхарматала</h5>
			</div>
		</div>

	</div>
</div>