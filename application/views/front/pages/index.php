<?php if (!empty($anchor)) : ?>
	<div class="container price mb40">
		<div style="position: absolute;right: 33px;width: 50px">
			<div style="position: fixed;">
				<div class="anchorBar">
					<a class="anchorToggle icon-anchor" href="#1"></a>
					<div class="dropdown">
						<a class="anchorToggle" href="#1"><span class="icon-arrDown"></span><span class="icon-anchor"></span>Оглавление</a>
						<div class="dropdown">
							<div class="dropdownWrap">
								<a class="anchorToggle" href="/"><span class="icon-arrUp"></span><span class="icon-anchor"></span>Оглавление</a>
								<ul>
									<?php foreach (array() as $i => $v) : ?>
										<li><a href="#<?=$i?>"><?=$v?></a></li>
									<?php endforeach; ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php foreach ($templates as $t) : ?>
	<?=View::factory('front/templates/' . $t->type, array('data' => $t))?>
<?php endforeach; ?>