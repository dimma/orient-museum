<?php if (!empty($total_exhibitions)) : ?>
	<div class="container">
		<div class="container container-main">
			<div id="indexCarousel" class="carousel slide" data-interval="false" data-ride="carousel">
				<div class="carousel-inner">
					<?php $i = 1; ?>
					<?php foreach ($exhibitions as $mex) : ?>
						<div class="item <?php if ($i === 1) : ?>active<?php endif;?>">
							<div class="row">
								<div class="col-xs-2 col-sm-2 col-md-2 pl0 pr0">
									<div class="carousel-caption">
										<p>Выставка</p>
										<?php $date = explode('-', $mex->date_end); ?>
										<span>До <?=((int) $date[2] . ' ' . Controller_Layout::$months_genitive[(int)$date[1]])?></span>
										<h2><?=(wordwrap($mex->title, 60, "<br/>\n"))?><br/></h2>
									</div>
								</div>
								<div class="col-xs-10 col-sm-10 col-md-10 pl0 pr0">
									<?php if ($mex->pictures->count_all()) : ?>
										<?php $img = $mex->pictures->where('cover', '=', '1')->order_by('date', 'DESC')->find(); ?>
										<img width="798" src="/assets/upload/items_pictures/<?=$img->name?>.<?=$img->ext?>" alt=""/>
									<?php endif; ?>
								</div>
								<!--<canvas height='493' width='798' id='example' class="col-xs-10 col-sm-10 col-md-10 pl0 pr0"></canvas>
								<script>
									var example = document.getElementById("example"),
										ctx = example.getContext('2d'),
										img = new Image();
									img.src = '/assets/img/carouselSlide.jpg';
									ctx.strokeRect(0, 0, 493, 798);
									ctx.drawImage(img, 0, 0);
								</script>-->
							</div>
						</div>
						<?php $i += 1; ?>
					<?php endforeach; ?>
				</div>
				<div class="row">
					<div class="carouselBar col-xs-2 col-sm-2 col-md-2">
						<a class="left carousel-control mb15" href="#indexCarousel" data-slide="next">
							<div id="carousel-index">1/<?=$total_exhibitions?></div>
							Eще >
						</a>
						<hr class="transition-timer-carousel-progress-bar" />
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php if (!empty($total_exhibitions) || !empty($total_events)) : ?>
	<div class="events mb90">
		<div class="container">
			<div class="container container-main">
				<h1 class="text-center">События</h1>
			</div>
		</div>
		<?php if (!empty($total_exhibitions)) : ?>
			<div class="row-fill">
				<div class="container">
					<div class="container container-main">
						<div class="row">
							<?php foreach ($exhibitions as $ex) : ?>
								<div class="col-xs-6 col-sm-6 col-md-6 pl0">
									<div class="row">
										<div class="col-xs-4 col-sm-4 col-md-4 text-right">
											<?php $date = explode('-', $ex->date_end); ?>
											<span><a href="/">До <?=((int) $date[2] . ' ' . Controller_Layout::$months_genitive[(int)$date[1]])?></a></span>
										</div>
										<div class="col-xs-8 col-sm-8 col-md-8 pl0">
											<p><a href="/"><?=$ex->title?></a></p>
											<span class="eventTag"><a href="/exhibitions">Выставки</a></span>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="container">
			<div class="container container-main">
				<div class="row">
					<?php foreach ($events as $ev) : ?>
						<div class="col-xs-6 col-sm-6 col-md-6 pl0">
							<div class="row">
								<div class="col-xs-4 col-sm-4 col-md-4 text-right">
									<span>
										<a href="/events/<?=$ev->id?>">
											<?php if (date('Y-m-d', strtotime($ev->date)) == date('Y-m-d')) : ?>
												Сегодня<br/>в <?=Date::format($ev->date, 'H:i')?>
											<?php elseif (date_diff(date_create(Date::format($ev->date, 'Y-m-d')), date_create(date('Y-m-d')))->format('%a') == 1) : ?>
												Завтра<br/>в <?=Date::format($ev->date, 'H:i')?>
											<?php else : ?>
												<?=Date::format($ev->date, 'd F')?><br/>в <?=Date::format($ev->date, 'H:i')?>
											<?php endif; ?>
										</a>
									</span>
								</div>
								<div class="col-xs-8 col-sm-8 col-md-8 pl0">
									<p><a href="/events/<?=$ev->id?>"><?=$ev->title?></a></p>
									<?php if (!empty($event_types[$ev->type])) : ?>
										<span class="eventTag">
											<a href="/events?type=<?=$ev->type?>"><?=$event_types[$ev->type]?> <?php if (!empty($ev->kids)) : ?>для детей<?php endif; ?></a>
										</span>
									<?php endif; ?>
									<?php switch ($ev->apply) :
										case 1:
											echo '<span class="text-grey">' . $event_apply_types[$ev->apply] . '</span>';
										break;

										case 2:
											echo '<span class="text-grey">' . $event_apply_types[$ev->apply] . ': ' . $ev->apply_phone . '</span>';
										break;

										case 3:
											echo '<a class="btn btn-grey btn-xs" href="javascript:void(0);">Записаться</a>';
										break;
									endswitch; ?>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
					<div class="col-xs-12 col-sm-12 col-md-12 text-center">
						<p class="text-grey mb10">Музей оставляет за собой право изменять дату и время проведения мероприятия.</p>
						<a class="btn btn-grey btn-xs" href="/events">Все события</a>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

<div class="container permanentExhibition">
	<div class="container container-main">
		<div class="row">
			<div class="col-xs-7 col-sm-7 col-md-7 pl0">
				<h2 class="mb30">Залы постоянной экспозиции</h2>
				<h4>Искусство разных стран и не только</h4>
				<div class="row">
					<?php $i = 1; ?>
					<?php $totalItems = $total_const_exhibitions / 2; // Make 2 columns ?>
					<?php foreach ($const_exhibitions as $ch) : ?>
						<?php if ($i === 1 || ($i % $totalItems) === 1) : ?><div class="col-xs-6 col-sm-6 col-md-6"><?php endif; ?>
							<span><a href="/exhibitions/<?=$ch->id?>"><?=$ch->title?></a></span>
						<?php if ($i === 0 || ($i % $totalItems) === 0) : ?></div><?php endif; ?>
						<?php $i += 1; ?>
					<?php endforeach; ?>
					
				</div>
			</div>
			<div class="col-xs-5 col-sm-5 col-md-5 text-right">
				<a class="virtualTour mt20" href="/virtual_tour"></a>
			</div>
		</div>
	</div>
</div>

<div class="container news">
	<div class="container container-main">
		<div class="row"><h2>Новости</h2></div>

		<?=View::factory('front/news/_news_block', array('news' => $news))?>

		<?php if ($total_news > 10) : ?>
			<div class="text-center"><a class="btn btn-grey btn-xs" href="/news">Все новости</a></div>
		<?php endif; ?>
	</div>
</div>

<?php if (!empty($polls->title)) : ?>
	<?php $answers = $polls->answers->where('user_answer', '=', '0')->find_all(); ?>
	<?php $totalAnswers = $answers->count(); ?>
	<?php if (!empty($totalAnswers)) : ?>
		<div class="quiz">
			<div class="container">
				<div class="container container-main">
					<h3><?=$polls->title?></h3>
					<?php $i = 1; ?>
					<?php foreach ($answers as $a) : ?>
						<div class="radio">
							<label for="accept<?=$i?>">
								<input id="accept<?=$i?>" name="accept<?=$i?>" type="radio"/>
								<?=$a->text?>
							</label>
						</div>
						<?php $i += 1; ?>
					<?php endforeach; ?>
					<?php if ($polls->user_answer == 1) : ?>
						<div class="radio">
							<label for="accept<?=$i?>">
								<input id="accept<?=$i?>" name="accept<?=$i?>" type="radio"/>
								Другое
							</label>
						</div>
						<div class="form-group row">
							<div class="col-xs-6 col-sm-6 col-md-6">
								<input class="form-control" type="text" placeholder="Напишите свой вариант" />
							</div>
						</div>
					<?php endif; ?>

					<button type="button" class="btn btn-black btn-sm">Проголосовать</button>

					<div class="quizInfo">
						<?php foreach ($answers as $a) : ?>
							<div class="row">
								<div class="col-xs-2 col-sm-2 col-md-2"><?=$a->result?> (<?=$a->result_percent?>%)</div>
								<div class="col-xs-10 col-sm-10 col-md-10"><?=$a->text?></div>
								<div class="col-xs-12 col-sm-12 col-md-12" style="width: <?=($a->result_percent ? ($a->result_percent + 3) : 0)?>%"><div></div></div>
							</div>
						<?php endforeach; ?>
						<?php if ($polls->user_answer == 1) : ?>
							<div class="row">
								<div class="col-xs-2 col-sm-2 col-md-2"><?=$user_answers_total?> (<?=$user_answers_count?>%)</div>
								<div class="col-xs-10 col-sm-10 col-md-10">Другое</div>
								<div class="col-xs-12 col-sm-12 col-md-12" style="width: <?=($user_answers_count ? ($user_answers_count + 3) : 0)?>%"><div></div></div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
<?php endif; ?>

<div class="container partners">
	<div class="container container-main">
		<div class="row"><h3>Партнеры</h3></div>
		<div class="row">
			<div class="col-xs-2 col-sm-2 col-md-2 text-center"><img src="/assets/img/brand4.png" alt=""/></div>
		</div>
	</div>
</div>