<div class="container containerGallery mb70">
	<div class="container container-main">

		<div class="row" id="links">

			<div class="col-xs-2 col-sm-2 col-md-2 text-center">
				<a href="/assets/img/brand4.png" title="" data-gallery>
					<img src="/assets/img/brand4.png" alt="">
				</a>
			</div>

			<div class="col-xs-2 col-sm-2 col-md-2 text-center">
				<a href="/assets/img/brand3.png" title="" data-gallery>
					<img src="/assets/img/brand3.png" alt="">
				</a>
			</div>

			<div class="col-xs-2 col-sm-2 col-md-2 text-center">
				<a href="/assets/img/brand2.png" title="" data-gallery>
					<img src="/assets/img/brand2.png" alt="">
				</a>
			</div>

			<div class="col-xs-2 col-sm-2 col-md-2 text-center">
				<a href="/assets/img/virtualThumb.png" title="" data-gallery>
					<img src="/assets/img/virtualThumb.png" alt="">
				</a>
			</div>

			<div class="col-xs-2 col-sm-2 col-md-2 text-center">
				<a href="/assets/img/brand4.png" title="" data-gallery>
					<img src="/assets/img/brand4.png" alt="">
				</a>
			</div>

			<div class="col-xs-2 col-sm-2 col-md-2 text-center">
				<a href="/assets/img/brand3.png" title="" data-gallery>
					<img src="/assets/img/brand3.png" alt="">
				</a>
			</div>

			<div class="col-xs-2 col-sm-2 col-md-2 text-center">
				<a href="/assets/img/brand2.png" title="" data-gallery>
					<img src="/assets/img/brand2.png" alt="">
				</a>
			</div>

			<div class="col-xs-2 col-sm-2 col-md-2 text-center">
				<a href="/assets/img/brand1.png" title="" data-gallery>
					<img src="/assets/img/brand1.png" alt="">
				</a>
			</div>

			<div class="col-xs-2 col-sm-2 col-md-2 text-center">
				<a href="/assets/img/brand4.png" title="" data-gallery>
					<img src="/assets/img/brand4.png" alt="">
				</a>
			</div>

			<div class="col-xs-2 col-sm-2 col-md-2 text-center">
				<a href="/assets/img/brand3.png" title="" data-gallery>
					<img src="/assets/img/brand3.png" alt="">
				</a>
			</div>

		</div>

		<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
		<div id="blueimp-gallery" class="blueimp-gallery">
			<!-- The container for the modal slides -->
			<div class="slides"></div>
			<!-- Controls for the borderless lightbox -->
			<!--<h3 class="title"></h3>-->
			<!--<a class="prev">‹</a>-->
			<!--<a class="next">›</a>-->
			<!--<a class="close">×</a>-->
			<!--<a class="play-pause"></a>-->
			<!--<ol class="indicator"></ol>-->
			<!-- The modal dialog, which will be used to wrap the lightbox content -->
			<div class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="row">

								<div class="modal-header">
									<button type="button" class="close icon-close close-black close-fill" aria-hidden="true"></button>
									<!--<h4 class="modal-title"></h4>-->
								</div>

								<div class="modal-body next"></div>
								<div class="modal-footer">

									<h5>Панно «Рождение Будды»</h5>
									<p class="text-grey">Мьянма. 1970-е гг. Бамбук, лак, плетение, гравировка. 99,5х62 см.</p>

									<button type="button" class="btn pull-left prev">
										<i class="icon-arrLeft"></i>
									</button>
									<button type="button" class="btn next">
										<i class="icon-arrRight"></i>
									</button>

								</div>
						</div>

						<div class="helper"></div>

					</div>
				</div>
			</div>
		</div>

	</div>
</div>