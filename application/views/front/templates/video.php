<div class="row-fill fill-black mb70">
	<div class="container">
		<div class="container container-main">
			<div class="row text-center">
				<div class="col-xs-8 col-sm-8 col-md-8 col-xs-offset-2 col-sm-offset-2 col-md-offset-2">
					<iframe class="mb20" width="640" height="480" src="<?=$data->title?>" frameborder="0" allowfullscreen></iframe>
					<p class="text-grey mb20"><?=$data->text?></p>
				</div>
			</div>
		</div>
	</div>
</div>