<div class="container">
	<div class="container container-main">

		<div id="indexCarousel" class="carousel slide" data-interval="false" data-ride="carousel">

			<!-- Carousel items -->
			<div class="carousel-inner">
				<div class="active item">
					<div class="row">

						<div class="col-xs-2 col-sm-2 col-md-2 pl0 pr0">
							<div class="carousel-caption">
								<h2 class="mt30 mb30">Голова ученика Будды</h2>
								<p>Мьянма, пров. Сагаин. 13–14 вв. Мрамор, резьба, роспись, позолота. В. 12,4 см.</p>
							</div>
						</div>

						<div class="col-xs-10 col-sm-10 col-md-10 pl0 pr0 mb30">
							<img src="/assets/img/carouselSlide.jpg" alt=""/>
						</div>

					</div>
					<div class="row">

						<div class="col-xs-2 col-sm-2 col-md-2 pl0 pr0"></div>
						<div class="col-xs-10 col-sm-10 col-md-10 pl0 pr0">
							<p>Голова является фрагментом алтарной композиции, в которой по обе стороны статуи Будды располагались меньшие по размеру фигуры учеников-монахов с бритыми головами. Такая композиция получила распространение в храмовой скульптуре Паганского королевства (11–13 вв.) – первого мьянманского государства. Мраморные изображения традиционно украшали росписью и позолотой, о чем говорят их следы на этой скульптуре. Лик монаха с полуприкрытыми глазами и легкой улыбкой на устах напоминает лицо Будды, отсутствуют лишь ушниша (выпуклость на темени – знак мудрости) и мелкие завитки волос.</p>
						</div>

					</div>
				</div>
				<div class="item">
					<div class="row">

						<div class="col-xs-2 col-sm-2 col-md-2 pl0 pr0">
							<div class="carousel-caption">
								<h2 class="mt30 mb30">Голова ученика Будды</h2>
								<p>Мьянма, пров. Сагаин. 13–14 вв. Мрамор, резьба, роспись, позолота. В. 12,4 см.</p>
							</div>
						</div>

						<div class="col-xs-10 col-sm-10 col-md-10 pl0 pr0 mb30">
							<img src="/assets/img/carouselSlide.jpg" alt=""/>
						</div>

					</div>
					<div class="row">

						<div class="col-xs-2 col-sm-2 col-md-2 pl0 pr0"></div>
						<div class="col-xs-10 col-sm-10 col-md-10 pl0 pr0">
							<p>Голова является фрагментом алтарной композиции, в которой по обе стороны статуи Будды располагались меньшие по размеру фигуры учеников-монахов с бритыми головами. Такая композиция получила распространение в храмовой скульптуре Паганского королевства (11–13 вв.) – первого мьянманского государства. Мраморные изображения традиционно украшали росписью и позолотой, о чем говорят их следы на этой скульптуре. Лик монаха с полуприкрытыми глазами и легкой улыбкой на устах напоминает лицо Будды, отсутствуют лишь ушниша (выпуклость на темени – знак мудрости) и мелкие завитки волос.</p>
						</div>

					</div>
				</div>
				<div class="item">
					<div class="row">

						<div class="col-xs-2 col-sm-2 col-md-2 pl0 pr0">
							<div class="carousel-caption">
								<h2 class="mt30 mb30">Голова ученика Будды</h2>
								<p>Мьянма, пров. Сагаин. 13–14 вв. Мрамор, резьба, роспись, позолота. В. 12,4 см.</p>
							</div>
						</div>

						<div class="col-xs-10 col-sm-10 col-md-10 pl0 pr0 mb30">
							<img src="/assets/img/carouselSlide.jpg" alt=""/>
						</div>

					</div>
					<div class="row">

						<div class="col-xs-2 col-sm-2 col-md-2 pl0 pr0"></div>
						<div class="col-xs-10 col-sm-10 col-md-10 pl0 pr0">
							<p>Голова является фрагментом алтарной композиции, в которой по обе стороны статуи Будды располагались меньшие по размеру фигуры учеников-монахов с бритыми головами. Такая композиция получила распространение в храмовой скульптуре Паганского королевства (11–13 вв.) – первого мьянманского государства. Мраморные изображения традиционно украшали росписью и позолотой, о чем говорят их следы на этой скульптуре. Лик монаха с полуприкрытыми глазами и легкой улыбкой на устах напоминает лицо Будды, отсутствуют лишь ушниша (выпуклость на темени – знак мудрости) и мелкие завитки волос.</p>
						</div>

					</div>
				</div>
			</div>

			<div class="row">
				<div class="carouselBar col-xs-2 col-sm-2 col-md-2">
					<a class="left carousel-control mb15" href="#indexCarousel" data-slide="next">
						<div id="carousel-index">1/3</div>
						Eще >
					</a>
					<hr class="transition-timer-carousel-progress-bar" />
				</div>
			</div>

		</div>

	</div>
</div>