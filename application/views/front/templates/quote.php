<div class="container mb70">
	<div class="container container-main">
		<blockquote class="row">
			<div class="col-xs-1 col-sm-1 col-md-1 text-center">
				<span class="fs50">&laquo;</span>
			</div>
			<div class="col-xs-10 col-sm-10 col-md-10 text-center">
				<h5><?=$data->text?></h5>
				<p class="text-grey"><?=$data->title?></p>
			</div>
			<div class="col-xs-1 col-sm-1 col-md-1 text-center">
				<span class="fs50">&raquo;</span>
			</div>
		</blockquote>
	</div>
</div>