<div class="container">
	<div class="container container-main">
		<div class="media mb70">
			<?php if ($data->pictures->count_all()) : ?>
				<?php $img = $data->pictures->where('cover', '=', '1')->order_by('date', 'DESC')->find(); ?>
				<div class="pull-left mw208">
					<div class="picWrap mwFull">
						<img class="mwFull" src='/assets/upload/items_pictures/<?=$img->name?>_200_200.<?=$img->ext?>' alt="" />
					</div>
				</div>
			<?php endif; ?>
			<div class="media-body">
				<p><?=$data->text?></p>
			</div>
		</div>
	</div>
</div>