<div class="container">
	<div class="container container-main">
		<div class="tableDiv mb70">
			<?php if ($data->pictures->count_all()) : ?>
				<?php $img = $data->pictures->where('cover', '=', '1')->order_by('date', 'DESC')->find(); ?>
				<div class="pr20">
					<div class="picWrap">
						<img width="600px" src='/assets/upload/items_pictures/<?=$img->name?>_1000.<?=$img->ext?>' alt="" />
					</div>
				</div>
			<?php endif; ?>
			<div class="v-a-b">
				<p class="text-grey mb0"><?=$data->text?></p>
			</div>
		</div>
	</div>
</div>