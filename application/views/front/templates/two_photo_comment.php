<div class="container">
	<div class="container container-main">
		<div class="row mb70">
			<?php if ($data->pictures->count_all()) : ?>
				<?php $imgages = $data->pictures->where('cover', '=', '1')->order_by('date', 'DESC')->find_all(); ?>
				<div class="col-xs-5 col-sm-5 col-md-5">
					<div class="picWrap mwFull mb30">
						<img class="mwFull" width="600px" src='/assets/upload/items_pictures/<?=$img->name?>_1000.<?=$img->ext?>' alt="" />
					</div>
					<p class="text-grey"><?=$data->text?></p>
				</div>
				<div class="col-xs-5 col-sm-5 col-md-5">
					<div class="picWrap mwFull mb30">
						<img class="mwFull" width="600px" src='/assets/upload/items_pictures/<?=$img->name?>_1000.<?=$img->ext?>' alt="" />
					</div>
					<p class="text-grey"><?=$data->text2?></p>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>