<?php if ($data->pictures->count_all()) : ?>
	<div class="container containerGallery mb70">
		<div class="container container-main">
			<div class="row" id="links">
				<?php $images = $data->pictures->where('cover', '=', '1')->order_by('date')->find_all(); ?>
				<?php $titles = explode(';', $data->title); ?>
				<?php $partners = array(); ?>
				<?php foreach ($images as $i => $v) : ?>
					<?php $partners[$i] = array(
						'title'		=> (stristr($titles[$i], 'http') === FALSE ? 'http://' : '') . $titles[$i],
						'img_name'	=> $v->name,
						'img_ext'	=> $v->ext,
					); ?>
				<?php endforeach; ?>
				<?php foreach ($partners as $p) : ?>
					<div class="col-xs-2 col-sm-2 col-md-2 text-center">
						<a href="<?=$p['title']?>" target="_blank"><img src="/assets/upload/items_pictures/<?=$p['img_name']?>_200_200.<?=$p['img_ext']?>" alt=""/></a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endif; ?>