<div class="container search mb40">
	<div class="container container-main">
		<h3 class="mt30 mb30">Результаты поиска</h3>
		<ul class="searchFilter mb30">
			<li class="active"><a class="btn btn-grey btn-xs" href="/">Искать везде</a></li>
			<li><a class="btn btn-grey btn-xs" href="/">Выставки</a></li>
			<li><a class="btn btn-grey btn-xs" href="/">События</a></li>
			<li><a class="btn btn-grey btn-xs" href="/">Новости</a></li>
		</ul>
		<p class="text-grey mb30">Найдено: <?=3?></p>
		<div class="searchItem">
			<h5><a class="text-red" href="">Вчера прошла <b>выставка про самое интересное в музее.</b></a></h5>
		</div>
		<div class="searchItem">
			<h5><a class="text-red" href="">Скоро состоится <b>выставка</b>, посвященная… <b>про самое интересное в нашем музее.</b></a></h5>
		</div>
		<div class="searchItem">
			<h5><a class="text-red" href="">Вечер встреч</a></h5>
			<p>Намечается <b>выставка</b> о прошлых… выясняется интересное положение дел… про все, <b>про самое интересное</b> нам расскажет… <b>В музее</b> будут представлены …</p>
		</div>
	</div>
</div>