<div class="container mainItem mb70">
	<div class="container container-main">
		<div class="row">
			<h1 class="text-center mt70 mb70"><?=$title?></h1>
			<div class="col-xs-10 col-sm-10 col-md-10 col-xs-offset-2 col-sm-offset-2 col-md-offset-2">
				<h3 class="mb30"><?=$item->title?></h3>
				<?php if ($item->pictures->count_all()) : ?>
					<?php $img = $item->pictures->where('cover', '=', '1')->order_by('date', 'DESC')->find(); ?>
					<div class="thumbItem">
						<img width="360" src="/assets/upload/items_pictures/<?=$img->name?>.<?=$img->ext?>" alt=""/>
					</div>
				<?php endif; ?>
				<p class="mb20"><?=$item->text?></p>
				<!--<p class="mb40">
					<a href="/download" class="text-red"><span class="icon-download text-red"></span> Скачать <span class="text-black">(PDF, 25MB)</span></a>
				</p>
				<script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
				<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki" data-yashareTheme="counter"></div>
				-->
			 </div>
		</div>
	</div>
</div>