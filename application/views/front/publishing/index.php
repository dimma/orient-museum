<div class="container mainCont mb70">
	<div class="container container-main">
		<h1 class="text-center mt70 mb70"><?=$title?></h1>
		<div class="row">
			<?php foreach ($publish as $p) : ?>
				<div class="col-xs-3 col-sm-3 col-md-3 mb70">
					<a href="/publishing/<?=$p->id?>">
						<div class="thumbItem">
							<?php if ($p->pictures->count_all()) : ?>
								<?php $img = $p->pictures->where('cover', '=', '1')->order_by('date', 'DESC')->find(); ?>
								<img src="/assets/upload/items_pictures/<?=$img->name?>_200_200.<?=$img->ext?>" alt=""/>
							<?php endif; ?>
						</div>
						<p><?=$p->title?></p>
					</a>
				</div>
			<?php endforeach; ?>
		</div>
		<p class="text-grey text-center">
			Данные издания вы можете приобрести в киоске музея на первом этаже.
			<br />
			Наличие изданий, пожалуйста, уточняйте заранее.
		</p>
	</div>
</div>