<div class="wrapper wrapper-content animated fadeInRight">
	<ul class="nav nav-tabs page-tabs">
		<li class="active"><a data-toggle="tab" href="#tab-common">Общее</a></li>
		<li class=""><a data-toggle="tab" href="#tab-settings"><i class="fa fa-cog"></i>&nbsp;Настройки</a></li>
	</ul>
	<div class="ibox float-e-margins">
		<div class="ibox-content">
			<?=Form::open('/admin/ajax/add_item', array('class' => 'ajax-form', 'id' => 'add-news-form'))?>
				<div class="tab-content">
					<div id="tab-common" class="tab-pane active">
						<input type="hidden" name="item_model" value="new">
						<div class="form-group">
							<label>Заголовок</label>
							<input type="text" class="form-control" name="title">
						</div>
						<div class="form-group">
							<label>Анонс</label>
							<textarea name="anons" class="form-control"></textarea>
						</div>
						<div class="form-group">
							<div id="item-pic"></div>
							<div class="row">
								<div class="col-sm-3">
									<div class="upload-wrap">
										<span class="btn file-upload btn-outline fileinput-button btn-white btn-block">
											<i class="fa fa-picture-o"></i>&nbsp;
											<span>Загрузить изображение</span>
											<input type="file" name="picture" data-url="/admin/ajax/upload_item_pic" accept=""/>
										</span>
										<div class="progress progress-striped active no-margin-bottom">
											<div style="width: 0" aria-valuemax="100" aria-valuemin="0" aria-valuenow="75" role="progressbar" class="progress-bar progress-bar-danger">
												<span class="sr-only"></span>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-9" style="padding-top: 8px;">
									<p><i>Минимальный размер изображения <b>1000&times;600</b> px.</i></p>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label>Дата:</label>
							<div class="row date-row">
								<div class="col-sm-3">
									<div class="input-group date">
										<input type="text" name="date" class="form-control" value="<?=date('d.m.Y')?>"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
							</div>
						</div>
						<div class="hr-line-dashed"></div>
						<div class="row">
							<div class="col-sm-2">
								<h2>Содержание</h2>
							</div>
							<div class="col-sm-8" style="padding-top: 9px">
								<button type="button" class="btn btn-outline btn-default btn-xs" data-toggle="modal" data-target="#modal-templates-list">&nbsp;&nbsp;Добавить шаблон&nbsp;&nbsp;</button>
							</div>
						</div>
						<div>
							<p><i>Заполните страницу с помощью шаблонов.</i></p>
						</div>
					</div>
					<div id="tab-settings" class="tab-pane">
						<div class="form-group">
							<label>URL</label>
							<input type="text" class="form-control" name="url">
						</div>
						<div class="form-group">
							<label>Заголовок страницы (title)</label>
							<input type="text" class="form-control" name="meta_title">
						</div>
						<div class="form-group">
							<label>Ключевые слова (keywords)</label>
							<textarea name="keywords" class="form-control"></textarea>
						</div>
						<div class="form-group">
							<label>Описание страницы (description)</label>
							<textarea name="description" class="form-control"></textarea>
						</div>
					</div>
				</div>
				<div class="hr-line-dashed"></div>
				<div class="form-group">
					<input type="submit" value="Сохранить" class="btn btn-danger">&nbsp;&nbsp;&nbsp;
					<a href="/admin/news" class="btn btn-default">Отмена</a>
				</div>
				<div class="alert alert-success alert-dismissable form-success">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					Изменения успешно сохранены.
				</div>
			<?=Form::close()?>
		</div>
	</div>
</div>

<?=View::factory('admin/templates/_modal_list')?>
