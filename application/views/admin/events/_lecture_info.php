<div id="modal-lecture-info" class="modal fade" aria-hidden="true">
	<div class="modal-dialog">
		<?=Form::open('/admin/ajax/update_item', array('class' => 'ajax-form', 'id' => 'update-lecture-form'))?>
			<input type="hidden" name="item_model" value="lecture">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title"></h4>
				</div>
				<div class="modal-body">
					<div id="lecture-info-body"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
					<input type="submit" class="btn btn-danger" value="Сохранить">
				</div>
			</div>
		<?=Form::close()?>
	</div>
</div>