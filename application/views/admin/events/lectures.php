<div class="wrapper wrapper-content animated fadeInRight">
	<div class="ibox float-e-margins">
		<div class="ibox-content">
			<div class="table-responsive" id="items-list-wrap" <? if($items->count() == 0) { ?>style="display: none"<? } ?>>
				<table class="table table-striped table-hover items-table">
					<thead>
						<tr>
							<th class="thin text-center">&#8470;</th>
							<th>Название</th>
							<th></th>
						</tr>
					</thead>
					<tbody id="lectures-list">
						<?=View::factory('admin/events/_lectures_list', array('items' => $items))?>
					</tbody>
				</table>
			</div>
			<? if($items->count() == 0) { ?>
				<div id="items-empty">Нет циклов.</div>
			<? } ?>
		</div>
	</div>
</div>
<? if($items->count()) { ?>
	<?=View::factory('admin/events/_lecture_info')?>
<? } ?>
<?=View::factory('admin/events/_lecture_add')?>