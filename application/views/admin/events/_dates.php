<? foreach($dates as $date) { ?>
	<div class="row date-row">
		<div class="col-sm-3">
			<div class="input-group date">
				<input type="text" name="eventdates[<?=$date->id?>][date]" value="<?=Date::format($date->date,'d.m.Y')?>" class="form-control"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="input-group">
				<select class="form-control pull-left" name="eventdates[<?=$date->id?>][hour]">
					<? for($i = 10; $i <= 22; $i++) { ?>
						<option <? if($i == Date::format($date->date, 'H')) { ?>selected="selected"<? } ?>><?=$i?></option>
					<? } ?>
				</select>
				<span class="input-group-addon no-borders">:</span>
				<select class="form-control pull-left" name="eventdates[<?=$date->id?>][minute]">
					<option <? if(Date::format($date->date, 'i') == '00') { ?>selected="selected"<? } ?>>00</option>
					<option <? if(Date::format($date->date, 'i') == '15') { ?>selected="selected"<? } ?>>15</option>
					<option <? if(Date::format($date->date, 'i') == '30') { ?>selected="selected"<? } ?>>30</option>
					<option <? if(Date::format($date->date, 'i') == '45') { ?>selected="selected"<? } ?>>45</option>
				</select>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="lecture-date-action"><a href="#" class="delete-event-date text-danger" data-id="<?=$date->id?>">Удалить</a></div>
		</div>
	</div>
<? } ?>