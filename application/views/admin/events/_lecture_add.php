<div id="modal-add-lecture" class="modal fade" aria-hidden="true">
	<div class="modal-dialog">
		<?=Form::open('/admin/ajax/add_item', array('class' => 'ajax-form', 'id' => 'add-lecture-form'))?>
			<input type="hidden" name="item_model" value="lecture">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Добавление цикла лекций</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Название цикла</label>
						<input type="text" class="form-control" name="title">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
					<input type="submit" class="btn btn-danger" value="Сохранить">
				</div>
			</div>
		<?=Form::close()?>
	</div>
</div>