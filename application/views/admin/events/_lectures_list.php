<? foreach($items as $item) { ?>
	<tr item_id="<?=$item->id?>" data-toggle="modal" data-target="#modal-lecture-info" class="<? if($item->status == 0) { ?>inactive<? } ?>">
		<td class="thin text-center"><?=$item->id?></td>
		<td class="item-title"><?=$item->title?></td>
		<td class="switch-cell"><input type="checkbox" class="js-switch" data-item="lecture" data-id="<?=$item->id?>" <? if($item->status == '1') { ?>checked="checked"<? } ?> /></td>
	</tr>
<? } ?>