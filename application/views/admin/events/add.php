<div class="wrapper wrapper-content animated fadeInRight">
	<ul class="nav nav-tabs page-tabs">
		<li class="active"><a data-toggle="tab" href="#tab-common">Общее</a></li>
		<li class=""><a data-toggle="tab" href="#tab-settings"><i class="fa fa-cog"></i>&nbsp;Настройки</a></li>
	</ul>
	<div class="ibox float-e-margins">
		<div class="ibox-content">
			<?=Form::open('/admin/ajax/add_item', array('class' => 'ajax-form', 'id' => 'add-event-form'))?>
				<div class="tab-content">
					<div id="tab-common" class="tab-pane active">
						<input type="hidden" name="item_model" value="event">
						<div class="form-group">
							<select name="type" class="form-control" data-change="additional">
								<option>Выбрать категорию</option>
								<?php foreach ($event_types as $id => $name) : ?>
									<option value="<?=$id?>"><?=$name?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group" data-additional="type" data-value="1" style="display: none">
							<select name="lecture_id" class="form-control">
								<option value="0">Выбрать цикл</option>
								<? foreach(ORM::factory('Lecture')->find_all() as $lecture) { ?>
									<option value="<?=$lecture->id?>"><?=$lecture->title?></option>
								<? } ?>
							</select>
						</div>
						<div class="form-group">
							<label>Заголовок</label>
							<input type="text" class="form-control" name="title">
						</div>
						<div class="form-group">
							<label>Когда пройдет</label>
							<div class="row date-row">
								<div class="col-sm-3">
									<div class="input-group date">
										<input type="text" name="date" class="form-control"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="input-group">
										<select class="form-control pull-left" name="hour">
											<? for($i = 10; $i <= 22; $i++) { ?>
											<option><?=$i?></option>
											<? } ?>
										</select>
										<span class="input-group-addon no-borders">:</span>
										<select class="form-control pull-left" name="minute">
											<option>00</option>
											<option>15</option>
											<option>30</option>
											<option>45</option>
										</select>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="lecture-date-action"><a href="#" id="add-event-date">Добавить дату</a></div>
								</div>
							</div>
							<div id="event-dates-list"></div>
						</div>
						<div class="form-group">
							<label>Запись на событие</label>
							<select name="apply" class="form-control" data-change="additional">
								<option>Выбрать</option>
								<?php foreach (Model_Event::$event_appply_types as $id => $name) : ?>
									<option value="<?=$id?>"><?=$name?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group" data-additional="apply" data-value="2" style="display: none">
							<label>Телефон (формат записи +7 495 123-45-67)</label>
							<input type="text" name="apply_phone" class="form-control">
						</div>
						<div class="form-group" data-additional="apply" data-value="3" style="display: none">
							<label>Максимальное количество участников</label>
							<input type="text" name="apply_max" class="form-control">
						</div>
						<div class="form-group" style="padding-top: 10px;">
							<label class="checkbox-inline i-checks"><input type="checkbox" name="kids" value="1">&nbsp;&nbsp;&nbsp;Для детей</label>
						</div>
						<div class="hr-line-dashed"></div>
						<div class="row">
							<div class="col-sm-2">
								<h2>Содержание</h2>
							</div>
							<div class="col-sm-8" style="padding-top: 9px">
								<button type="button" class="btn btn-outline btn-default btn-xs" data-toggle="modal" data-target="#modal-templates-list">&nbsp;&nbsp;Добавить шаблон&nbsp;&nbsp;</button>
							</div>
						</div>
						<div>
							<p><i>Заполните страницу с помощью шаблонов.</i></p>
						</div>
					</div>
					<div id="tab-settings" class="tab-pane">
						<div class="form-group">
							<label>URL</label>
							<input type="text" class="form-control" name="url">
						</div>
						<div class="form-group">
							<label>Заголовок страницы (title)</label>
							<input type="text" class="form-control" name="meta_title">
						</div>
						<div class="form-group">
							<label>Ключевые слова (keywords)</label>
							<textarea name="keywords" class="form-control"></textarea>
						</div>
						<div class="form-group">
							<label>Описание страницы (description)</label>
							<textarea name="description" class="form-control"></textarea>
						</div>
					</div>
				</div>
				<div class="hr-line-dashed"></div>
				<div class="form-group">
					<input type="submit" value="Сохранить" class="btn btn-danger">&nbsp;&nbsp;&nbsp;
					<a href="/admin/events" class="btn btn-default">Отмена</a>
				</div>
			<?=Form::close()?>
		</div>
	</div>
</div>

<?=View::factory('admin/templates/_modal_list')?>
