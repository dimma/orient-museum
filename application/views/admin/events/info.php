<div class="wrapper wrapper-content animated fadeInRight">
	<ul class="nav nav-tabs page-tabs">
		<li class="active"><a data-toggle="tab" href="#tab-common">Общее</a></li>
		<li class=""><a data-toggle="tab" href="#tab-settings"><i class="fa fa-cog"></i>&nbsp;Настройки</a></li>
	</ul>
	<div class="ibox float-e-margins">
		<div class="ibox-content">
			<?=Form::open('/admin/ajax/update_item', array('class' => 'ajax-form', 'id' => 'update-event-form'))?>
				<div class="tab-content">
					<div id="tab-common" class="tab-pane active">
						<input type="hidden" name="item_model" value="event">
						<input type="hidden" name="item_id" value="<?=$item->id?>">
						<div class="form-group">
							<select name="type" class="form-control" data-change="additional">
								<option>Выбрать категорию</option>
								<?php foreach ($event_types as $id => $name) : ?>
									<option value="<?=$id?>" <?php if ($item->type == $id) : ?>selected="selected"<?php endif; ?>><?=$name?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group" data-additional="type" data-value="1" <? if($item->type !== '1') { ?>style="display: none"<? } ?>>
							<select name="lecture_id" class="form-control">
								<option value="0">Выбрать цикл</option>
								<? foreach(ORM::factory('Lecture')->find_all() as $lecture) { ?>
									<option value="<?=$lecture->id?>" <? if($lecture->id == $item->lecture_id) { ?>selected="selected"<? } ?>><?=$lecture->title?></option>
								<? } ?>
							</select>
						</div>
						<div class="form-group">
							<label>Заголовок</label>
							<input type="text" class="form-control" name="title" value="<?=$item->title?>">
						</div>
						<div class="form-group">
							<label>Когда пройдет</label>
							<div class="row date-row">
								<div class="col-sm-3">
									<div class="input-group date">
										<input type="text" name="date" value="<?=Date::format($item->date,'d.m.Y')?>" class="form-control"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="input-group">
										<select class="form-control pull-left" name="hour">
											<? for($i = 10; $i <= 22; $i++) { ?>
											<option <? if($i == Date::format($item->date, 'H')) { ?>selected="selected"<? } ?>><?=$i?></option>
											<? } ?>
										</select>
										<span class="input-group-addon no-borders">:</span>
										<select class="form-control pull-left" name="minute">
											<option <? if(Date::format($item->date, 'i') == '00') { ?>selected="selected"<? } ?>>00</option>
											<option <? if(Date::format($item->date, 'i') == '15') { ?>selected="selected"<? } ?>>15</option>
											<option <? if(Date::format($item->date, 'i') == '30') { ?>selected="selected"<? } ?>>30</option>
											<option <? if(Date::format($item->date, 'i') == '45') { ?>selected="selected"<? } ?>>45</option>
										</select>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="lecture-date-action"><a href="#" id="add-event-date">Добавить дату</a></div>
								</div>
							</div>
							<div id="event-dates-list">
								<?=View::factory('admin/events/_dates', array('dates' => $item->dates->find_all()))?>
							</div>
						</div>
						<div class="form-group">
							<label>Запись на событие</label>
							<select name="apply" class="form-control" data-change="additional">
								<option>Выбрать</option>
								<?php foreach (Model_Event::$event_appply_types as $id => $name) : ?>
									<option value="<?=$id?>" <?php if ($item->apply == $id) : ?>selected="selected"<?php endif; ?>><?=$name?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group" data-additional="apply" data-value="2" <? if($item->apply !== '2') { ?>style="display: none"<? } ?>>
							<label>Телефон (формат записи +7 495 123-45-67)</label>
							<input type="text" name="apply_phone" value="<?=$item->apply_phone?>" class="form-control">
						</div>
						<div class="form-group" data-additional="apply" data-value="3"  <? if($item->apply !== '3') { ?>style="display: none"<? } ?>>
							<label>Максимальное количество участников</label>
							<input type="text" name="apply_max" value="<?=$item->apply_max?>" class="form-control">
						</div>
						<div class="form-group" style="padding-top: 10px;">
							<label class="checkbox-inline i-checks"><input type="checkbox" name="kids" value="1" <? if($item->kids == '1') { ?>checked="checked" <? } ?>>&nbsp;&nbsp;&nbsp;Для детей</label>
						</div>
						<div class="hr-line-dashed"></div>
						<div class="row">
							<div class="col-sm-2">
								<h2>Содержание</h2>
							</div>
							<div class="col-sm-8" style="padding-top: 9px">
								<button type="button" class="btn btn-outline btn-default btn-xs" data-toggle="modal" data-target="#modal-templates-list">&nbsp;&nbsp;Добавить шаблон&nbsp;&nbsp;</button>
							</div>
						</div>
						<div>
							<p><i>Заполните страницу с помощью шаблонов.</i></p>
						</div>
					</div>
					<div id="tab-settings" class="tab-pane">
						<div class="form-group">
							<label>URL</label>
							<input type="text" class="form-control" name="url" value="<?=$item->url?>">
						</div>
						<div class="form-group">
							<label>Заголовок страницы (title)</label>
							<input type="text" class="form-control" name="meta_title" value="<?=$item->meta_title?>">
						</div>
						<div class="form-group">
							<label>Ключевые слова (keywords)</label>
							<textarea name="keywords" class="form-control"><?=$item->keywords?></textarea>
						</div>
						<div class="form-group">
							<label>Описание страницы (description)</label>
							<textarea name="description" class="form-control"><?=$item->description?></textarea>
						</div>
					</div>
				</div>
				<div class="hr-line-dashed"></div>
				<div class="form-group">
					<input type="submit" value="Сохранить" class="btn btn-danger">&nbsp;&nbsp;&nbsp;
					<a href="/admin/events" class="btn btn-default btn-outline">Отмена</a>
				</div>
				<div class="alert alert-success alert-dismissable form-success">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					Изменения успешно сохранены.
				</div>
			<?=Form::close()?>
		</div>
	</div>
</div>

<?=View::factory('admin/templates/_modal_list')?>
