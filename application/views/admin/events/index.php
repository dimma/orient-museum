<div class="wrapper wrapper-content animated fadeInRight">
	<div class="ibox float-e-margins">
		<div class="ibox-content">
			<div class="table-responsive">
				<table class="table table-striped table-hover items-table">
					<thead>
						<tr>
							<th class="thin text-center">&#8470;</th>
							<th>Дата&nbsp;проведения</th>
							<th>Название</th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody id="events-list">
					<? foreach($items as $item) { ?>
						<tr item_id="<?=$item->id?>" class="<? if($item->status == 0) { ?>inactive<? } ?>">
							<td class="thin text-center"><?=$item->id?></td>
							<td class="thin">
								<?=Date::format($item->date,'d F h:i')?><? if($item->dates->count_all()) { ?>,<br>
								<? foreach($item->dates->find_all() as $date) { ?>
									<?=Date::format($date->date, 'd F H:i')?><br>
								<? } } ?>
							</td>
							<td><a href="/admin/events/<?=$item->id?>"><?=$item->title?></a></td>
							<td><?=$event_types[$item->type]?></td>
							<td class="switch-cell"><input type="checkbox" class="js-switch" data-item="event" data-id="<?=$item->id?>" <? if($item->status == '1') { ?>checked="checked"<? } ?> /></td>
							<td class="text-right thin"><button type="button" class="btn btn-danger btn-outline btn-xs" data-title="событие" data-action="delete" data-model="event" data-id="<?=$item->id?>"><i class="fa fa-times"></i></button></td>
						</tr>
					<? } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
