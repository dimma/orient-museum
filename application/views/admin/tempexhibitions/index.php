<div class="wrapper wrapper-content animated fadeInRight">
	<div class="ibox float-e-margins">
		<div class="ibox-content">
			<div class="table-responsive">
				<table class="table table-striped table-hover items-table">
					<thead>
						<tr>
							<th class="thin text-center">&#8470;</th>
							<th>Дата проведения</th>
							<th>Название</th>
							<? if(!$archive) { ?><th></th><? } ?>
							<th></th>
						</tr>
					</thead>
					<tbody>
					<? foreach($items as $item) { ?>
						<tr item_id="<?=$item->id?>" class="<? if($item->status == 0) { ?>inactive<? } ?>">
							<td class="text-center"><?=$item->id?></td>
							<td class="thin"><?=Date::format($item->date_start,'d F')?>&nbsp;&mdash;&nbsp;<?=Date::format($item->date_end,'d F Y')?></td>
							<td><a href="/admin/tempexhibitions/<?=$item->id?>"><?=$item->title?></a></td>
							<? if(!$archive) { ?><td class="switch-cell"><input type="checkbox" class="js-switch" data-item="tempexhibition" data-id="<?=$item->id?>" <? if($item->status == '1') { ?>checked="checked"<? } ?> /></td><? } ?>
							<td class="text-right thin"><button type="button" class="btn btn-danger btn-outline btn-xs" data-title="выставку" data-action="delete" data-model="tempexhibition" data-id="<?=$item->id?>"><i class="fa fa-times"></i></button></td>
						</tr>
					<? } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
