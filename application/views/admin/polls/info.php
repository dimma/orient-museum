<div class="wrapper wrapper-content animated fadeInRight">
	<ul class="nav nav-tabs page-tabs">
		<li class="active"><a data-toggle="tab" href="#tab-common">Общее</a></li>
		<li><a data-toggle="tab" href="#tab-results"><i class="fa fa-tasks"></i>&nbsp;Результаты</a></li>
	</ul>
	<div class="ibox float-e-margins">
		<div class="ibox-content">
			<?=Form::open('/admin/ajax/update_item', array('class' => 'ajax-form', 'id' => 'update-poll-form'))?>
				<div class="tab-content">
					<div id="tab-common" class="tab-pane active">
						<input type="hidden" name="item_model" value="poll">
						<input type="hidden" name="item_id" value="<?=$item->id?>">
						<div class="form-group">
							<label>Заголовок</label>
							<input type="text" class="form-control" name="title" value="<?=$item->title?>">
						</div>
						<div class="form-group">
							<label>Показывать до:</label>
							<div class="row date-row">
								<div class="col-sm-3">
									<div class="input-group date">
										<input type="text" name="date_end" class="form-control" value="<?=Date::format($item->date_end,'d.m.Y')?>"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
							</div>
						</div>
						<h2>Ответы</h2>
						<div class="form-group" style="margin-top: 20px;">
							<label class="checkbox-inline i-checks"><input type="checkbox" name="user_answer" value="1" <? if($item->user_answer == '1') { ?>checked="checked" <? } ?>>&nbsp;&nbsp;&nbsp;Свой вариант ответа</label>
						</div>
						<div class="form-group">
							<ol class="list" id="pollanswers-list">
								<?=View::factory('admin/polls/_answers', array('answers' => $item->answers->where('user_answer','=','0')->find_all()))?>
							</ol>
						</div>
					</div>
					<div id="tab-results" class="tab-pane">
						<p style="margin-bottom: 20px;">Всего проголосовало: <b><?=$answers_count?></b>.</p>
						<? foreach($item->answers->where('user_answer','=','0')->find_all() as $answer) { ?>
							<h5><?=$answer->text?></h5>
							<div class="row">
								<div class="col-sm-5">
									<div class="progress">
										<div style="width: <?=$answer->result_percent?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?=$answer->result_percent?>" role="progressbar" class="progress-bar progress-bar-<? if($answer->id == $best_answer) { ?>danger<? } else { ?>grey<? } ?>">
											<span class="sr-only"><?=$answer->result_percent?>%</span>
										</div>
									</div>
								</div>
								<div class="col-sm-2">
									<b><?=$answer->result_percent?></b>%&nbsp;(<?=$answer->result?>)
								</div>
							</div>
						<? } ?>
						<? if($user_answers_count) { ?>
							<h5>Свой вариант</h5>
							<div class="row">
								<div class="col-sm-5">
									<div class="progress">
										<div style="width: <?=$user_answers_count?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?=$user_answers_count?>" role="progressbar" class="progress-bar progress-bar-grey">
											<span class="sr-only"><?=$user_answers_count?>%</span>
										</div>
									</div>
								</div>
								<div class="col-sm-2">
									<b><?=$user_answers_count?></b>%&nbsp;(<?=$user_answers_total?>)
								</div>
							</div>
							<h2>Свои варианты</h2>
							<ol>
								<? foreach($item->answers->where('user_answer','=','1')->find_all() as $answer) { ?>
									<li><?=$answer->text?></li>
								<? } ?>
							</ol>
						<? } ?>
					</div>
				</div>
				<div class="hr-line-dashed"></div>
				<div class="form-group">
					<input type="submit" value="Сохранить" class="btn btn-danger">&nbsp;&nbsp;&nbsp;
					<a href="/admin/polls" class="btn btn-default btn-outline">Отмена</a>
				</div>
				<div class="alert alert-success alert-dismissable form-success">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					Изменения успешно сохранены.
				</div>
			<?=Form::close()?>
		</div>
	</div>
</div>

<?=View::factory('admin/templates/_modal_list')?>
