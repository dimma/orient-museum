<div class="wrapper wrapper-content animated fadeInRight">
	<div class="ibox float-e-margins">
		<div class="ibox-content">
			<?=Form::open('/admin/ajax/add_item', array('class' => 'ajax-form', 'id' => 'add-poll-form'))?>
				<input type="hidden" name="item_model" value="poll">
				<div class="form-group">
					<label>Заголовок</label>
					<input type="text" class="form-control" name="title">
				</div>
				<div class="form-group">
					<label>Показывать до:</label>
					<div class="row date-row">
						<div class="col-sm-3">
							<div class="input-group date">
								<input type="text" name="date_end" class="form-control"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
					</div>
				</div>
				<h2>Ответы</h2>
				<div class="form-group" style="margin-top: 20px;">
					<label class="checkbox-inline i-checks"><input type="checkbox" name="user_answer" value="1">&nbsp;&nbsp;&nbsp;Свой вариант ответа</label>
				</div>
				<div class="form-group">
					<ol class="list" id="pollanswers-list">
						<li>
							<div class="row">
								<span class="col-sm-5"><input type="text" class="form-control" name="newanswers[]"></span>
								<span class="col-sm-3 add-answer-wrap"><a href="#" id="add-pollanswer">Добавить</a></span>
							</div>
						</li>
						<li>
							<div class="row">
								<span class="col-sm-5"><input type="text" class="form-control" name="newanswers[]"></span>
							</div>
						</li>
					</ol>
				</div>
				<div class="hr-line-dashed"></div>
				<div class="form-group">
					<input type="submit" value="Сохранить" class="btn btn-danger">&nbsp;&nbsp;&nbsp;
					<a href="/admin/polls" class="btn btn-default">Отмена</a>
				</div>
				<div class="alert alert-success alert-dismissable form-success">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					Изменения успешно сохранены.
				</div>
			<?=Form::close()?>
		</div>
	</div>
</div>

<?=View::factory('admin/templates/_modal_list')?>
