<!DOCTYPE html>
<html>
	<head>
		<title><?=$window_title?></title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="<?=$meta_keywords;?>" />
		<meta name="description" content="<?=$meta_description;?>" />
		<meta name="copyright" content="<?=$meta_copyright;?>" />
		<meta name="author" content="<?=$meta_author;?>" />
		<? foreach($styles as $file => $type) { echo HTML::style($file, array('media' => $type)), "\n"; } ?>
	</head>
	<body>
		<div id="wrapper">
			<nav class="navbar-default navbar-static-side" role="navigation">
				<div class="sidebar-collapse">
					<ul class="nav" id="side-menu">
						<li class="nav-header">
							<span class="admin-logo profile-element">AdminExpo <a href="/" target="_blank" class="pull-right"><i class="fa fa-external-link"></i></a></span>
							<div class="logo-element">AE</div>
						</li>
						<? foreach($menu as $k => $v) { ?>
							<li <? if($controller == $k) { ?>class="active"<? } ?>>
								<a href="/admin/<?=$v['url']?>"><i class="fa fa-<?=$v['icon']?>"></i> <span class="nav-label"><?=$v['name']?></span><? /* <span class="label label-warning pull-right">16/24</span> */ ?><? if(isset($v['items'])) { ?><span class="fa arrow"></span><? } ?></a>
								<? if(isset($v['items'])) { ?>
								<ul class="nav nav-second-level">
									<? foreach($v['items'] as $sub) { ?>
										<li <? if($action == $sub['url']) { ?>class="active" <? } ?>><a href="/admin/<?=$k?>/<?=$sub['url']?>"><?=$sub['name']?></a></li>
									<? } ?>
								</ul>
								<? } ?>
							</li>
						<? } ?>
					</ul>
				</div>
			</nav>
			<div id="page-wrapper" class="gray-bg">
				<div class="row border-bottom">
					<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
						<div class="navbar-header">
							<a class="navbar-minimalize minimalize-styl-2 btn btn-outline btn-danger" href="#"><i class="fa fa-bars"></i></a>
						</div>
						<div class="navbar-left site-logo">
							<img src="/assets/admin/i/museum_logo.png" alt="Музей востока">
							<span>Государственный музей <span class="text-danger">востока</span></span>
						</div>
						<ul class="nav navbar-top-links navbar-right">
							<li><span class="m-r-sm text-muted welcome-message"><strong><?=$user_title?></strong></span></li>
							<li class="lnk-logout"><a href="/admin/logout"><i class="fa fa-sign-out"></i> Выйти</a></li>
						</ul>
					</nav>
				</div>
				<div class="row wrapper border-bottom white-bg page-heading">
					<div class="col-sm-9">
						<h1 class="main-title"><?=$title?></h1>
						<? if($breadcrumbs) { ?>
						<?=View::factory('admin/breadcrumbs', array('breadcrumbs' => $breadcrumbs))?>
						<? } ?>
					</div>
					<div class="col-sm-3">
						<div class="title-action">
							<?=$title_content?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<?=$content?>
					</div>
				</div>
				<div class="footer">
					<div class="pull-right"><a href="http://siteexpo.ru" class="text-danger" target="_blank"><span class="siteexpo-logo"></span></a></div>
					<div>AdminExpo CMS</div>
				</div>
			</div>
		</div>

		<!-- javascript
		================================================== -->
		<? foreach($scripts as $file) { echo HTML::script($file), "\n"; } ?>
	</body>
</html>
