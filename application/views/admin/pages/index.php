<div class="wrapper wrapper-content animated fadeInRight">
	<div class="ibox float-e-margins">
		<div class="ibox-content">
			<div class="table-responsive">
				<table class="table items-table">
					<tbody id="events-list">
						<?php $row_page = ''; ?>
						<?php foreach ($items as $item) : ?>
							<?php if (empty($row_page) || $row_page != $item['parent']) : ?>
								<?php $row_page = $item['parent']; ?>
								<tr>
									<td>
										<b>
											<?php switch ($item['parent']) :
												case 'about':
													echo 'О музее';
												break;

												case 'visitors':
													echo 'Посетителям';
												break;

												case 'shop':
													echo 'Наш магазин';
												break;

												case 'exhibition':
													echo 'Постоянная экспозиция';
												break;

												case 'events':
													echo 'Выставки и события';
												break;
											endswitch; ?>
										</b>
									</td>
									<td></td>
									<td></td>
								</tr>
							<?php endif; ?>
							<tr item_id="<?=$item['id']?>" <?php if ($item['status'] == 0) : ?>class="inactive"<?php endif; ?>>
								<td>
									<a href="/admin/pages/<?=$item['id']?>"><?=$item['title']?></a>
								</td>
								<td>
									<?=$item['url']?>
								</td>
								<td class="switch-cell">
									<input type="checkbox" class="js-switch" data-item="page" data-id="<?=$item['id']?>" <?php if ($item['status'] == 1) : ?>checked="checked"<?php endif; ?> />
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>