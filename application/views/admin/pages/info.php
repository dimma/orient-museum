<div class="wrapper wrapper-content animated fadeInRight">
	<ul class="nav nav-tabs page-tabs">
		<li class="active"><a data-toggle="tab" href="#tab-common">Общее</a></li>
		<li class=""><a data-toggle="tab" href="#tab-settings"><i class="fa fa-cog"></i>&nbsp;Настройки</a></li>
	</ul>
	<div class="ibox float-e-margins">
		<div class="ibox-content">
			<?=Form::open('/admin/ajax/update_item', array('class' => 'ajax-form item-form', 'id' => 'update-page-form'))?>
				<div class="tab-content">
					<div id="tab-common" class="tab-pane active">
						<input type="hidden" name="item_model" value="page"/>
						<input type="hidden" name="item_id" value="<?=$item->id?>"/>
						<div class="form-group">
							<a target="_blank" href="/<?=$item->url?>">Просмотр на сайте</a>
						</div>
						<div class="form-group">
							<label>Заголовок</label>
							<input type="text" class="form-control" name="title" value="<?=$item->title?>">
						</div>
						<div class="form-group">
							<label class="checkbox-inline i-checks">
								<input type="checkbox" name="anchor" value="1" <?php if ($item->anchor == 1) : ?>checked="checked"<?php endif; ?>/>
								Показывать якорь на странице
							</label>
						</div>
						<div class="hr-line-dashed"></div>
						<div class="row">
							<div class="col-sm-2">
								<h2>Содержание</h2>
							</div>
							<div class="col-sm-8" style="padding-top: 9px">
								<button type="button" class="btn btn-outline btn-default btn-xs" data-toggle="modal" data-target="#modal-templates-list">&nbsp;&nbsp;Добавить шаблон&nbsp;&nbsp;</button>
							</div>
						</div>
						<div>
							<p><i>Заполните страницу с помощью шаблонов.</i></p>
						</div>
						<div id="item-templates-list">
							<?=View::factory('admin/templates/_item_templates_list', array('templates' => Model_Template::getModelTemplates('page', $item->id)))?>
						</div>
					</div>
					<div id="tab-settings" class="tab-pane">
						<div class="form-group">
							<label>Заголовок страницы (title)</label>
							<input type="text" class="form-control" name="meta_title" value="<?=$item->meta_title?>">
						</div>
						<div class="form-group">
							<label>Ключевые слова (keywords)</label>
							<textarea name="keywords" class="form-control"><?=$item->keywords?></textarea>
						</div>
						<div class="form-group">
							<label>Описание страницы (description)</label>
							<textarea name="description" class="form-control"><?=$item->description?></textarea>
						</div>
					</div>
				</div>
				<div class="hr-line-dashed"></div>
				<div class="form-group">
					<input type="submit" value="Сохранить" class="btn btn-danger">&nbsp;&nbsp;&nbsp;
					<a href="/admin/pages" class="btn btn-default btn-outline">Отмена</a>
				</div>
				<div class="alert alert-success alert-dismissable form-success">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					Изменения успешно сохранены.
				</div>
			<?=Form::close()?>
		</div>
	</div>
</div>

<?=View::factory('admin/templates/_modal_list')?>
