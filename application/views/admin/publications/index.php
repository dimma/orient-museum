<div class="wrapper wrapper-content animated fadeInRight">
	<div class="ibox float-e-margins">
		<div class="ibox-content">
			<div class="table-responsive">
				<table class="table table-striped table-hover items-table">
					<thead>
						<tr>
							<th class="thin text-center">&#8470;</th>
							<th>Дата&nbsp;добавления</th>
							<th>Название</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					<? foreach($items as $item) { ?>
						<tr item_id="<?=$item->id?>" class="<? if($item->status == 0) { ?>inactive<? } ?>">
							<td class="text-center thin"><?=$item->id?></td>
							<td class="thin"><?=Date::format($item->date,'d F Y')?></td>
							<td><a href="/admin/publishing/<?=$item->id?>"><?=$item->title?></a></td>
							<td class="switch-cell" style="padding-left: 30px;"><input type="checkbox" class="js-switch" data-item="publication" data-id="<?=$item->id?>" <? if($item->status == '1') { ?>checked="checked"<? } ?> /></td>
							<td class="text-right thin"><button type="button" class="btn btn-danger btn-outline btn-xs" data-title="издание" data-action="delete" data-model="publication" data-id="<?=$item->id?>"><i class="fa fa-times"></i></button></td>
						</tr>
					<? } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
