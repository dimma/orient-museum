<div id="modal-template-<?=$type?>" class="modal fade" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content template-content">
			<?=Form::open('/admin/ajax/save_template', array('class' => 'ajax-form template-form', 'id' => 'template-' . $type))?>
				<input type="hidden" name="type" value="<?=$type?>"/>
				<input type="hidden" name="item_id"/>
				<input type="hidden" name="item_model"/>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title"><?=$name?></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<h4>Ссылка (с YouTube)</h4>
						<input type="text" name="title" class="form-control"/>
					</div>
					<h4>Описание</h4>
					<textarea name="text-<?=$type?>" class="form-control"></textarea>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Закрыть</button>
					<button type="submit" class="btn btn-danger">Сохранить</button>
				</div>
			<?=Form::close()?>
		</div>
	</div>
</div>