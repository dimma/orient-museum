<div id="modal-template-<?=$type?>" class="modal fade" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content template-content">
			<?=Form::open('/admin/ajax/save_template', array('class' => 'ajax-form template-form', 'id' => 'template-' . $type))?>
				<input type="hidden" name="type" value="<?=$type?>"/>
				<input type="hidden" name="item_id"/>
				<input type="hidden" name="item_model"/>
				<input type="hidden" name="adding_template" value="1"/>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title"><?=$name?></h4>
				</div>
				<div class="modal-body">
					<div id="item-<?=$type?>"></div>
					<div class="upload-wrap upload-button">
						<span class="btn file-upload btn-outline fileinput-button btn-white btn-block">
							<i class="fa fa-picture-o"></i>&nbsp;
							<span>Выбрать изображение</span>
							<input type="file" name="picture[]" data-url="/admin/ajax/upload_item_pic" accept=""/>
						</span>
						<div class="progress progress-striped active no-margin-bottom">
							<div style="width: 0" aria-valuemax="100" aria-valuemin="0" aria-valuenow="75" role="progressbar" class="progress-bar progress-bar-danger">
								<span class="sr-only"></span>
							</div>
						</div>
					</div>
					<h4>Комментарий</h4>
					<textarea name="text-<?=$type?>" class="form-control"></textarea>
					<br/><br/>
					<div class="upload-wrap upload-button">
						<span class="btn file-upload btn-outline fileinput-button btn-white btn-block">
							<i class="fa fa-picture-o"></i>&nbsp;
							<span>Выбрать изображение</span>
							<input type="file" name="picture[]" data-url="/admin/ajax/upload_item_pic" accept=""/>
						</span>
						<div class="progress progress-striped active no-margin-bottom">
							<div style="width: 0" aria-valuemax="100" aria-valuemin="0" aria-valuenow="75" role="progressbar" class="progress-bar progress-bar-danger">
								<span class="sr-only"></span>
							</div>
						</div>
					</div>
					<h4>Комментарий</h4>
					<textarea name="text2-<?=$type?>" class="form-control"></textarea>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Закрыть</button>
					<button type="submit" class="btn btn-danger">Сохранить</button>
				</div>
			<?=Form::close()?>
		</div>
	</div>
</div>