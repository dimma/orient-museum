<div id="modal-templates-list" class="modal fade" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Выберите шаблон</h4>
			</div>
			<div class="modal-body" id="templates-list">
				<?php foreach (Controller_Admin_Layout::$template_types as $type => $name) : ?>
					<button type="button" class="btn btn-outline btn-default btn-xs template-button" data-toggle="modal" data-target="#modal-template-<?=$type?>"><?=$name?></button>
				<?php endforeach; ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Закрыть</button>
			</div>
		</div>
	</div>
</div>

<?php foreach (Controller_Admin_Layout::$template_types as $type => $name) : ?>
	<?=View::factory('admin/templates/' . $type, array('type' => $type, 'name' => $name))?>
	<?=View::factory('admin/templates/_edit', array('type' => $type))?>
<?php endforeach; ?>