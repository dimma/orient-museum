<ul class="todo-list small-list">
	<?php foreach ($templates as $template) : ?>
		<li>
			<i class="fa fa-circle"></i>
			<b class="template-type">
				<?=$template['sort_order']?>. <?=Controller_Admin_Layout::$template_types[$template['type']]?>
			</b>
			<span class="template-content">
				<?php if (!empty($template['text'])) : ?>
					<?=Text::limit_chars(strip_tags($template['text']), 100, '&hellip;')?>
				<?php else : ?>
					<?=$template['title']?>
				<?php endif; ?>
			</span>
			<button type="button" class="pull-right btn btn-outline btn-default btn-xs" data-action="delete" data-model="template" data-id="<?=$template['id']?>" data-title="шаблон"><i class="fa fa-times"></i></button>
			<button type="button" class="pull-right btn btn-outline btn-default btn-xs" data-action="edit-template" data-toggle="modal" data-target="#modal-template-<?=$template['type']?>-edit" data-id="<?=$template['id']?>"><i class="fa fa-pencil"></i></button>
		</li>
	<?php endforeach; ?>
</ul>
