<?=Form::open('/admin/ajax/save_template', array('class' => 'ajax-form template-form', 'id' => 'template-edit-' . $template->type))?>
	<input type="hidden" name="type" value="<?=$template->type?>"/>
	<input type="hidden" name="template_id" value="<?=$template->id?>"/>
	<input type="hidden" name="item_id" value="<?=$template->item_model_id?>"/>
	<input type="hidden" name="item_model" value="<?=$template->parent?>"/>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title"><?=$name?></h4>
	</div>
	<div class="modal-body">
		<textarea name="text-<?=$template->type?>" class="form-control"><?=$template->text?></textarea>
		<div class="form-group">
			<h4>Подпись</h4>
			<input type="text" name="title" value="<?=$template->title?>" class="form-control"/>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Закрыть</button>
		<button type="submit" class="btn btn-danger">Сохранить</button>
	</div>
<?=Form::close()?>