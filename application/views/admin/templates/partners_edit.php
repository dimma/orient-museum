<?=Form::open('/admin/ajax/save_template', array('class' => 'ajax-form template-form', 'id' => 'template-edit-' . $template->type))?>
	<input type="hidden" name="type" value="<?=$template->type?>"/>
	<input type="hidden" name="template_id" value="<?=$template->id?>"/>
	<input type="hidden" name="item_id" value="<?=$template->item_model_id?>"/>
	<input type="hidden" name="item_model" value="<?=$template->parent?>"/>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title"><?=$name?></h4>
	</div>
	<div class="modal-body">
		<div class="form-group">
			<h4>URL</h4>
			<input type="text" name="title[]" placeholder="http://" class="form-control"/>
		</div>
		<div class="form-group form-logo">
			<h4>Лого</h4>
			<div class="upload-wrap upload-button">
				<span class="btn file-upload btn-outline fileinput-button btn-white btn-block">
					<i class="fa fa-picture-o"></i>&nbsp;
					<span>Выбрать изображение</span>
					<input type="file" name="picture[]" data-url="/admin/ajax/upload_item_pic" accept=""/>
				</span>
				<div class="progress progress-striped active no-margin-bottom">
					<div style="width: 0" aria-valuemax="100" aria-valuemin="0" aria-valuenow="75" role="progressbar" class="progress-bar progress-bar-danger">
						<span class="sr-only"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<a href="javascript:void(0);" class="add-partner"><i class="fa fa-plus"></i>&nbsp;Добавить партнера</a>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Закрыть</button>
		<button type="submit" class="btn btn-danger">Сохранить</button>
	</div>
<?=Form::close()?>