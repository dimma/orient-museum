<?=Form::open('/admin/ajax/save_template', array('class' => 'ajax-form template-form tiny-mce-form', 'id' => 'template-edit-' . $template->type))?>
	<input type="hidden" name="type" value="<?=$template->type?>"/>
	<input type="hidden" name="template_id" value="<?=$template->id?>"/>
	<input type="hidden" name="item_id" value="<?=$template->item_model_id?>"/>
	<input type="hidden" name="item_model" value="<?=$template->parent?>"/>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title"><?=$name?></h4>
	</div>
	<div class="modal-body">
		<?php if ($template->pictures->count_all()) : ?>
			<?php $picture = $template->pictures->where('cover', '=', '1')->order_by('date', 'DESC')->find(); ?>
			<div id="item-<?=$template->type?>">
				<img src="/assets/upload/items_pictures/<?=$picture->name?>_200_200.<?=$picture->ext?>" alt=""/>
				<button type="button" class="btn btn-xs btn-danger btn-outline delete-pic" style="top:20px;" data-id="<?=$picture?>">
					<i class="fa fa-times"></i>&nbsp;&nbsp;удалить
				</button>
			</div>
		<?php endif; ?>
		<div class="upload-wrap upload-button">
			<span class="btn file-upload btn-outline fileinput-button btn-white btn-block">
				<i class="fa fa-picture-o"></i>&nbsp;
				<span>Выбрать изображение</span>
				<input type="file" name="picture" data-url="/admin/ajax/upload_item_pic" accept=""/>
			</span>
			<div class="progress progress-striped active no-margin-bottom">
				<div style="width: 0" aria-valuemax="100" aria-valuemin="0" aria-valuenow="75" role="progressbar" class="progress-bar progress-bar-danger">
					<span class="sr-only"></span>
				</div>
			</div>
		</div>
		<textarea name="text-<?=$template->type?>" class="editor"><?=$template->text?></textarea>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Закрыть</button>
		<button type="submit" class="btn btn-danger">Сохранить</button>
	</div>
<?=Form::close()?>