<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Авторизация | AdminExpo</title>
		<link href="/assets/admin/css/bootstrap.min.css" rel="stylesheet">
		<link href="/assets/admin/font-awesome/css/font-awesome.css" rel="stylesheet">
		<link href="/assets/admin/css/animate.css" rel="stylesheet">
		<link href="/assets/admin/css/style.css" rel="stylesheet">
		<link href="/assets/admin/css/layout.css" rel="stylesheet">
		<style type="text/css">
			body { background-color: #f3f3f4 !important; }
			.logo-wrap { text-align: center; margin-top: 100px; }
			.loginscreen { margin-top: -150px !important; }
		</style>
	</head>
	<body class="gray-bg">
		<div class="logo-wrap"><h1 class="logo-name">AdminExpo</h1></div>
		<div class="middle-box text-center loginscreen animated fadeInDown">
			<div>
				<h3>Авторизация</h3>
				<?=Form::open('/admin/login', array('role' => 'form', 'class' => 'm-t', 'id' => 'auth-form'))?>
					<div class="form-group">
						<input type="text" name="username" class="form-control" placeholder="Логин" required="">
					</div>
					<div class="form-group">
						<input type="password" name="password" class="form-control" placeholder="Пароль" required="">
					</div>
					<button type="submit" class="btn btn-danger block full-width m-b">Войти</button>
				<?=Form::close()?>
				<p class="m-t"><small>&copy; 2014, SiteExpo</small></p>
			</div>
		</div>
		<!-- Mainly scripts -->
		<script src="/assets/admin/js/jquery-1.10.2.js"></script>
		<script src="/assets/admin/js/bootstrap.min.js"></script>

		<script type="text/javascript">
			$(document).ready(function(){
				$('#auth-form input[name="username"]').focus();
			});
		</script>
	</body>
</html>
