<div class="wrapper wrapper-content animated fadeInRight">
	<div class="ibox float-e-margins">
		<div class="ibox-content">
			<?=Form::open('/admin/ajax/update_item', array('class' => 'ajax-form', 'id' => 'update-headerblock-form'))?>
				<input type="hidden" name="item_id" value="<?=$item->id?>">
				<input type="hidden" name="item_model" value="setting">
				<div class="form-group">
					<label>Описание</label>
					<input type="text" class="form-control" name="value" value="<?=$item->value?>">
				</div>
				<div class="form-group">
					<label>Показывать до</label>
					<div class="row">
						<div class="col-sm-3">
							<div class="input-group date">
								<input type="text" name="date" class="form-control" value="<?=Date::format($item->date, 'd.m.Y')?>"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
					</div>
				</div>
				<div class="hr-line-dashed"></div>
				<div class="form-group">
					<input type="submit" value="Сохранить" class="btn btn-danger">
				</div>
				<div class="alert alert-success alert-dismissable form-success">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					Изменения успешно сохранены.
				</div>
			<?=Form::close()?>
		</div>
	</div>
</div>
