<?php defined('SYSPATH') or die('No direct access allowed.');
return array(
	'tempexhibitions' => array(
		'url' => 'tempexhibitions',
		'icon' => 'calendar',
		'name' => 'Временные выставки',
		'items' => array(
			'archive' => array(
				'url' => 'archive',
				'icon' => 'inbox',
				'name' => 'Архив',
			),
		)
	),
	'events' => array(
		'url' => 'events',
		'icon' => 'calendar-o',
		'name' => 'События',
		'items' => array(
			'lectures' => array(
				'url' => 'lectures',
				'icon' => 'graduation-cap',
				'name' => 'Циклы лекций',
			),
		)
	),
	'headerblock' => array(
		'url' => 'headerblock',
		'icon' => 'info-circle',
		'name' => 'Информация в шапке',
	),
	'oneexhibit' => array(
		'url' => 'oneexhibit',
		'icon' => 'picture-o',
		'name' => 'Выставка экспоната',
	),
	'polls' => array(
		'url' => 'polls',
		'icon' => 'comments',
		'name' => 'Опросы',
	),
	'news' => array(
		'url' => 'news',
		'icon' => 'file-text-o',
		'name' => 'Новости',
	),
	'publishing' => array(
		'url' => 'publishing',
		'icon' => 'book',
		'name' => 'Издания',
	),
	'contacts' => array(
		'url' => 'contacts',
		'icon' => 'phone-square',
		'name' => 'Контакты',
		'items' => array(
			'ru' => array(
				'url' => 'ru',
				'icon' => '',
				'name' => 'На русском',
			),
			'en' => array(
				'url' => 'en',
				'icon' => '',
				'name' => 'На английском',
			),
		)
	),
	'pages' => array(
		'url'	=> 'pages',
		'icon'	=> 'paste',
		'name'	=> 'Остальные страницы',
	),
);
?>