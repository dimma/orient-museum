<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
	'text'			=> 'Текст',
	'text_bg'		=> 'Текст с фоном',
	'quote'			=> 'Цитата',
	'two_cols'		=> 'Две колонки текста',
	'video'			=> 'Видео',
	'photo_comment'		=> 'Фото с комментарием',
	'two_photo_comment'	=> 'Два фото с комментарием',
	'small_photo_text'	=> 'Маленькое фото + текст',
	'gallery'		=> 'Галерея',
	'gallery_modal'		=> 'Галерея в отдельном окне',
	'partners'		=> 'Партнёры',
);
?>