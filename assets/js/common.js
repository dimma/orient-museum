$(document).ready(function(){

	function progressBarCarousel()
	{
		bar.css({width: percent + '%'});
		percent = percent + 0.5;
		if (percent > 100)
		{
			percent = 0;
			crsl.carousel('next');
		}
	}

	var percent	= 0;
	var interval	= 30;
	var bar		= $('.transition-timer-carousel-progress-bar');
	var crsl	= $('#indexCarousel');
	
	crsl.carousel({
		interval	: false,
		pause		: true
	}).on('slid.bs.carousel', function () {});

	var barInterval = setInterval(progressBarCarousel, interval);
	crsl.hover(
		function() { clearInterval(barInterval); },
		function() { barInterval = setInterval(progressBarCarousel, interval); }
	);

	var hoverItems	= ('.events .col-xs-8.col-sm-8.col-md-8 p, .events .col-xs-4.col-sm-4.col-md-4 span');
	var items	= ('.col-xs-8.col-sm-8.col-md-8 p:first-child a, .col-xs-8.col-sm-8.col-md-8 p:first-child span, .col-xs-4.col-sm-4.col-md-4 > span a');

	$(hoverItems).mouseover(function(){
		$(this).parent().parent('.row').find(items).css({color: '#c60c30'});
	});
	$(hoverItems).mouseout(function(){
		$(this).parent().parent('.row').find(items).css({color: ''});
	});

	// Menu hover and click
	$('.nav li.dropdown').hover(
		function() { $(this).addClass('active'); },
		function() { $(this).removeClass('active'); }
	);

	// Carousel slides counter
	$('#indexCarousel').on('slid.bs.carousel', function () {
		var carouselData = $(this).data('bs.carousel');
		var currentIndex = carouselData.getItemIndex(carouselData.$element.find('.item.active'));
		$('#carousel-index').text((currentIndex + 1) + "/" + carouselData.$items.length);
	});

	// Search
	$('#btn-search').click(function(){
		var $this	= $(this);
		var headerMenu	= $('.headerMenu');
		var searchInput	= $('#search input');
		if ($this.find('span').hasClass('icon-search'))
		{
			headerMenu.fadeOut();
			searchInput.fadeIn().focus();
			$this.find('span').attr('class', 'icon-close');
		}
		else
		{
			headerMenu.fadeIn();
			searchInput.fadeOut().focus();
			$this.find('span').attr('class', 'icon-search');
		}
	});

	// Header notify close button
	$('#headerNotify').click(function() {
		$.post('/admin/ajax/hide_notify');
		$(this).parents('.notifyMessage').hide();
	});

	$('li.dropdown').click(function() {
		var href = $(this).find('a').attr('href');
		switch (href)
		{
			// О музее
			case '/about':
				window.location = href;
			break;

			// Посетителям
			case '/visitors':
				window.location = '/contacts';
			break;

			// Выставки и события
			case '/events':
				window.location = '/exhibitions';
			break;

			// Постоянная экспозиция
			case '/exhibition':
				window.location = '/exhibitions';
			break;
		}
	});
});