// ================================================================================================================== //
// Common site scripts
// ================================================================================================================== //

!function ($) {
	$(function(){
		// Number format
		function number_format(number) {
			number += '';
			x = number.split('.');
			x1 = x[0];
			x2 = x.length > 1 ? '.' + x[1] : '';
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(x1)) { x1 = x1.replace(rgx, '$1' + ' ' + '$2'); }
			return x1 + x2;
		}

		// Show scroll for modal windows
		setInterval(function() {
			if ($('.modal-dialog').is(':visible'))
			{
				$('body').css('overflow', 'hidden');
				$('.modal').css('overflow', 'auto');
			}
			else
			{
				$('body').css('overflow', 'auto');
				$('.modal').css('overflow', 'hidden');
			}
		}, 3000);

		// Hide nav bar button
		$('.navbar-minimalize').click(function(){
			return false;
		});

		$('.navbar-default ul.nav > li > a').click(function(event){
			event.stopPropagation();
		});

		// CK editor
		function init_editor()
		{
			tinymce.init({
				selector	: 'textarea.editor',
				menubar		: false,
				language	: 'ru',
				plugins		: [
					"advlist autolink lists link image charmap print preview anchor",
					"searchreplace visualblocks code fullscreen",
					"insertdatetime media table contextmenu paste code"
				],
				toolbar		: "styleselect | bold italic | bullist numlist | table | link | code",
				style_formats	: [{
					title: "Headers",
					items: [
						{title: "Header 2", format: "h2"},
						{title: "Header 3", format: "h3"},
						{title: "Header 4", format: "h4"},
						{title: "Header 5", format: "h5"},
						{title: "Header 6", format: "h6"}
					]
				}],
				content_css	: '/assets/admin/css/editor.css',
				height		: $(window).height() - 300,
				skin		: 'adminexpo'
			});
		}
		init_editor();

		// Init all
		function init_all()
		{
			init_ajax_forms();
			init_switchery();
			init_datepicker();
			init_event_dates();
			init_lecture_info();
			init_delete_answer();

			if($('textarea').length > 0)
			{
				init_textarea();
			}

			if($('.i-checks').length > 0)
			{
				$('.i-checks').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
				});
			}
		}
		init_all();

		// Datepicker
		function init_datepicker()
		{
			$('.input-daterange').datepicker({
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				format: 'dd.mm.yyyy',
				language: 'ru'
			});

			$('.input-group.date').datepicker({
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				format: 'dd.mm.yyyy',
				autoclose: true,
				language: 'ru'
			});
		}

		function init_textarea()
		{
			$('textarea').autosize();
		}

		function init_switchery()
		{
			$('.js-switch').each(function(){
				//var elem = document.querySelector('.js-switch');
				var switcher = new Switchery(this);

				$(this).change(function(){
					var $this = $(this);
					if ($this.attr('data-item'))
					{
						var status = ($this.is(':checked')) ? 1 : 0;
						$.post('/admin/ajax/update_item_status', { item_type: $this.attr('data-item'), item_id: $this.attr('data-id'), status: status });

						// Unset all active polls
						if ($this.attr('data-item') === 'poll' && status == 1)
						{
							$this.parents('tr').siblings().toggleClass('inactive', true);
						}
						$this.parents('tr').toggleClass('inactive', status == 0);
					}
				});
			});
		}

		// Ajax forms
		function init_ajax_forms()
		{
			$('.ajax-form').unbind().submit(function(){
				var $this	= $(this);
				var isTemplates	= $this.hasClass('template-form');
				var hasTinyMce	= $this.hasClass('tiny-mce-form');
				var type	= $this.find('[name=type]').val();
				$this.find('input[type="submit"]').attr('disabled','disabled');

				if (hasTinyMce)
				{
					var editor	= tinyMCE.get('text-' + type);
					var editor2	= tinyMCE.get('text2-' + type);
					if (editor != null)
					{
						editor.setProgressState(true);
						$this.find('textarea[name=text-' + type + ']').val(editor.getContent());
					}
					if (editor2 != null)
					{
						editor2.setProgressState(true);
						$this.find('textarea[name=text2-' + type + ']').val(editor2.getContent());
					}
				}

				$.post($this.attr('action'), $this.serialize(), function(data){
					var response = $.parseJSON(data);

					$this.find('input[type="submit"]').removeAttr('disabled');
					$this.find('.has-error').removeClass('has-error');
					$this.find('.help-block').hide();

					if(response.errors)
					{
						for(var n in response.errors)
						{
							$this.find('*[name="'+n+'"]').parents('.form-group').addClass('has-error');
							$this.find('*[name="'+n+'"]').parents('.form-group').find('.help-block').show();
						}
					}

					if(response.status == 1)
					{
						$this.find('.form-success').fadeIn('fast');
						setTimeout(function(){
							$this.find('.form-success').fadeOut('fast');
						}, 2000);

						switch ($this.attr('id'))
						{
							case 'add-tempexhibition-form':
								window.location = '/admin/tempexhibitions/'+response.item_id;
							break;
							
							case 'add-event-form':
								window.location = '/admin/events/'+response.item_id;
							break;
							
							case 'add-poll-form':
								window.location = '/admin/polls/'+response.item_id;
							break;
							
							case 'add-news-form':
								window.location = '/admin/news/'+response.item_id;
							break;
							
							case 'add-publication-form':
								window.location = '/admin/publishing/'+response.item_id;
							break;

							case 'add-lecture-form':
								$('#modal-add-lecture').modal('hide');
								$('#items-list-wrap').show();
								$('#items-empty').hide();
								$('#lectures-list').load('/admin/ajax/get_lectures_list', function(){
									init_switchery();
									init_lecture_info();
								});
							break;
							
							case 'update-event-form':
								$.post('/admin/ajax/get_event_dates', { id: $this.find('input[name="item_id"]').val() }, function(data){
									var response = $.parseJSON(data);
									$('#event-dates-list').html(response.dates);
									init_datepicker();
									init_event_dates();
								});
							break;
							
							case 'update-lecture-form':
								$('#modal-lecture-info').modal('hide');
								$('#lectures-list').load('/admin/ajax/get_lectures_list', function(){
									init_switchery();
									init_lecture_info();
								});
							break;
							
							case 'update-poll-form':
								$.post('/admin/ajax/get_poll_answers/' + $this.find('input[name="item_id"]').val(), function(data){
									var response = $.parseJSON(data);
									$('#pollanswers-list').html(response.answers);
									init_delete_answer();
								});
							break;
						}

						// Templates
						if (isTemplates)
						{
							$this.parents('.modal').modal('hide');

							if (hasTinyMce)
							{
								if (editor != null)
								{
									editor.setProgressState(false);
									editor.setContent('');
								}
								if (editor2 != null)
								{
									editor2.setProgressState(false);
									editor2.setContent('');
								}
							}

							$('#item-templates-list ul').load(
								'/admin/ajax/templates_list/',
								{
									item_model	: $this.find('input[name="item_model"]').val(),
									item_id		: $this.find('input[name="item_id"]').val()
								},
								function() {
									$this.find('input:not([type="hidden"])').val('');
									$this.find('textarea').val('');
									$('#templates-hint').hide();
									init_delete_item();
									init_edit_template();
								}
							);
						}
					}
				});
				return false;
			});
		}

		// Delete item
		function init_delete_item()
		{
			$('button[data-action="delete"]').unbind().click(function(){
				var self = this;
				bootbox.confirm("Вы действительно хотите удалить "+$(self).attr('data-title')+"?", function(result) {
					if(result)
					{
						$.post('/admin/ajax/delete_item', { item_model: $(self).attr('data-model'), item_id: $(self).attr('data-id') }, function(){
							if($(self).parents('tr[item_id]').length > 0)
							{
								$(self).parents('tr[item_id]').fadeOut('fast', function(){
									$(self).parents('tr[item_id]').remove();
								});
							}
							if($(self).parents('#item-templates-list').length > 0)
							{
								$(self).parents('li').fadeOut('fast', function(){
									$(self).parents('li').remove();

									if($('#item-templates-list li').length == 0)
									{
										$('#templates-hint').show();
									}
								});
							}
						});
					}
				});
			});
		}
		init_delete_item();

		// Edit template
		function init_edit_template()
		{
			$('button[data-action="edit-template"]').unbind().click(function(){
				var $this = $(this);
				$($this.attr('data-target') + ' .template-content').load(
					'/admin/ajax/template_content',
					{ template_id: $this.attr('data-id') },
					function() { init_editor(); init_ajax_forms(); }
				);
			});
		}
		init_edit_template();

		// File upload
		function init_file_upload()
		{
			$('.file-upload input[type="file"]').unbind().fileupload({
				dataType: 'json',
				formData: function(form) {
					return $(form).serializeArray();
				},
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					$(this).parents('.upload-wrap').find('.progress-bar').css({ width: progress + '%' });
				},
				done: function (e, data) {
					// data.result.file
					if(data.result.type == 'item_cover')
					{
						var html	= '';
						var imgEl	= '#item-';
						var type	= $(this).parents('.template-form').find('[name=type]').val();
						var del		= false;

						switch (type)
						{
							case 'photo_comment':
							case 'two_photo_comment':
							case 'small_photo_text':
							case 'gallery':
							case 'gallery_modal':
							case 'partners':
								imgEl += type;
								del = 'style="top:20px;"';
							break;

							default:
								imgEl += 'pic';
							break;
						}

						if(data.result.temp)
						{
							html = '<img width="250px" src="/assets/upload/temp/'+data.result.name+'.'+data.result.ext+'" alt=""/>'
								+ '<button type="button" class="btn btn-xs btn-danger btn-outline delete-pic" ' + (del ? del : '') + ' data-name="'+data.result.name+'" data-ext="'+data.result.ext+'">'
									+ '<i class="fa fa-times"></i>&nbsp;&nbsp;удалить'
								+ '</button>';
							$(data.form).append('<input type="hidden" name="pictures['+data.result.name+']" value="'+data.result.ext+'"/>');
						}
						else
						{
							html = '<img width="250px" src="'+data.result.preview+'" alt=""/>'
								+ '<button type="button" class="btn btn-xs btn-danger btn-outline delete-pic" ' + (del ? del : '') + ' data-id="'+data.result.id+'">'
									+ '<i class="fa fa-times"></i>&nbsp;&nbsp;удалить'
								+ '</button>';
						}

						$(imgEl).html(html);
					}
				},
				stop: function(){
					$(this).parents('.upload-wrap').find('.progress-bar').css({ width: 0 });
				}
			});
		}
		init_file_upload();

		$('body').on('click', '.delete-pic', function() {
			var self = $(this);

			if (self.attr('data-id'))
			{
				$.post('/admin/ajax/delete_pic', { id: self.attr('data-id') }, function(){
					hide_photo(self);
				});
			}
			// Temporary picture
			else if (self.attr('data-name') && self.attr('data-ext'))
			{
				$.post('/admin/ajax/delete_pic', { name: self.attr('data-name'), ext: self.attr('data-ext') }, function(){
					hide_photo(self);
					$('input[value="'+self.attr('data-name')+'.'+self.attr('data-ext')+'"]').remove();
				});
			}
		}).on('click', '.add-partner', function() {
			var $this = $(this);
			var html = '<div class="form-group">'
					+ '<h4>URL</h4>'
					+ '<input type="text" name="title[]" placeholder="http://" class="form-control"/>'
				+ '</div>'
				+ '<div class="form-group form-logo">'
					+ '<h4>Лого</h4>'
					+ '<div class="upload-wrap upload-button">'
						+ '<span class="btn file-upload btn-outline fileinput-button btn-white btn-block">'
							+ '<i class="fa fa-picture-o"></i>&nbsp;'
							+ '<span>Выбрать изображение</span>'
							+ '<input type="file" name="picture[]" data-url="/admin/ajax/upload_item_pic" accept=""/>'
						+ '</span>'
						+ '<div class="progress progress-striped active no-margin-bottom">'
							+ '<div style="width: 0" aria-valuemax="100" aria-valuemin="0" aria-valuenow="75" role="progressbar" class="progress-bar progress-bar-danger">'
								+ '<span class="sr-only"></span>'
							+ '</div>'
						+ '</div>'
					+ '</div>'
				+ '</div>';
			$this.parents('.modal-body').find('.form-logo').last().after(html);
			init_file_upload();
		});

		function hide_photo(self)
		{
			var imgEl	= '#item-';
			var type	= self.parents('.template-form').find('[name=type]').val();

			switch (type)
			{
				case 'photo_comment':
				case 'two_photo_comment':
				case 'small_photo_text':
				case 'gallery':
				case 'gallery_modal':
				case 'partners':
					imgEl += type;
				break;

				default:
					imgEl += 'pic';
				break;
			}

			// Item main picture
			if (self.parents('#item-' + imgEl).length > 0)
			{
				$('#item-' + imgEl + ' img, #item-' + imgEl + ' button').fadeOut('fast');
			}
		}

		// Additional options for selects in items edit
		$('select[data-change="additional"]').change(function(){
			$('[data-additional="'+$(this).attr('name')+'"]').hide();

			if($('[data-additional="'+$(this).attr('name')+'"][data-value="'+$(this).val()+'"]').length > 0)
			{
				$('[data-additional="'+$(this).attr('name')+'"][data-value="'+$(this).val()+'"]').fadeIn('fast', function(){
					$(this).find('input[type="text"]:eq(0)').focus();
				});
			}
			else
			{
				$('[data-additional="'+$(this).attr('name')+'"]').fadeOut('fast');
			}
		});

		// Add event date
		function init_event_dates()
		{
			$('.delete-event-date').unbind().click(function(){
				var self = this;

				if($(self).attr('data-id'))
				{
					$.post('/admin/ajax/delete_eventdate', { id: $(self).attr('data-id') });
				}

				$(self).parents('.date-row').fadeOut('fast', function(){
					$(self).parents('.date-row').remove();
				});

				return false;
			});
		}

		$('#add-event-date').click(function(){
			var hours	= '';
			var minutes	= '';
			var i;

			for (i = 10; i < 23; i++)
			{
				hours += '<option>' + i + '</option>';
			}
			for (i = 0; i < 60; i += 15)
			{
				minutes += '<option>' + (i === 0 ? '0' : '') + i + '</option>';
			}

			var html = '<div class="row date-row">'
					+ '<div class="col-sm-3">'
						+ '<div class="input-group date">'
							+ '<input type="text" name="new_eventdates[date][]" class="form-control"/>'
							+ '<span class="input-group-addon"><i class="fa fa-calendar"></i></span>'
						+ '</div>'
					+ '</div>'
					+ '<div class="col-sm-2">'
						+ '<div class="input-group">'
							+ '<select class="form-control pull-left" name="new_eventdates[hour][]">' + hours + '</select>'
							+ '<span class="input-group-addon no-borders">:</span>'
							+ '<select class="form-control pull-left" name="new_eventdates[minute][]">' + minutes + '</select>'
						+ '</div>'
					+ '</div>'
					+ '<div class="col-sm-6">'
						+ '<div class="lecture-date-action">'
							+ '<a href="#" class="delete-event-date text-danger">Удалить</a>'
						+ '</div>'
					+ '</div>'
				+ '</div>';
			$('#event-dates-list').append(html);
			init_event_dates();
			init_datepicker();

			return false;
		});

		// Lecture info
		function init_lecture_info()
		{
			$('#lectures-list tr').click(function(){
				$('#modal-lecture-info .modal-title').text($(this).find('.item-title').text());
				$('#lecture-info-body').load('/admin/ajax/lecture_info/'+$(this).attr('item_id'));
			});
		}

		// Polls
		$('#add-pollanswer').click(function(){
			$('#pollanswers-list').append(
				'<li>'
					+ '<div class="row">'
						+ '<span class="col-sm-5">'
							+ '<input type="text" class="form-control" name="newanswers[]""/>'
						+ '</span>'
						+ '<span class="col-sm-3 add-answer-wrap">'
							+ '<a href="#" class="delete-answer text-danger">Удалить</a>'
						+ '</span>'
					+ '</div>'
				+ '</li>'
			);
			$('#pollanswers-list input[type="text"]:last').focus();
			init_delete_answer();

			return false;
		});

		function init_delete_answer()
		{
			$('.delete-answer').click(function(){
				var self = this;

				if($(self).attr('data-id'))
				{
					$.post('/admin/ajax/delete_item', { item_model: 'pollanswer', item_id: $(self).attr('data-id') });
				}

				$(this).parents('li').fadeOut('fast', function(){
					$(self).parents('li').remove();
				});
			});
		}

		// Templates
		$('#templates-list button').click(function(){
			$('#modal-templates-list').modal('hide');

			if($('.item-form input[name="item_model"]').val() && $('.item-form input[name="item_id"]').val())
			{
				var modal = $(this).attr('data-target').replace('#','');
				$('#'+modal+' input[name="item_id"]').val($('.item-form input[name="item_id"]').val());
				$('#'+modal+' input[name="item_model"]').val($('.item-form input[name="item_model"]').val());
			}
		});
	});
}(window.jQuery)